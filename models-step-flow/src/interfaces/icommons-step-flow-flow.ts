import { CommonsType } from 'tscommons-core';
import { ICommonsOrientatedOrdered, isICommonsOrientatedOrdered } from 'tscommons-models';

import { ICommonsStepFlowStep } from './icommons-step-flow-step';

export interface ICommonsStepFlowFlow<S extends ICommonsStepFlowStep> extends ICommonsOrientatedOrdered<S> {
		step: number;
		
		outstep: number;
}

export function isICommonsStepFlowFlow<S extends ICommonsStepFlowStep>(test: unknown): test is ICommonsStepFlowFlow<S> {
	if (!isICommonsOrientatedOrdered<ICommonsStepFlowStep>(test, 'step')) return false;
	
	if (!CommonsType.hasPropertyNumber(test, 'outstep')) return false;
	
	return true;
}
