import { CommonsType } from 'tscommons-core';
import { ICommonsFirstClass, isICommonsFirstClass } from 'tscommons-models';

import { ECommonsStepFlowStepType, isECommonsStepFlowStepType } from '../enums/ecommons-step-flow-step-type';

export interface ICommonsStepFlowStep extends ICommonsFirstClass {
		type: ECommonsStepFlowStepType;
}

export function isICommonsStepFlowStep(test: unknown): test is ICommonsStepFlowStep {
	if (!isICommonsFirstClass(test)) return false;
	
	if (!CommonsType.hasPropertyEnum<ECommonsStepFlowStepType>(test, 'type', isECommonsStepFlowStepType)) return false;
	
	return true;
}
