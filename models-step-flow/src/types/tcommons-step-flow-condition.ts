import { CommonsType } from 'tscommons-core';

import { ICommonsStepFlowCondition } from '../interfaces/icommons-step-flow-condition';
import { ICommonsStepFlowStep } from '../interfaces/icommons-step-flow-step';
import { ICommonsStepFlowFlow } from '../interfaces/icommons-step-flow-flow';

export type TCommonsStepFlowCondition = Readonly<ICommonsStepFlowCondition<
		ICommonsStepFlowFlow<ICommonsStepFlowStep>,
		ICommonsStepFlowStep
>>;

export function isTCommonsStepFlowCondition(test: unknown): test is TCommonsStepFlowCondition {
	if (!CommonsType.hasPropertyNumber(test, 'flow')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'id')) return false;
	
	return true;
}

export function encodeICommonsStepFlowCondition(condition: ICommonsStepFlowCondition<ICommonsStepFlowFlow<ICommonsStepFlowStep>, ICommonsStepFlowStep>): TCommonsStepFlowCondition {
	return {
			flow: condition.flow,
			id: condition.id
	};
}

export function decodeICommonsStepFlowCondition(encoded: TCommonsStepFlowCondition): ICommonsStepFlowCondition<ICommonsStepFlowFlow<ICommonsStepFlowStep>, ICommonsStepFlowStep> {
	return {
			// tslint:disable:object-literal-sort-keys
			flow: encoded.flow,
			id: encoded.id
			// tslint:enable:object-literal-sort-keys
	};
}
