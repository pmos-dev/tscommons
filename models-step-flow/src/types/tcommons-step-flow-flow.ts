import { CommonsType } from 'tscommons-core';

import { ICommonsStepFlowStep } from '../interfaces/icommons-step-flow-step';
import { ICommonsStepFlowFlow } from '../interfaces/icommons-step-flow-flow';

export type TCommonsStepFlowFlow = Readonly<ICommonsStepFlowFlow<
		ICommonsStepFlowStep
>>;

export function isTCommonsStepFlowFlow(test: unknown): test is TCommonsStepFlowFlow {
	if (!CommonsType.hasPropertyNumber(test, 'step')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'id')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'ordered')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'outstep')) return false;
	
	return true;
}

export function encodeICommonsStepFlowFlow(flow: ICommonsStepFlowFlow<ICommonsStepFlowStep>): TCommonsStepFlowFlow {
	return {
			step: flow.step,
			id: flow.id,
			ordered: flow.ordered,
			outstep: flow.outstep
	};
}

export function decodeICommonsStepFlowFlow(encoded: TCommonsStepFlowFlow): ICommonsStepFlowFlow<ICommonsStepFlowStep> {
	return {
			// tslint:disable:object-literal-sort-keys
			step: encoded.step,
			id: encoded.id,
			ordered: encoded.ordered,
			outstep: encoded.outstep
			// tslint:enable:object-literal-sort-keys
	};
}
