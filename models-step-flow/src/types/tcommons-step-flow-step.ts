import { CommonsType } from 'tscommons-core';

import { ICommonsStepFlowStep } from '../interfaces/icommons-step-flow-step';

import { isECommonsStepFlowStepType, fromECommonsStepFlowStepType, toECommonsStepFlowStepType } from '../enums/ecommons-step-flow-step-type';

export type TCommonsStepFlowStep = Readonly<Omit<
		ICommonsStepFlowStep,
		'type'
>> & {
		type: string;
};

export function isTCommonsStepFlowStep(test: unknown): test is TCommonsStepFlowStep {
	if (!CommonsType.hasPropertyNumber(test, 'id')) return false;
	
	if (!CommonsType.hasPropertyString(test, 'type')) return false;
	if (!isECommonsStepFlowStepType(test.type)) return false;
	
	return true;
}

export function encodeICommonsStepFlowStep(step: ICommonsStepFlowStep): TCommonsStepFlowStep {
	return {
			id: step.id,
			type: fromECommonsStepFlowStepType(step.type)
	};
}

export function decodeICommonsStepFlowStep(encoded: TCommonsStepFlowStep): ICommonsStepFlowStep {
	return {
			// tslint:disable:object-literal-sort-keys
			id: encoded.id,
			type: toECommonsStepFlowStepType(encoded.type)!
			// tslint:enable:object-literal-sort-keys
	};
}
