import { CommonsType } from 'tscommons-core';

export enum ECommonsStepFlowStepType {
		ROOT = 'root',
		DATA = 'data',
		COMPLETE = 'complete',
		ABORT = 'abort'
}

export function toECommonsStepFlowStepType(type: string): ECommonsStepFlowStepType|undefined {
	switch (type) {
		case ECommonsStepFlowStepType.ROOT.toString():
			return ECommonsStepFlowStepType.ROOT;
		case ECommonsStepFlowStepType.DATA.toString():
			return ECommonsStepFlowStepType.DATA;
		case ECommonsStepFlowStepType.COMPLETE.toString():
			return ECommonsStepFlowStepType.COMPLETE;
		case ECommonsStepFlowStepType.ABORT.toString():
			return ECommonsStepFlowStepType.ABORT;
	}
	return undefined;
}

export function fromECommonsStepFlowStepType(type: ECommonsStepFlowStepType): string {
	switch (type) {
		case ECommonsStepFlowStepType.ROOT:
			return ECommonsStepFlowStepType.ROOT.toString();
		case ECommonsStepFlowStepType.DATA:
			return ECommonsStepFlowStepType.DATA.toString();
		case ECommonsStepFlowStepType.COMPLETE:
			return ECommonsStepFlowStepType.COMPLETE.toString();
		case ECommonsStepFlowStepType.ABORT:
			return ECommonsStepFlowStepType.ABORT.toString();
	}
	
	throw new Error('Unknown ECommonsStepFlowStepType');
}

export function isECommonsStepFlowStepType(test: unknown): test is ECommonsStepFlowStepType {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsStepFlowStepType(test) !== undefined;
}

export function keyToECommonsStepFlowStepType(key: string): ECommonsStepFlowStepType {
	switch (key) {
		case 'ROOT':
			return ECommonsStepFlowStepType.ROOT;
		case 'DATA':
			return ECommonsStepFlowStepType.DATA;
		case 'COMPLETE':
			return ECommonsStepFlowStepType.COMPLETE;
		case 'ABORT':
			return ECommonsStepFlowStepType.ABORT;
	}
	
	throw new Error(`Unable to obtain ECommonsStepFlowStepType for key: ${key}`);
}

export const ECOMMONS_STEP_FLOW_STEP_TYPES: ECommonsStepFlowStepType[] = Object.keys(ECommonsStepFlowStepType)
		.map((e: string): ECommonsStepFlowStepType => keyToECommonsStepFlowStepType(e));
