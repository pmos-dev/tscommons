import { CommonsType } from 'tscommons-core';

export interface ICommonsSession<T> {
		sid: string;
		
		start: Date;
		last: Date;
		
		data: T;
}

export function isICommonsSession<T>(
		test: unknown,
		isT: (t: unknown) => t is T
): test is ICommonsSession<T> {
	if (!CommonsType.hasPropertyString(test, 'sid')) return false;
	
	if (!CommonsType.hasPropertyDate(test, 'start')) return false;
	if (!CommonsType.hasPropertyDate(test, 'last')) return false;

	if (!CommonsType.hasPropertyT<T>(test, 'data', isT)) return false;
	
	return true;
}
