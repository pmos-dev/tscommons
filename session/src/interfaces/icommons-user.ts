import { CommonsType } from 'tscommons-core';

export interface ICommonsUser {
		username: string;
		enabled: boolean;
}

export function isICommonsUser(
		test: unknown
): test is ICommonsUser {
	if (!CommonsType.hasPropertyString(test, 'username')) return false;
	if (!CommonsType.hasPropertyBoolean(test, 'enabled')) return false;
	
	return true;
}
