import { CommonsType } from 'tscommons-core';

export enum ECommonsAdamantineManagedDataType {
		BOOLEAN = 'boolean',
		DATETIME = 'datetime',
		DATE = 'date',
		EMAIL = 'email',
		ENUM = 'enum',
		FLOAT = 'float',
		HEXRGB = 'hexrgb',
		INT = 'int',
		SMALLINT = 'smallint',
		STRING = 'string',
		TEXT = 'text',
		TIME = 'time',
		TINYINT = 'tinyint',
		URL = 'url'
}

export function toECommonsAdamantineManagedDataType(type: string): ECommonsAdamantineManagedDataType|undefined {
	switch (type) {
		case ECommonsAdamantineManagedDataType.BOOLEAN.toString():
			return ECommonsAdamantineManagedDataType.BOOLEAN;
		case ECommonsAdamantineManagedDataType.DATETIME.toString():
			return ECommonsAdamantineManagedDataType.DATETIME;
		case ECommonsAdamantineManagedDataType.DATE.toString():
			return ECommonsAdamantineManagedDataType.DATE;
		case ECommonsAdamantineManagedDataType.EMAIL.toString():
			return ECommonsAdamantineManagedDataType.EMAIL;
		case ECommonsAdamantineManagedDataType.ENUM.toString():
			return ECommonsAdamantineManagedDataType.ENUM;
		case ECommonsAdamantineManagedDataType.FLOAT.toString():
			return ECommonsAdamantineManagedDataType.FLOAT;
		case ECommonsAdamantineManagedDataType.HEXRGB.toString():
			return ECommonsAdamantineManagedDataType.HEXRGB;
		case ECommonsAdamantineManagedDataType.INT.toString():
			return ECommonsAdamantineManagedDataType.INT;
		case ECommonsAdamantineManagedDataType.SMALLINT.toString():
			return ECommonsAdamantineManagedDataType.SMALLINT;
		case ECommonsAdamantineManagedDataType.STRING.toString():
			return ECommonsAdamantineManagedDataType.STRING;
		case ECommonsAdamantineManagedDataType.TEXT.toString():
			return ECommonsAdamantineManagedDataType.TEXT;
		case ECommonsAdamantineManagedDataType.TIME.toString():
			return ECommonsAdamantineManagedDataType.TIME;
		case ECommonsAdamantineManagedDataType.TINYINT.toString():
			return ECommonsAdamantineManagedDataType.TINYINT;
		case ECommonsAdamantineManagedDataType.URL.toString():
			return ECommonsAdamantineManagedDataType.URL;
	}
	return undefined;
}

export function fromECommonsAdamantineManagedDataType(type: ECommonsAdamantineManagedDataType): string {
	switch (type) {
		case ECommonsAdamantineManagedDataType.BOOLEAN:
			return ECommonsAdamantineManagedDataType.BOOLEAN.toString();
		case ECommonsAdamantineManagedDataType.DATETIME:
			return ECommonsAdamantineManagedDataType.DATETIME.toString();
		case ECommonsAdamantineManagedDataType.DATE:
			return ECommonsAdamantineManagedDataType.DATE.toString();
		case ECommonsAdamantineManagedDataType.EMAIL:
			return ECommonsAdamantineManagedDataType.EMAIL.toString();
		case ECommonsAdamantineManagedDataType.ENUM:
			return ECommonsAdamantineManagedDataType.ENUM.toString();
		case ECommonsAdamantineManagedDataType.FLOAT:
			return ECommonsAdamantineManagedDataType.FLOAT.toString();
		case ECommonsAdamantineManagedDataType.HEXRGB:
			return ECommonsAdamantineManagedDataType.HEXRGB.toString();
		case ECommonsAdamantineManagedDataType.INT:
			return ECommonsAdamantineManagedDataType.INT.toString();
		case ECommonsAdamantineManagedDataType.SMALLINT:
			return ECommonsAdamantineManagedDataType.SMALLINT.toString();
		case ECommonsAdamantineManagedDataType.STRING:
			return ECommonsAdamantineManagedDataType.STRING.toString();
		case ECommonsAdamantineManagedDataType.TEXT:
			return ECommonsAdamantineManagedDataType.TEXT.toString();
		case ECommonsAdamantineManagedDataType.TIME:
			return ECommonsAdamantineManagedDataType.TIME.toString();
		case ECommonsAdamantineManagedDataType.TINYINT:
			return ECommonsAdamantineManagedDataType.TINYINT.toString();
		case ECommonsAdamantineManagedDataType.URL:
			return ECommonsAdamantineManagedDataType.URL.toString();
	}
	
	throw new Error('Unknown ECommonsAdamantineManagedDataType');
}

export function isECommonsAdamantineManagedDataType(test: unknown): test is ECommonsAdamantineManagedDataType {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsAdamantineManagedDataType(test) !== undefined;
}

export function keyToECommonsAdamantineManagedDataType(key: string): ECommonsAdamantineManagedDataType {
	switch (key) {
		case 'BOOLEAN':
			return ECommonsAdamantineManagedDataType.BOOLEAN;
		case 'DATETIME':
			return ECommonsAdamantineManagedDataType.DATETIME;
		case 'DATE':
			return ECommonsAdamantineManagedDataType.DATE;
		case 'EMAIL':
			return ECommonsAdamantineManagedDataType.EMAIL;
		case 'ENUM':
			return ECommonsAdamantineManagedDataType.ENUM;
		case 'FLOAT':
			return ECommonsAdamantineManagedDataType.FLOAT;
		case 'HEXRGB':
			return ECommonsAdamantineManagedDataType.HEXRGB;
		case 'INT':
			return ECommonsAdamantineManagedDataType.INT;
		case 'SMALLINT':
			return ECommonsAdamantineManagedDataType.SMALLINT;
		case 'STRING':
			return ECommonsAdamantineManagedDataType.STRING;
		case 'TEXT':
			return ECommonsAdamantineManagedDataType.TEXT;
		case 'TIME':
			return ECommonsAdamantineManagedDataType.TIME;
		case 'TINYINT':
			return ECommonsAdamantineManagedDataType.TINYINT;
		case 'URL':
			return ECommonsAdamantineManagedDataType.URL;
	}
	
	throw new Error(`Unable to obtain ECommonsAdamantineManagedDataType for key: ${key}`);
}

export const ECOMMONS_ADAMANTINE_MANAGED_DATA_TYPES: ECommonsAdamantineManagedDataType[] = Object.keys(ECommonsAdamantineManagedDataType)
		.map((e: string): ECommonsAdamantineManagedDataType => keyToECommonsAdamantineManagedDataType(e));
