import { CommonsType } from 'tscommons-core';

import { ICommonsManagedModelMetadata } from '../interfaces/icommons-managed-model-metadata';
import { ICommonsManagedModelDataField } from '../interfaces/icommons-managed-model-data-field';

import { TCommonsManagedModelDataField, isTCommonsManagedModelDataField, encodeICommonsManagedModelDataField, decodeICommonsManagedModelDataField } from '../types/tcommons-managed-model-data-field';

import { ECommonsAdamantineAccess, isECommonsAdamantineAccess } from '../enums/ecommons-adamantine-access';

export type TCommonsManagedModelMetadata = Readonly<Omit<
		ICommonsManagedModelMetadata,
		'manageableFields' | 'isAliasingSupported'
> & {
		manageableFields: TCommonsManagedModelDataField[];
}>;

type TCommonsManagedModelMetadataWithIsAliasingSupported = TCommonsManagedModelMetadata & {
		isAliasingSupported: boolean;
};

export function isTCommonsManagedModelMetadata(test: unknown): test is TCommonsManagedModelMetadata {
	if (!CommonsType.hasPropertyTArray<TCommonsManagedModelDataField>(test, 'manageableFields', isTCommonsManagedModelDataField)) return false;
	if (!CommonsType.hasPropertyBooleanOrUndefined(test, 'isAliasingSupported')) return false;
	if (!CommonsType.hasPropertyEnum<ECommonsAdamantineAccess>(test, 'accessRequiredToManage', isECommonsAdamantineAccess)) return false;
	
	return true;
}

export function encodeICommonsManagedModelMetadata(data: ICommonsManagedModelMetadata): TCommonsManagedModelMetadata {
	const encoded: TCommonsManagedModelMetadata = {
			// tslint:disable:object-literal-sort-keys
			manageableFields: data.manageableFields
					.map((field: ICommonsManagedModelDataField): TCommonsManagedModelDataField => encodeICommonsManagedModelDataField(field)),
			accessRequiredToManage: data.accessRequiredToManage
			// tslint:enable:object-literal-sort-keys
	};
	
	if (data.isAliasingSupported !== undefined) {
		(encoded as TCommonsManagedModelMetadataWithIsAliasingSupported).isAliasingSupported = data.isAliasingSupported;
	}
	
	return encoded;
}

export function decodeICommonsManagedModelMetadata(encoded: TCommonsManagedModelMetadata): ICommonsManagedModelMetadata {
	const decoded: ICommonsManagedModelMetadata = {
			// tslint:disable:object-literal-sort-keys
			manageableFields: encoded.manageableFields
					.map((field: TCommonsManagedModelDataField): ICommonsManagedModelDataField => decodeICommonsManagedModelDataField(field)),
			accessRequiredToManage: encoded.accessRequiredToManage
			// tslint:enable:object-literal-sort-keys
	};

	if (encoded['isAliasingSupported'] !== undefined) {
		decoded.isAliasingSupported = (encoded as TCommonsManagedModelMetadataWithIsAliasingSupported).isAliasingSupported;
	}
	
	return decoded;
}
