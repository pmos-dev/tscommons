import { CommonsType } from 'tscommons-core';

import { ICommonsManagedModelDataField } from '../interfaces/icommons-managed-model-data-field';

import { ECommonsAdamantineManagedDataType, isECommonsAdamantineManagedDataType } from '../enums/ecommons-adamantine-managed-data-type';

export type TCommonsManagedModelDataField = Readonly<
		Omit<ICommonsManagedModelDataField, 'description' | 'suffix' | 'helper' | 'options' | 'alpha' | 'defaultValue' >
>;

type TCommonsManagedModelDataFieldWithDescription = TCommonsManagedModelDataField & {
		description: string;
};
type TCommonsManagedModelDataFieldWithOptions = TCommonsManagedModelDataField & {
		options: string[];
};
type TCommonsManagedModelDataFieldWithSuffix = TCommonsManagedModelDataField & {
		suffix: string;
};
type TCommonsManagedModelDataFieldWithHelper = TCommonsManagedModelDataField & {
		helper: string;
};
type TCommonsManagedModelDataFieldWithAlpha = TCommonsManagedModelDataField & {
		alpha: boolean;
};
type TCommonsManagedModelDataFieldWithDefaultValue = TCommonsManagedModelDataField & {
		defaultValue: string|number|boolean;
};

export function isTCommonsManagedModelDataField(
		test: unknown
): test is TCommonsManagedModelDataField {
	if (!CommonsType.hasPropertyString(test, 'name')) return false;
	if (!CommonsType.hasPropertyEnum<ECommonsAdamantineManagedDataType>(test, 'type', isECommonsAdamantineManagedDataType)) return false;
	if (!CommonsType.hasPropertyBoolean(test, 'optional')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'description')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'suffix')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'helper')) return false;
	if (!CommonsType.hasPropertyStringArrayOrUndefined(test, 'options')) return false;
	if (!CommonsType.hasPropertyBooleanOrUndefined(test, 'alpha')) return false;
	
	if (
			!CommonsType.hasPropertyNumberOrUndefined(test, 'defaultValue')
			&& !CommonsType.hasPropertyStringOrUndefined(test, 'defaultValue')
			&& !CommonsType.hasPropertyBooleanOrUndefined(test, 'defaultValue')
	) return false;
	
	return true;
}

export function encodeICommonsManagedModelDataField(data: ICommonsManagedModelDataField): TCommonsManagedModelDataField {
	const encoded: TCommonsManagedModelDataField = {
			name: data.name,
			type: data.type,
			optional: data.optional
	};
	
	if (data.description !== undefined) {
		(encoded as TCommonsManagedModelDataFieldWithDescription).description = data.description;
	}
	
	if (data.suffix !== undefined) {
		(encoded as TCommonsManagedModelDataFieldWithSuffix).suffix = data.suffix;
	}
	
	if (data.helper !== undefined) {
		(encoded as TCommonsManagedModelDataFieldWithHelper).helper = data.helper;
	}
	
	if (data.options !== undefined) {
		(encoded as TCommonsManagedModelDataFieldWithOptions).options = data.options;
	}
	
	if (data.alpha !== undefined) {
		(encoded as TCommonsManagedModelDataFieldWithAlpha).alpha = data.alpha;
	}
	
	if (data.defaultValue !== undefined) {
		(encoded as TCommonsManagedModelDataFieldWithDefaultValue).defaultValue = data.defaultValue;
	}
	
	return encoded;
}

export function decodeICommonsManagedModelDataField(encoded: TCommonsManagedModelDataField): ICommonsManagedModelDataField {
	const decoded: ICommonsManagedModelDataField = {
			// tslint:disable:object-literal-sort-keys
			name: encoded.name,
			type: encoded.type,
			optional: encoded.optional
			// tslint:enable:object-literal-sort-keys
	};
	
	if ((encoded as TCommonsManagedModelDataFieldWithDescription).description !== undefined) decoded.description = (encoded as TCommonsManagedModelDataFieldWithDescription).description;
	if ((encoded as TCommonsManagedModelDataFieldWithSuffix).suffix) decoded.suffix = (encoded as TCommonsManagedModelDataFieldWithSuffix).suffix;
	if ((encoded as TCommonsManagedModelDataFieldWithHelper).helper) decoded.helper = (encoded as TCommonsManagedModelDataFieldWithHelper).helper;
	if ((encoded as TCommonsManagedModelDataFieldWithOptions).options !== undefined) decoded.options = (encoded as TCommonsManagedModelDataFieldWithOptions).options;
	if ((encoded as TCommonsManagedModelDataFieldWithAlpha).alpha !== undefined) decoded.alpha = (encoded as TCommonsManagedModelDataFieldWithAlpha).alpha;
	if ((encoded as TCommonsManagedModelDataFieldWithDefaultValue).defaultValue !== undefined) decoded.defaultValue = (encoded as TCommonsManagedModelDataFieldWithDefaultValue).defaultValue;
	
	return decoded;
}
