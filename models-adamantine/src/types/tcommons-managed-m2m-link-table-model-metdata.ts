import { CommonsType } from 'tscommons-core';

import { ICommonsManagedM2MLinkTableModelMetadata } from '../interfaces/icommons-managed-m2m-link-table-model-metadata';
import { ICommonsManagedModelMetadata } from '../interfaces/icommons-managed-model-metadata';

import { TCommonsManagedModelMetadata, isTCommonsManagedModelMetadata, encodeICommonsManagedModelMetadata, decodeICommonsManagedModelMetadata } from './tcommons-managed-model-metadata';

export type TCommonsManagedM2MLinkTableModelMetadata = TCommonsManagedModelMetadata
		& Pick<
				ICommonsManagedM2MLinkTableModelMetadata,
				'aFieldName' | 'bFieldName'
		>;

export function isTCommonsManagedM2MLinkTableModelMetadata(test: unknown): test is TCommonsManagedM2MLinkTableModelMetadata {
	if (!isTCommonsManagedModelMetadata(test)) return false;
	
	if (!CommonsType.hasPropertyString(test, 'aFieldName')) return false;
	if (!CommonsType.hasPropertyString(test, 'bFieldName')) return false;
	
	return true;
}

export function encodeICommonsManagedM2MLinkTableModelMetadata(data: ICommonsManagedM2MLinkTableModelMetadata): TCommonsManagedM2MLinkTableModelMetadata {
	const step1: TCommonsManagedModelMetadata = encodeICommonsManagedModelMetadata(data);
	
	return {
			...step1,
			aFieldName: data.aFieldName,
			bFieldName: data.bFieldName
	};
}

export function decodeICommonsManagedM2MLinkTableModelMetadata(encoded: TCommonsManagedM2MLinkTableModelMetadata): ICommonsManagedM2MLinkTableModelMetadata {
	const step1: ICommonsManagedModelMetadata = decodeICommonsManagedModelMetadata(encoded);
	
	return {
			...step1,
			aFieldName: encoded.aFieldName,
			bFieldName: encoded.bFieldName
	};
}
