import { CommonsType } from 'tscommons-core';
import { ICommonsFirstClass, isICommonsFirstClass } from 'tscommons-models';

import { ICommonsUniqueNamed } from './icommons-unique-named';

export interface ICommonsManaged extends ICommonsFirstClass, ICommonsUniqueNamed {
		name: string;
}

export function isICommonsManaged(test: unknown): test is ICommonsManaged {
	if (!isICommonsFirstClass(test)) return false;
	
	if (!CommonsType.hasPropertyString(test, 'name')) return false;

	return true;
}
