import { CommonsType } from 'tscommons-core';

import { ICommonsManagedModelMetadata, isICommonsManagedModelMetadata } from './icommons-managed-model-metadata';

export interface ICommonsManagedM2MLinkTableModelMetadata extends ICommonsManagedModelMetadata {
		aFieldName: string;
		bFieldName: string;
}

export function isICommonsManagedM2MLinkTableModelMetadata(test: unknown): test is ICommonsManagedM2MLinkTableModelMetadata {
	if (!isICommonsManagedModelMetadata(test)) return false;
	
	if (!CommonsType.hasPropertyString(test, 'aFieldName')) return false;
	if (!CommonsType.hasPropertyString(test, 'bFieldName')) return false;

	return true;
}
