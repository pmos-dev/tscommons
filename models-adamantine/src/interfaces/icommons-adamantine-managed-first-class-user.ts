import { ICommonsUser, isICommonsUser } from 'tscommons-session';

import { ICommonsManaged, isICommonsManaged } from './icommons-managed';
import { ICommonsAccess, isICommonsAccess } from './icommons-access';

export interface ICommonsAdamantineManagedFirstClassUser extends ICommonsManaged, ICommonsUser, ICommonsAccess {}

export function isICommonsAdamantineManagedFirstClassUser(
		test: unknown
): test is ICommonsAdamantineManagedFirstClassUser {
	if (!isICommonsManaged(test)) return false;
	if (!isICommonsUser(test)) return false;
	if (!isICommonsAccess(test)) return false;
	
	return true;
}
