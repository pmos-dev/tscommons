import { CommonsType } from 'tscommons-core';

import { ECommonsAdamantineAccess, isECommonsAdamantineAccess } from '../enums/ecommons-adamantine-access';

import { ICommonsManagedModelDataField, isICommonsManagedModelDataField } from './icommons-managed-model-data-field';

export interface ICommonsManagedModelMetadata {
		manageableFields: ICommonsManagedModelDataField[];
		accessRequiredToManage: ECommonsAdamantineAccess;
		isAliasingSupported?: boolean;
}

export function isICommonsManagedModelMetadata(test: unknown): test is ICommonsManagedModelMetadata {
	if (!CommonsType.hasPropertyTArray<ICommonsManagedModelDataField>(test, 'manageableFields', isICommonsManagedModelDataField)) return false;
	if (!CommonsType.hasPropertyEnum<ECommonsAdamantineAccess>(test, 'accessRequiredToManage', isECommonsAdamantineAccess)) return false;
	if (!CommonsType.hasPropertyBooleanOrUndefined(test, 'isAliasingSupported')) return false;

	return true;
}
