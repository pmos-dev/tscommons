import { CommonsType } from 'tscommons-core';

import { ECommonsAdamantineManagedDataType, isECommonsAdamantineManagedDataType } from '../enums/ecommons-adamantine-managed-data-type';

export interface ICommonsManagedModelDataField {
		name: string;
		type: ECommonsAdamantineManagedDataType;
		optional: boolean;
		description?: string;
		suffix?: string;
		helper?: string;
		options?: string[];
		alpha?: boolean;
		defaultValue?: number|string|boolean|undefined;
}

export function isICommonsManagedModelDataField(
		test: unknown
): test is ICommonsManagedModelDataField {
	if (!CommonsType.hasPropertyString(test, 'name')) return false;
	if (!CommonsType.hasPropertyEnum<ECommonsAdamantineManagedDataType>(test, 'type', isECommonsAdamantineManagedDataType)) return false;
	if (!CommonsType.hasPropertyBoolean(test, 'optional')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'description')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'suffix')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'helper')) return false;
	if (!CommonsType.hasPropertyStringArrayOrUndefined(test, 'options')) return false;
	if (!CommonsType.hasPropertyBooleanOrUndefined(test, 'alpha')) return false;

	if (
			!CommonsType.hasPropertyNumberOrUndefined(test, 'defaultValue')
			&& !CommonsType.hasPropertyStringOrUndefined(test, 'defaultValue')
			&& !CommonsType.hasPropertyBooleanOrUndefined(test, 'defaultValue')
	) return false;

	return true;
}
