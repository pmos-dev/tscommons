import { ICommonsModel } from 'tscommons-models';

// tslint:disable-next-line:no-empty-interface
export interface ICommonsUniqueNamed extends ICommonsModel {}
