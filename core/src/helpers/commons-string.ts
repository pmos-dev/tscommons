const ALPHABET_CHARACTERS: string[] = 'abcdefghijklmnopqrstuvwxyz'.split('');
const VOWEL_CHARACTERS: string[] = 'aeiou'.split('');
const CONSONANT_CHARACTERS: string[] = ALPHABET_CHARACTERS
		.filter((c: string): boolean => !VOWEL_CHARACTERS.includes(c));

export abstract class CommonsString {
	// Casing methods

	public static ucWords(value: string): string {
		// easier to do this via a loop rather than a regex

		let lastWord: boolean = false;
		return value
				.toString()
				.split('')
				.map((c: string): string => {
					const isWord: boolean = /^[A-Z]$/i.test(c);
					try {
						if (!isWord || lastWord) return c;
						return c.toUpperCase();
					} finally {
						lastWord = isWord;
					}
				})
				.join('');
	}
	
	public static camelCase(value: string, ucFirst: boolean = true): string {
		return value
				.toString()
				.replace(/([A-Z])/g, ' $1')
				.replace(/[^a-z0-9]/ig, ' ')
				.replace(/[ ]{2,}/, ' ')
				.trim()
				.split(' ')
				.map((s: string): string => s.trim())
				.filter((s: string): boolean => s !== '')
				.map((s: string, index: number): string => {
					if (index === 0 && !ucFirst) return s.toLowerCase();
					if (s.length < 2) return s.toUpperCase();
					return s.substring(0, 1).toUpperCase() + s.substring(1);
				})
				.join('');
	}
	
	private static delimeterCase(value: string, delimeter: string) {
		return value
				.toString()
				.replace(/([A-Z])/g, ' $1')
				.replace(/[^a-z0-9]/ig, ' ')
				.replace(/[ ]{2,}/, ' ')
				.trim()
				.split(' ')
				.map((s: string): string => s.trim().toLowerCase())
				.filter((s: string): boolean => s !== '')
				.join(delimeter);
	}
	
	public static snakeCase(value: string): string {
		return CommonsString.delimeterCase(value, '_');
	}
	
	public static kebabCase(value: string): string {
		return CommonsString.delimeterCase(value, '-');
	}
	
	public static dashedCase(value: string): string {
		return CommonsString.kebabCase(value);
	}
	
	public static regexEscapeString(term: string): string {
		return term
				.replace(/[.]/g, '\\.')
				.replace(/[?]/g, '[?]')
				.replace(/[*]/g, '[*]')
				.replace(/[+]/g, '[+]')
				.replace(/[(]/g, '\\(')
				.replace(/[)]/g, '\\)')
				.replace(/\[/g, '\\[)')
				.replace(/\]/g, '\\]')
				.replace(/[{]/g, '[{]')
				.replace(/[}]/g, '[}]');
	}
	
	// Simple character, word and sentence methods
	
	public static isAlphabet(c: string): boolean {
		return c.length === 1
				&& ALPHABET_CHARACTERS.includes(c.toLowerCase());
	}

	public static isVowel(c: string): boolean {
		return CommonsString.isAlphabet(c)
				&& VOWEL_CHARACTERS.includes(c.toLowerCase());
	}

	public static isConsonant(c: string): boolean {
		return CommonsString.isAlphabet(c)
				&& CONSONANT_CHARACTERS.includes(c.toLowerCase());
	}
	
	public static regexLike(term: string): string {
		return `^${CommonsString.regexEscapeString(term)}$`
				.replace(/%/g, '(?:.|\\s)*');
	}
	
	public static rtrim(value: string, characters: string = ' \r\n\0'): string {
		const chars: string[] = characters.split('');
		if (chars.length === 0) return value;
		
		while (value.length > 0 && chars.includes(value.substr(value.length - 1, 1))) {
			value = value.substr(0, value.length - 1);
		}
		
		return value;
	}
	
	public static ltrim(value: string, characters: string = ' \r\n\0'): string {
		const chars: string[] = characters.split('');
		if (chars.length === 0) return value;
		
		while (value.length > 0 && chars.includes(value.substr(0, 1))) {
			value = value.substr(1);
		}
		
		return value;
	}

	public static trim(value: string, characters: string = ' \r\n\0'): string {
		return CommonsString.ltrim(CommonsString.rtrim(value, characters), characters);
	}
	
	// Complex word and sentence methods
	
	public static limitLength(value: string, length: number = 128, soft: boolean = false): string {
		if (value.length <= length) return value;

		let hard: string = value.substr(0, length - 3);	// -2 = make room for ...
		if (!soft) return `${hard}...`;

		const fallback: string = hard;
		
		while (hard.length > 1) {
			if (/[^a-zA-Z0-9]/.test(hard.slice(-1))) return `${hard.substr(0, hard.length - 1)}...`;
			hard = hard.substr(0, hard.length - 1);
		}
		
		return `${fallback}...`;
	}
	
	public static isCapitalised(value: string): boolean {
		return /^[A-Z]+$/.test(value.replace(/[^a-z]/i, ''));
	}

	private static capitaliseString(value: string, capitalised: boolean): string {
		return capitalised ? value.toUpperCase() : value;
	}
	
	public static pluralise(singular: string, quantity: number = 2, autoDetectEndings: boolean = true): string {
		if (singular.length === 0) return '';
		
		if (quantity === 1) return singular;
		if (quantity === 0) return CommonsString.pluralise(singular, 2, autoDetectEndings);
		
		const capitalised: boolean = CommonsString.isCapitalised(singular);
		
		const last: string = singular.charAt(singular.length - 1);
		if (!/[a-z]/i.test(last)) {
			// doesn't end with a-z
			return `${singular}${CommonsString.capitaliseString('s', capitalised)}`;
		}
		
		if (autoDetectEndings) {
			const same: string[] = `
				accommodation advice alms aluminum ammends
				baggage barracks binoculars bison bourgeois breadfruit
				cannon caribou cattle chalk chassis chinos clippers clothes clothing cod concrete corps correspondence crossroads
				deer dice doldrums dozen dungarees
				education eggfruit elk eyeglasses
				flour food fruit furniture
				gallows glasses goldfish grapefruit greenfly grouse gymnastics
				haddock halibut headquarters help homework
				ides information insignia
				jackfruit jeans
				kennels knickers knowledge kudos
				leggings lego luggage
				means monkfish moose mullet music
				nailclippers news
				offspring oxygen
				pants passionfruit pike pliers police premises public pyjamas
				reindeer rendezvous
				salmon scenery scissors series shambles sheep shellfish shorts shrimp smithereens species squid staff starfruit sugar swine
				tongs trousers trout tuna tweezers
				wheat whitebait wood
				you
			`
					.replace(/\s+/, ' ')
					.split(' ')
					.map((word: string): string => CommonsString.trim(word))
					.filter((word: string): boolean => word !== '');
			if (same.includes(singular.toLowerCase())) return singular;

			switch (singular.toLowerCase()) {
				case 'cafe':	return CommonsString.capitaliseString('cafes', capitalised);
				case 'child':	return CommonsString.capitaliseString('children', capitalised);
				case 'woman':	return CommonsString.capitaliseString('women', capitalised);
				case 'man':	return CommonsString.capitaliseString('men', capitalised);
				case 'mouse':	return CommonsString.capitaliseString('mice', capitalised);
				case 'goose':	return CommonsString.capitaliseString('geese', capitalised);
				case 'potato':	return CommonsString.capitaliseString('potatoes', capitalised);
			}
			
			if (/(craft|ies)$/.test(singular.toLowerCase())) return singular;

			if (/(ch|x|s|sh)$/i.test(singular)) return `${singular}${CommonsString.capitaliseString('es', capitalised)}`;

			const regex1: RegExpExecArray|null = /^(.+)(f|fe)$/i.exec(singular);
			if (regex1 !== null) return `${regex1[1]}${CommonsString.capitaliseString('ves', capitalised)}`;
			
			const regex2: RegExpExecArray|null = /^(.+[abcdfghjklmnpqrstvwxyz])y$/i.exec(singular);
			if (regex2 !== null) return `${regex2[1]}${CommonsString.capitaliseString('ies', capitalised)}`;
		}
		
		return `${singular}${CommonsString.capitaliseString('s', capitalised)}`;
	}
	
	private static noOrNumber(quantity: number, none: boolean = false, capitaliseNoNone: boolean = false): string {
		if (quantity === 0) {
			const wording: string = none ? 'none' : 'no';
			return capitaliseNoNone ? CommonsString.ucWords(wording) : wording;
		}
		
		return quantity.toString();
	}
	
	public static quantify(singular: string, quantity: number, autoDetectEndings: boolean = true, capitaliseNoNone: boolean = false): string {
		if (singular.length === 0) return CommonsString.noOrNumber(quantity, true, capitaliseNoNone);
		
		const capitalised: boolean = CommonsString.isCapitalised(singular);

		return CommonsString.capitaliseString(`${CommonsString.noOrNumber(quantity, false, capitaliseNoNone)} ${CommonsString.pluralise(singular, quantity, autoDetectEndings)}`, capitalised);
	}
	
	private static isVowelOrY(c: string): boolean {
		return CommonsString.isVowel(c) || c.toLowerCase() === 'y';
	}

	private static isConsonantNotY(c: string): boolean {
		return CommonsString.isConsonant(c) && c.toLowerCase() !== 'y';
	}
	
	public static splitWords(sentence: string, allowHyphens: boolean = true, allowApostrophies: boolean = false): string[] {
		if (!allowHyphens) sentence = sentence.replace('-', ' - ');
		if (!allowApostrophies) sentence = sentence.replace(`'`, ` ' `);
		
		return sentence
				.split(/[^-'A-Za-z]/)
				.map((word: string): string => CommonsString.trim(word))
				.filter((word: string): boolean => word !== '');
	}

	public static internalRoughSyllables(word: string): string[] {
		// assumes that syllables consist of vowels in the center of each syllable
		
		const chars: string[] = CommonsString.trim(word.toLowerCase()).split('');

		const stack: string[] = [];
		let allowConsume: boolean = false;
		let build: string = '';
		let reset: boolean = true;

		while (true) {
			const c: string|undefined = chars.shift();
			if (c === undefined) break;

			if (!CommonsString.isAlphabet(c)) {	// non-character
				if (build.length > 0) stack.push(build);
				build = '';
				reset = true;

				allowConsume = false;	// don't consume future single consonant
				continue;
			}

			const isVowel: boolean = CommonsString.isVowelOrY(c);

			if (allowConsume && isVowel) {	// double vowel; concatinate with last
				stack.push(`${stack.pop()}${c}`);

				allowConsume = true;	// consume future single consonant
				continue;
			}

			if (allowConsume && !isVowel) {
				// append to last stacked word
				stack.push(`${stack.pop()}${c}`);

				allowConsume = false;	// don't consume future single consonant
				continue;
			}

			if (isVowel) {
				if (chars.length === 0 && stack.length > 0 && !reset) {
					// absorb terminal vowels
					stack.push(`${stack.pop()}${c}`);
					break;
				}

				stack.push(`${build}${c}`);
				build = '';

				allowConsume = true;
				continue;
			}

			build += c;
		}

		if (build !== '') {
			if (build.length > 1) {
				stack.push(build);
			} else {
				stack.push(`${stack.pop()}${build}`);
			}
		}

		// attempt to make syllables start with consonants where possible
		for (let i = 1; i < stack.length; i++) {
			if (stack[i - 1].length < 2) continue;

			if (CommonsString.isVowelOrY(stack[i].charAt(0)) && CommonsString.isConsonantNotY(stack[i - 1].slice(-1))) {
				stack[i] = `${stack[i - 1].slice(-1)}${stack[i]}`;
				stack[i - 1] = stack[i - 1].slice(0, stack[i - 1].length - 1);
			}
		}

		return stack;
	}

	public static roughSyllables(wordOrWords: string): string[] {
		// assumes that syllables consist of vowels in the center of each syllable
		
		// it gets complicated trying to cater for multiple words, so we just split and process for each
		const total: string[] = [];
		
		for (const word of CommonsString.splitWords(wordOrWords)) {
			const syllables: string[] = CommonsString.internalRoughSyllables(word);
			for (const syllable of syllables) total.push(syllable);
		}
		
		return total;
	}
	
	public static splitSentences(paragraph: string, breakChars: RegExp = /[.!?]/, allowDecimals: boolean = true, breakOnMultipleHyphens: boolean = true): string[] {
		if (breakOnMultipleHyphens) {
			// multiple hyphens count as breaks; single count as hyphenated words
			paragraph = paragraph.replace(/-{2,}/g, '. ');
		}
		
		if (!breakChars.test('\r')) {
			paragraph = paragraph.replace(/\r/, ' ');
		}
		if (!breakChars.test('\n')) {
			paragraph = paragraph.replace(/\n/, ' ');
		}
		
		// We don't use split, as we want to preserve the break characters
		const chars: string[] = CommonsString.trim(paragraph).split('');

		const sentences: string[] = [];
		let build: string = '';
		while (true) {
			const c: string|undefined = chars.shift();
			if (c === undefined) break;
			
			if (breakChars.test(c)) {
				if (build === '' && sentences.length > 0) {
					// concatenate multiple break characters onto the end of the last sentence
					sentences.push(`${sentences.pop()}${c}`);
					continue;
				}

				if (CommonsString.trim(build) === '') {
					// nothing to push
					build = '';
					continue;
				}
				
				if (
						!allowDecimals
						|| !/[-0-9]/.test(build.slice(-1))
						|| chars.length === 0
						|| !/[0-9]/.test(chars[0])
				) {
					sentences.push(`${CommonsString.ltrim(build)}${c}`);
					build = '';
					continue;
				}
			}
			
			build += c;
		}
		if (CommonsString.trim(build) !== '') sentences.push(CommonsString.trim(build));

		return sentences;
	}
	
	public static fleschKincaidReadingEase(paragraph: string): number {
		const words: string[] = CommonsString.splitWords(paragraph);
		const sentences: string[] = CommonsString.splitSentences(paragraph);
		const syllables: string[] = CommonsString.roughSyllables(paragraph);
		
		return 206.835 - (1.015 * (words.length / sentences.length)) - (84.6 * (syllables.length / words.length));
	}

	public static automatedReadabilityIndex(paragraph: string): number {
		const words: string[] = CommonsString.splitWords(paragraph);
		const sentences: string[] = CommonsString.splitSentences(paragraph);
		
		const characters: string[] = [];
		for (const word of words) {
			for (const c of word.split('')) {
				if (CommonsString.isAlphabet(c)) characters.push(c);
			}
		}

		return (4.71 * (characters.length / words.length)) + (0.5 * (words.length / sentences.length)) - 21.43;
	}
}
