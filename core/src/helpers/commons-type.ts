import { TDefined } from '../types/tdefined';
import { TPrimative } from '../types/tprimative';
import { TPropertyObject } from '../types/tproperty-object';
import { TEncoded, TEncodedArray, TEncodedObject } from '../types/tencoded';
import { TKeyObject } from '../types/tkey-object';
import { TEnumObject } from '../types/tenum-object';

import { COMMONS_REGEX_PATTERN_DATE_ECMA } from '../consts/commons-regex';

import { CommonsDate } from './commons-date';

export enum EVariableType {
	STRING = 'string',
	NUMBER = 'number',
	BOOLEAN = 'boolean',
	OBJECT = 'object',
	DATE = 'date'
}

export abstract class CommonsType {
	// Simple type guards and assertions

	public static isDefined(test: unknown): test is TDefined {
		return test !== undefined;
	}

	public static assertDefined(test: unknown): TDefined {
		if (!CommonsType.isDefined(test)) throw new Error('Assertion fail: variable is undefined');
		return test;
	}

	public static isPrimative(test: unknown): test is TPrimative {
		if (!CommonsType.isDefined(test)) return false;
	
		if (test === null) return false;
		
		switch (typeof test) {
			case 'string':
			case 'number':
			case 'boolean':
				return true;
			default:
				return false;
		}
	}

	public static assertPrimative(test: unknown): TPrimative {
		if (!CommonsType.isPrimative(test)) throw new Error('Assertion fail: variable is not a primative');
		return test;
	}
	
	public static isString(test: unknown): test is string {
		return CommonsType.isPrimative(test) && 'string' === typeof test;
	}

	public static assertString(test: unknown): string {
		if (!CommonsType.isString(test)) throw new Error('Assertion fail: variable is not a string');
		return test;
	}
	
	public static attemptString(value: unknown): string|undefined {
		if (CommonsType.isString(value)) return value;
		
		if (value === undefined) return undefined;
		if (value === null) return undefined;
		if ('undefined' === typeof value) return undefined;

		if (CommonsType.isArray(value)) {
			return value
					.map((item: unknown): string|undefined => CommonsType.attemptString(item))
					.filter((item: string|undefined): boolean => item !== undefined)
					.join(', ');
		}
		
		if ('object' === typeof value) return JSON.stringify(value);
		
		if ('number' === typeof value) return value.toString();
		
		if ('boolean' === typeof value) return value ? 'true' : 'false';
		
		return String(value);
	}

	public static isNumber(test: unknown): test is number {
		return CommonsType.isPrimative(test) && 'number' === typeof test;
	}

	public static assertNumber(test: unknown): number {
		if (!CommonsType.isNumber(test)) throw new Error('Assertion fail: variable is not a number');
		return test;
	}
	
	public static attemptNumber(value: unknown): number|undefined {
		if (CommonsType.isNumber(value)) return value;
		
		if (value === undefined) return undefined;
		if (value === null) return undefined;
		if ('undefined' === typeof value) return undefined;
		
		if ('string' === typeof value) {
			const attempt: number = value.indexOf('.') > -1 ? parseFloat(value) : parseInt(value, 10);
			if (Number.isNaN(attempt)) return undefined;
			
			return attempt;
		}
		
		if ('boolean' === typeof value) {
			return value ? 1 : 0;
		}

		return Number(value);
	}
	
	public static isBoolean(test: unknown): test is boolean {
		return CommonsType.isPrimative(test) && 'boolean' === typeof test;
	}

	public static assertBoolean(test: unknown): boolean {
		if (!CommonsType.isBoolean(test)) throw new Error('Assertion fail: variable is not a boolean');
		return test;
	}
	
	public static attemptBoolean(value: unknown): boolean|undefined {
		if (CommonsType.isBoolean(value)) return value;
		
		if (value === undefined) return undefined;
		if (value === null) return undefined;
		if ('undefined' === typeof value) return undefined;
		
		if (!CommonsType.isPrimative(value)) return undefined;

		return CommonsType.checkboxBoolean(value);
	}
	
	public static isObject(test: unknown): test is {} {
		if (!CommonsType.isDefined(test)) return false;
	
		if (test === null) return false;

		return 'object' === typeof test;
	}

	public static assertObject(test: unknown): {} {
		if (!CommonsType.isObject(test)) throw new Error('Assertion fail: variable is not an object');
		return test;
	}
	
	public static attemptObject(value: unknown): {}|undefined {
		if (CommonsType.isObject(value)) return value;
		
		if (value === undefined) return undefined;
		if (value === null) return undefined;
		if ('undefined' === typeof value) return undefined;

		if ('string' === typeof value) {
			try {
				const attempt: unknown = JSON.parse(value);
				
				if ('object' === typeof attempt) return attempt as {};

				return undefined;
			} catch (e) {
				return undefined;
			}
		}
		
		if ('object' !== typeof value || CommonsType.isArray(value)) return undefined;
		
		return value as {};
	}
	
	public static isDate(test: unknown): test is Date {
		return CommonsType.isObject(test) && test instanceof Date;
	}

	public static assertDate(test: unknown): Date {
		if (!CommonsType.isDate(test)) throw new Error('Assertion fail: variable is not a Date object');
		return test;
	}
	
	public static attemptDate(value: unknown, iso: boolean = false): Date|undefined {
		if (CommonsType.isDate(value)) return value;
		
		if (value === undefined) return undefined;
		if (value === null) return undefined;
		if ('undefined' === typeof value) return undefined;
		
		if ('string' === typeof value) {
			if (CommonsDate.isYmdHis(value)) return CommonsDate.YmdHisToDate(value, iso);
			if (CommonsDate.isdmYHi(value)) return CommonsDate.dmYHiToDate(value, iso);
			if (CommonsDate.isYmd(value)) return CommonsDate.YmdToDate(value, iso);
			if (CommonsDate.isdmY(value)) return CommonsDate.dmYToDate(value, iso);
			if (CommonsDate.isHis(value)) return CommonsDate.HisToDate(value, iso);
			if (CommonsDate.isHi(value)) return CommonsDate.HiToDate(value, iso);
			
			const attempt: number = Date.parse(value);
			if (Number.isNaN(attempt)) return undefined;
			
			return new Date(attempt);
		}

		return undefined;
	}
	
	public static isError(test: unknown): test is Error {
		return CommonsType.isObject(test) && test instanceof Error;
	}

	public static assertError(test: unknown): Error {
		if (!CommonsType.isError(test)) throw new Error('Assertion fail: variable is not an Error object');
		return test;
	}
	
	public static isArray(test: unknown): test is unknown[] {
		return CommonsType.isObject(test) && Array.isArray(test);
	}

	public static assertArray(test: unknown): unknown[] {
		if (!CommonsType.isArray(test)) throw new Error('Assertion fail: variable is not an array');
		return test;
	}

	private static validateTKeyObject<T>(test: unknown, checker: (t: unknown) => t is T): test is TKeyObject<T> {
		if (!CommonsType.isObject(test)) return false;
		
		for (const key of Object.keys(test)) {
			if (!checker(test[key])) return false;
		}
		
		return true;
	}
	
	private static validateTOrUndefinedKeyObject<T>(test: unknown, checker: (t: unknown) => t is T): test is TKeyObject<T|undefined> {
		if (!CommonsType.isObject(test)) return false;
		
		for (const key of Object.keys(test)) {
			// NB can't use hasPropertyTOrUndefined, as that requires T to be an object, and we also use this method for primatives
			if (!CommonsType.hasProperty(test, key) || test[key] === undefined) continue;

			if (!checker(test[key])) return false;
		}
		
		return true;
	}
	
	private static validateTEnumObject<E extends string, T>(
			test: unknown,
			valueChecker: (t: unknown) => t is T,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, T> {
		if (!CommonsType.isObject(test)) return false;
		
		for (const key of Object.keys(test)) {
			if (!keyChecker(key)) return false;
			if (!valueChecker(test[key as string])) return false;
		}
		
		return true;
	}
	
	private static validateTOrUndefinedEnumObject<E extends string, T>(
			test: unknown,
			valueChecker: (t: unknown) => t is T,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, T|undefined> {
		if (!CommonsType.isObject(test)) return false;
		
		for (const key of Object.keys(test)) {
			if (!keyChecker(key)) return false;

			// NB can't use hasPropertyTOrUndefined, as that requires T to be an object, and we also use this method for primatives
			if (!CommonsType.hasProperty(test, key) || test[key] === undefined) continue;

			if (!valueChecker(test[key as string])) return false;
		}
		
		return true;
	}
	
	public static isDefinedArray(test: unknown): test is TDefined[] {
		if (!CommonsType.isArray(test)) return false;

		for (const subtest of test) {
			if (!CommonsType.isDefined(subtest)) return false;
		}
	
		return true;
	}
	
	public static assertDefinedArray(test: unknown): TDefined[] {
		if (!CommonsType.isDefinedArray(test)) throw new Error('Assertion fail: variable is not a defined array');
		return test;
	}
	
	public static isDefinedKeyObject(test: unknown): test is TKeyObject<TDefined> {
		if (!CommonsType.isObject(test)) return false;

		for (const key of Object.keys(test)) {
			if (!CommonsType.isDefined(test[key])) return false;
		}
	
		return true;
	}
	
	public static assertDefinedKeyObject(test: unknown): TKeyObject<TDefined> {
		if (!CommonsType.isDefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a defined key object');
		return test;
	}

	public static isDefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, TDefined> {
		if (!CommonsType.isObject(test)) return false;

		for (const key of Object.keys(test)) {
			if (!keyChecker(key)) return false;
			if (!CommonsType.isDefined(test[key as string])) return false;
		}
	
		return true;
	}
	
	public static assertDefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, TDefined> {
		if (!CommonsType.isDefinedEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a defined enum object');
		
		return test;
	}

	public static isPrimativeArray(test: unknown): test is TPrimative[] {
		if (!CommonsType.isDefinedArray(test)) return false;

		for (const subtest of test) {
			if (!CommonsType.isPrimative(subtest)) return false;
		}
	
		return true;
	}

	public static assertPrimativeArray(test: unknown): TPrimative[] {
		if (!CommonsType.isPrimativeArray(test)) throw new Error('Assertion fail: variable is not a primative array');
		return test;
	}
	
	public static isPrimativeKeyObject(test: unknown): test is TKeyObject<TPrimative> {
		if (!CommonsType.isObject(test)) return false;

		for (const key of Object.keys(test)) {
			if (!CommonsType.isPrimative(test[key])) return false;
		}
	
		return true;
	}
	
	public static assertPrimativeKeyObject(test: unknown): TKeyObject<TPrimative> {
		if (!CommonsType.isPrimativeKeyObject(test)) throw new Error('Assertion fail: variable is not a primative key object');
		return test;
	}
	
	public static isPrimativeEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, TPrimative> {
		if (!CommonsType.isObject(test)) return false;

		for (const key of Object.keys(test)) {
			if (!keyChecker(key)) return false;
			if (!CommonsType.isPrimative(test[key as string])) return false;
		}
	
		return true;
	}
	
	public static assertPrimativeEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, TPrimative> {
		if (!CommonsType.isPrimativeEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a primative enum object');
		
		return test;
	}
	
	public static isStringArray(test: unknown): test is string[] {
		if (!CommonsType.isPrimativeArray(test)) return false;

		for (const subtest of test) {
			if (!CommonsType.isString(subtest)) return false;
		}
	
		return true;
	}

	public static assertStringArray(test: unknown): string[] {
		if (!CommonsType.isStringArray(test)) throw new Error('Assertion fail: variable is not a string array');
		return test;
	}
	
	public static isStringKeyObject(test: unknown): test is TKeyObject<string> {
		return CommonsType.validateTKeyObject<string>(test, CommonsType.isString);
	}
	
	public static assertStringKeyObject(test: unknown): TKeyObject<string> {
		if (!CommonsType.isStringKeyObject(test)) throw new Error('Assertion fail: variable is not a string key object');
		return test;
	}
	
	public static isStringEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, string> {
		return CommonsType.validateTEnumObject<E, string>(
				test,
				CommonsType.isString,
				keyChecker
		);
	}
	
	public static assertStringEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, string> {
		if (!CommonsType.isStringEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a string enum object');
		
		return test;
	}
	
	public static isStringOrUndefinedKeyObject(test: unknown): test is TKeyObject<string|undefined> {
		return CommonsType.validateTOrUndefinedKeyObject<string>(test, CommonsType.isString);
	}
	
	public static assertStringOrUndefinedKeyObject(test: unknown): TKeyObject<string|undefined> {
		if (!CommonsType.isStringOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a string or undefined key object');
		return test;
	}
	
	public static isStringOrUndefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, string|undefined> {
		return CommonsType.validateTOrUndefinedEnumObject<E, string>(
				test,
				CommonsType.isString,
				keyChecker
		);
	}
	
	public static assertStringOrUndefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, string|undefined> {
		if (!CommonsType.isStringOrUndefinedEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a string or undefined key object');
		
		return test;
	}
	
	public static isStringArrayKeyObject(test: unknown): test is TKeyObject<string[]> {
		return CommonsType.validateTKeyObject<string[]>(test, CommonsType.isStringArray);
	}
	
	public static assertStringArrayKeyObject(test: unknown): TKeyObject<string[]> {
		if (!CommonsType.isStringArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a string array key object');
		return test;
	}
	
	public static isStringArrayEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, string[]> {
		return CommonsType.validateTEnumObject<E, string[]>(
				test,
				CommonsType.isStringArray,
				keyChecker
		);
	}
	
	public static assertStringArrayEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, string[]> {
		if (!CommonsType.isStringArrayEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a string array key object');
		
		return test;
	}
	
	public static isNumberArray(test: unknown): test is number[] {
		if (!CommonsType.isPrimativeArray(test)) return false;

		for (const subtest of test) {
			if (!CommonsType.isNumber(subtest)) return false;
		}
	
		return true;
	}

	public static assertNumberArray(test: unknown): number[] {
		if (!CommonsType.isNumberArray(test)) throw new Error('Assertion fail: variable is not a number array');
		return test;
	}
	
	public static isNumberKeyObject(test: unknown): test is TKeyObject<number> {
		return CommonsType.validateTKeyObject<number>(test, CommonsType.isNumber);
	}
	
	public static assertNumberKeyObject(test: unknown): TKeyObject<number> {
		if (!CommonsType.isNumberKeyObject(test)) throw new Error('Assertion fail: variable is not a number key object');
		return test;
	}
	
	public static isNumberEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, number> {
		return CommonsType.validateTEnumObject<E, number>(
				test,
				CommonsType.isNumber,
				keyChecker
		);
	}
	
	public static assertNumberEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, number> {
		if (!CommonsType.isNumberEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a number key object');
		return test;
	}
	
	public static isNumberOrUndefinedKeyObject(test: unknown): test is TKeyObject<number|undefined> {
		return CommonsType.validateTOrUndefinedKeyObject<number>(test, CommonsType.isNumber);
	}
	
	public static assertNumberOrUndefinedKeyObject(test: unknown): TKeyObject<number|undefined> {
		if (!CommonsType.isNumberOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a number or undefined key object');
		return test;
	}
	
	public static isNumberOrUndefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, number|undefined> {
		return CommonsType.validateTOrUndefinedEnumObject<E, number>(
				test,
				CommonsType.isNumber,
				keyChecker
		);
	}
	
	public static assertNumberOrUndefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, number|undefined> {
		if (!CommonsType.isNumberOrUndefinedEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a number or undefined key object');
		
		return test;
	}
	
	public static isNumberArrayKeyObject(test: unknown): test is TKeyObject<number[]> {
		return CommonsType.validateTKeyObject<number[]>(test, CommonsType.isNumberArray);
	}
	
	public static assertNumberArrayKeyObject(test: unknown): TKeyObject<number[]> {
		if (!CommonsType.isNumberArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a number array key object');
		return test;
	}
	
	public static isNumberArrayEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, number[]> {
		return CommonsType.validateTEnumObject<E, number[]>(
				test,
				CommonsType.isNumberArray,
				keyChecker
		);
	}
	
	public static assertNumberArrayEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, number[]> {
		if (!CommonsType.isNumberArrayEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a number array key object');
		
		return test;
	}
	
	public static isBooleanArray(test: unknown): test is boolean[] {
		if (!CommonsType.isPrimativeArray(test)) return false;

		for (const subtest of test) {
			if (!CommonsType.isBoolean(subtest)) return false;
		}
	
		return true;
	}

	public static assertBooleanArray(test: unknown): boolean[] {
		if (!CommonsType.isBooleanArray(test)) throw new Error('Assertion fail: variable is not a boolean array');
		return test;
	}
	
	public static isBooleanKeyObject(test: unknown): test is TKeyObject<boolean> {
		return CommonsType.validateTKeyObject<boolean>(test, CommonsType.isBoolean);
	}
	
	public static assertBooleanKeyObject(test: unknown): TKeyObject<boolean> {
		if (!CommonsType.isBooleanKeyObject(test)) throw new Error('Assertion fail: variable is not a boolean key object');
		return test;
	}
	
	public static isBooleanEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, boolean> {
		return CommonsType.validateTEnumObject<E, boolean>(
				test,
				CommonsType.isBoolean,
				keyChecker
		);
	}
	
	public static assertBooleanEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, boolean> {
		if (!CommonsType.isBooleanEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a boolean key object');
		
		return test;
	}
	
	public static isBooleanOrUndefinedKeyObject(test: unknown): test is TKeyObject<boolean|undefined> {
		return CommonsType.validateTOrUndefinedKeyObject<boolean>(test, CommonsType.isBoolean);
	}
	
	public static assertBooleanOrUndefinedKeyObject(test: unknown): TKeyObject<boolean|undefined> {
		if (!CommonsType.isBooleanOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a boolean or undefined key object');
		return test;
	}
	
	public static isBooleanOrUndefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, boolean|undefined> {
		return CommonsType.validateTOrUndefinedEnumObject<E, boolean>(
				test,
				CommonsType.isBoolean,
				keyChecker
		);
	}
	
	public static assertBooleanOrUndefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, boolean|undefined> {
		if (!CommonsType.isBooleanOrUndefinedEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a boolean or undefined key object');
		
		return test;
	}
	
	public static isBooleanArrayKeyObject(test: unknown): test is TKeyObject<boolean[]> {
		return CommonsType.validateTKeyObject<boolean[]>(test, CommonsType.isBooleanArray);
	}
	
	public static assertBooleanArrayKeyObject(test: unknown): TKeyObject<boolean[]> {
		if (!CommonsType.isBooleanArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a boolean array key object');
		return test;
	}
	
	public static isBooleanArrayEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, boolean[]> {
		return CommonsType.validateTEnumObject<E, boolean[]>(
				test,
				CommonsType.isBooleanArray,
				keyChecker
		);
	}
	
	public static assertBooleanArrayEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, boolean[]> {
		if (!CommonsType.isBooleanArrayEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a boolean array key object');
		
		return test;
	}
	
	public static isObjectArray(test: unknown): test is {}[] {
		if (!CommonsType.isDefinedArray(test)) return false;

		for (const subtest of test) {
			if (!CommonsType.isObject(subtest)) return false;
		}
	
		return true;
	}

	public static assertObjectArray(test: unknown): {}[] {
		if (!CommonsType.isObjectArray(test)) throw new Error('Assertion fail: variable is not an object array');
		return test;
	}
	
	public static isObjectKeyObject(test: unknown): test is TKeyObject<{}> {
		return CommonsType.validateTKeyObject<{}>(test, CommonsType.isObject);
	}
	
	public static assertObjectKeyObject(test: unknown): TKeyObject<{}> {
		if (!CommonsType.isObjectKeyObject(test)) throw new Error('Assertion fail: variable is not a object key object');
		return test;
	}
	
	public static isObjectEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, {}> {
		return CommonsType.validateTEnumObject<E, {}>(
				test,
				CommonsType.isObject,
				keyChecker
		);
	}
	
	public static assertObjectEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, {}> {
		if (!CommonsType.isObjectEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a object key object');
		
		return test;
	}
	
	public static isObjectOrUndefinedKeyObject(test: unknown): test is TKeyObject<{}|undefined> {
		return CommonsType.validateTOrUndefinedKeyObject<{}>(test, CommonsType.isObject);
	}
	
	public static assertObjectOrUndefinedKeyObject(test: unknown): TKeyObject<{}|undefined> {
		if (!CommonsType.isObjectOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a object or undefined key object');
		return test;
	}
	
	public static isObjectOrUndefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, {}|undefined> {
		return CommonsType.validateTOrUndefinedEnumObject<E, {}>(
				test,
				CommonsType.isObject,
				keyChecker
		);
	}
	
	public static assertObjectOrUndefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, {}|undefined> {
		if (!CommonsType.isObjectOrUndefinedEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a object or undefined key object');
		
		return test;
	}
	
	public static isObjectArrayKeyObject(test: unknown): test is TKeyObject<{}[]> {
		return CommonsType.validateTKeyObject<{}[]>(test, CommonsType.isObjectArray);
	}
	
	public static assertObjectArrayKeyObject(test: unknown): TKeyObject<{}[]> {
		if (!CommonsType.isObjectArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a object array key object');
		return test;
	}
	
	public static isObjectArrayEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, {}[]> {
		return CommonsType.validateTEnumObject<E, {}[]>(
				test,
				CommonsType.isObjectArray,
				keyChecker
		);
	}
	
	public static assertObjectArrayEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, {}[]> {
		if (!CommonsType.isObjectArrayEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a object array key object');
		
		return test;
	}
	
	public static isDateArray(test: unknown): test is Date[] {
		if (!CommonsType.isObjectArray(test)) return false;

		for (const subtest of test) {
			if (!CommonsType.isDate(subtest)) return false;
		}
	
		return true;
	}

	public static assertDateArray(test: unknown): Date[] {
		if (!CommonsType.isDateArray(test)) throw new Error('Assertion fail: variable is not a Date array');
		return test;
	}
	
	public static isDateKeyObject(test: unknown): test is TKeyObject<Date> {
		return CommonsType.validateTKeyObject<Date>(test, CommonsType.isDate);
	}
	
	public static assertDateKeyObject(test: unknown): TKeyObject<Date> {
		if (!CommonsType.isDateKeyObject(test)) throw new Error('Assertion fail: variable is not a Date key object');
		return test;
	}
	
	public static isDateEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, Date> {
		return CommonsType.validateTEnumObject<E, Date>(
				test,
				CommonsType.isDate,
				keyChecker
		);
	}
	
	public static assertDateEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, Date> {
		if (!CommonsType.isDateEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a Date key object');
		
		return test;
	}
	
	public static isDateOrUndefinedKeyObject(test: unknown): test is TKeyObject<Date|undefined> {
		return CommonsType.validateTOrUndefinedKeyObject<Date>(test, CommonsType.isDate);
	}
	
	public static assertDateOrUndefinedKeyObject(test: unknown): TKeyObject<Date|undefined> {
		if (!CommonsType.isDateOrUndefinedKeyObject(test)) throw new Error('Assertion fail: variable is not a Date or undefined key object');
		return test;
	}
	
	public static isDateOrUndefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, Date|undefined> {
		return CommonsType.validateTOrUndefinedEnumObject<E, Date>(
				test,
				CommonsType.isDate,
				keyChecker
		);
	}
	
	public static assertDateOrUndefinedEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, Date|undefined> {
		if (!CommonsType.isDateOrUndefinedEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a Date or undefined key object');
		
		return test;
	}
	
	public static isDateArrayKeyObject(test: unknown): test is TKeyObject<Date[]> {
		return CommonsType.validateTKeyObject<Date[]>(test, CommonsType.isDateArray);
	}
	
	public static assertDateArrayKeyObject(test: unknown): TKeyObject<Date[]> {
		if (!CommonsType.isDateArrayKeyObject(test)) throw new Error('Assertion fail: variable is not a Date array key object');
		return test;
	}
	
	public static isDateArrayEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, Date[]> {
		return CommonsType.validateTEnumObject<E, Date[]>(
				test,
				CommonsType.isDateArray,
				keyChecker
		);
	}
	
	public static assertDateArrayEnumObject<E extends string>(
			test: unknown,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, Date[]> {
		if (!CommonsType.isDateArrayEnumObject<E>(
				test,
				keyChecker
		)) throw new Error('Assertion fail: variable is not a Date array key object');
		
		return test;
	}
	
	// Object checkers and assertions

	public static isPropertyObject(test: unknown): test is TPropertyObject {
		if (!CommonsType.isObject(test)) return false;
		if (CommonsType.isArray(test) || CommonsType.isDate(test)) return false;
		
		return CommonsType.isStringArray(Object.keys(test));
	}

	public static assertPropertyObject(test: unknown): TPropertyObject {
		if (!CommonsType.isPropertyObject(test)) throw new Error('Assertion fail: variable is not a property object');
		return test;
	}
	
	public static isEncoded(test: unknown): test is TEncoded {
		if (!CommonsType.isDefined(test)) return false;
		if (test === null) return true;
		
		if (CommonsType.isPrimative(test)) return true;
		
		if (CommonsType.isDate(test)) return false;

		if (CommonsType.isArray(test)) {
			for (const item of test) {
				if (!CommonsType.isEncoded(item)) return false;
			}
			return true;
		}
		
		if (CommonsType.isObject(test)) {
			for (const property of Object.keys(test)) {
				if (!CommonsType.isDefined(test[property])) return false;
				if (!CommonsType.isEncoded(test[property])) return false;
			}
			
			return true;
		}
		
		// unknown
		return false;
	}
	
	public static isEncodedObject(test: unknown): test is TEncodedObject {
		if (!CommonsType.isPropertyObject(test)) return false;
		return CommonsType.isEncoded(test);
	}

	public static assertEncodedObject(test: unknown): TEncodedObject {
		if (!CommonsType.isEncodedObject(test)) throw new Error('Assertion fail: variable is not an encoded object');
		return test;
	}
	
	// Complex type guards and assertions
	
	public static isT<T>(test: unknown, checker: (t: unknown) => t is T): test is T {
		if (!CommonsType.isObject(test)) return false;
		return checker(test);
	}
	
	public static assertT<T>(test: unknown, checker: (t: unknown) => t is T, typeName: string): T {
		if (!CommonsType.isT<T>(test, checker)) throw new Error(`Assertion fail: variable is not an object of type ${typeName}`);
		return test;
	}
	
	public static isTArray<T>(test: unknown, checker: (t: unknown) => t is T): test is T[] {
		if (!CommonsType.isObjectArray(test)) return false;

		for (const subtest of test) {
			if (!CommonsType.isT<T>(subtest, checker)) return false;
		}
	
		return true;
	}
	
	public static assertTArray<T>(test: unknown, checker: (t: unknown) => t is T, typeName: string): T[] {
		if (!CommonsType.isTArray<T>(test, checker)) throw new Error(`Assertion fail: variable is not an array of objects of type ${typeName}`);
		return test;
	}
	
	public static isTKeyObject<T>(test: unknown, checker: (t: unknown) => t is T): test is TKeyObject<T> {
		return CommonsType.validateTKeyObject<T>(
				test,
				(t: unknown): t is T => CommonsType.isT<T>(t, checker)
		);
	}
	
	public static assertTKeyObject<T>(test: unknown, checker: (t: unknown) => t is T, typeName: string): TKeyObject<T> {
		if (!CommonsType.isTKeyObject<T>(test, checker)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${typeName}`);
		return test;
	}
	
	public static isTEnumObject<E extends string, T>(
			test: unknown,
			checker: (t: unknown) => t is T,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, T> {
		return CommonsType.validateTEnumObject<E, T>(
				test,
				(t: unknown): t is T => CommonsType.isT<T>(t, checker),
				keyChecker
		);
	}
	
	public static assertTEnumObject<E extends string, T>(
			test: unknown,
			checker: (t: unknown) => t is T,
			typeName: string,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, T> {
		if (!CommonsType.isTEnumObject<E, T>(
				test,
				checker,
				keyChecker
		)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${typeName}`);
		return test;
	}
	
	public static isTOrUndefinedKeyObject<T>(test: unknown, checker: (t: unknown) => t is T): test is TKeyObject<T|undefined> {
		return CommonsType.validateTOrUndefinedKeyObject<T>(
				test,
				(t: unknown): t is T => CommonsType.isT<T>(t, checker)
		);
	}
	
	public static assertTOrUndefinedKeyObject<T>(test: unknown, checker: (t: unknown) => t is T, typeName: string): TKeyObject<T|undefined> {
		if (!CommonsType.isTKeyObject<T>(test, checker)) throw new Error(`Assertion fail: variable is not an key object of objects or undefineds of type ${typeName}`);
		return test;
	}
	
	public static isTOrUndefinedEnumObject<E extends string, T>(
			test: unknown,
			checker: (t: unknown) => t is T,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, T|undefined> {
		return CommonsType.validateTOrUndefinedEnumObject<E, T>(
				test,
				(t: unknown): t is T => CommonsType.isT<T>(t, checker),
				keyChecker
		);
	}
	
	public static assertTOrUndefinedEnumObject<E extends string, T>(
			test: unknown,
			checker: (t: unknown) => t is T,
			typeName: string,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, T|undefined> {
		if (!CommonsType.isTEnumObject<E, T>(
				test,
				checker,
				keyChecker
		)) throw new Error(`Assertion fail: variable is not an key object of objects or undefineds of type ${typeName}`);
		return test;
	}
	
	public static isTArrayKeyObject<T>(test: unknown, checker: (t: unknown) => t is T): test is TKeyObject<T[]> {
		return CommonsType.validateTKeyObject<T[]>(
				test,
				(t: unknown): t is T[] => CommonsType.isTArray<T>(t, checker)
		);
	}
	
	public static assertTArrayKeyObject<T>(test: unknown, checker: (t: unknown) => t is T, typeName: string): TKeyObject<T[]> {
		if (!CommonsType.isTArrayKeyObject<T>(test, checker)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${typeName}`);
		return test;
	}
	
	public static isTArrayEnumObject<E extends string, T>(
			test: unknown,
			checker: (t: unknown) => t is T,
			keyChecker: (k: unknown) => k is E
	): test is TEnumObject<E, T[]> {
		return CommonsType.validateTEnumObject<E, T[]>(
				test,
				(t: unknown): t is T[] => CommonsType.isTArray<T>(t, checker),
				keyChecker
		);
	}
	
	public static assertTArrayEnumObject<E extends string, T>(
			test: unknown,
			checker: (t: unknown) => t is T,
			typeName: string,
			keyChecker: (k: unknown) => k is E
	): TEnumObject<E, T[]> {
		if (!CommonsType.isTArrayEnumObject<E, T>(
				test,
				checker,
				keyChecker
		)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${typeName}`);

		return test;
	}
	
	public static isEnum<E>(test: unknown, resolver: (_: string) => E|undefined): test is E {
		if (!CommonsType.isString(test)) return false;
		return resolver(test) !== undefined;
	}
	
	public static assertEnum<E>(test: unknown, resolver: (_: string) => E|undefined, enumName: string): E {
		if (!CommonsType.isEnum<E>(test, resolver)) throw new Error(`Assertion fail: variable is not an enum of type ${enumName}`);
		return test;
	}
	
	public static isEnumArray<E>(test: unknown, resolver: (_: string) => E|undefined): test is E[] {
		if (!CommonsType.isStringArray(test)) return false;

		for (const subtest of test) {
			if (!CommonsType.isEnum<E>(subtest, resolver)) return false;
		}
	
		return true;
	}
	
	public static assertEnumArray<E>(test: unknown, resolver: (_: string) => E|undefined, enumName: string): E[] {
		if (!CommonsType.isEnumArray<E>(test, resolver)) throw new Error(`Assertion fail: variable is not an array of enums of type ${enumName}`);
		return test;
	}
	
	public static isEnumKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined): test is TKeyObject<E> {
		return CommonsType.validateTKeyObject<E>(
				test,
				(e: unknown): e is E => CommonsType.isEnum<E>(e, resolver)
		);
	}
	
	public static assertEnumKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined, enumName: string): TKeyObject<E> {
		if (!CommonsType.isEnumKeyObject<E>(test, resolver)) throw new Error(`Assertion fail: variable is not an key object of enums of type ${enumName}`);
		return test;
	}
	
	// NB, K and E are the other way round for this one
	public static isEnumEnumObject<K extends string, E>(
			test: unknown,
			resolver: (_: string) => E|undefined,
			keyChecker: (k: unknown) => k is K
	): test is TEnumObject<K, E> {
		return CommonsType.validateTEnumObject<K, E>(
				test,
				(e: unknown): e is E => CommonsType.isEnum<E>(e, resolver),
				keyChecker
		);
	}
	
	// NB, K and E are the other way round for this one
	public static assertEnumEnumObject<K extends string, E>(
			test: unknown,
			resolver: (_: string) => E|undefined,
			enumName: string,
			keyChecker: (k: unknown) => k is K
	): TEnumObject<K, E> {
		if (!CommonsType.isEnumEnumObject<K, E>(
				test,
				resolver,
				keyChecker
		)) throw new Error(`Assertion fail: variable is not an key object of enums of type ${enumName}`);
		return test;
	}
	
	public static isEnumOrUndefinedKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined): test is TKeyObject<E|undefined> {
		return CommonsType.validateTOrUndefinedKeyObject<E>(
				test,
				(e: unknown): e is E => CommonsType.isEnum<E>(e, resolver)
		);
	}
	
	public static assertEnumOrUndefinedKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined, enumName: string): TKeyObject<E|undefined> {
		if (!CommonsType.isEnumKeyObject<E>(test, resolver)) throw new Error(`Assertion fail: variable is not an key object of enums of type ${enumName}`);
		return test;
	}
	
	// NB, K and E are the other way round for this one
	public static isEnumOrUndefinedEnumObject<K extends string, E>(
			test: unknown,
			resolver: (_: string) => E|undefined,
			keyChecker: (k: unknown) => k is K
	): test is TEnumObject<K, E|undefined> {
		return CommonsType.validateTOrUndefinedEnumObject<K, E>(
				test,
				(e: unknown): e is E => CommonsType.isEnum<E>(e, resolver),
				keyChecker
		);
	}
	
	// NB, K and E are the other way round for this one
	public static assertEnumOrUndefinedEnumObject<K extends string, E>(
			test: unknown,
			resolver: (_: string) => E|undefined,
			enumName: string,
			keyChecker: (k: unknown) => k is K
	): TEnumObject<K, E|undefined> {
		if (!CommonsType.isEnumEnumObject<K, E>(
				test,
				resolver,
				keyChecker
		)) throw new Error(`Assertion fail: variable is not an key object of enums of type ${enumName}`);
		return test;
	}
	
	public static isEnumArrayKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined): test is TKeyObject<E[]> {
		return CommonsType.validateTKeyObject<E[]>(
				test,
				(e: unknown): e is E[] => CommonsType.isEnumArray<E>(e, resolver)
		);
	}
	
	public static assertEnumArrayKeyObject<E>(test: unknown, resolver: (_: string) => E|undefined, enumName: string): TKeyObject<E[]> {
		if (!CommonsType.isEnumArrayKeyObject<E>(test, resolver)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${enumName}`);
		return test;
	}
	
	// NB, K and E are the other way round for this one
	public static isEnumArrayEnumObject<K extends string, E>(
			test: unknown,
			resolver: (_: string) => E|undefined,
			keyChecker: (k: unknown) => k is K
	): test is TEnumObject<K, E[]> {
		return CommonsType.validateTEnumObject<K, E[]>(
				test,
				(e: unknown): e is E[] => CommonsType.isEnumArray<E>(e, resolver),
				keyChecker
		);
	}
	
	// NB, K and E are the other way round for this one
	public static assertEnumArrayEnumObject<K extends string, E>(
			test: unknown,
			resolver: (_: string) => E|undefined,
			enumName: string,
			keyChecker: (k: unknown) => k is K
	): TEnumObject<K, E[]> {
		if (!CommonsType.isEnumArrayEnumObject<K, E>(
				test,
				resolver,
				keyChecker
		)) throw new Error(`Assertion fail: variable is not an key object of objects of type ${enumName}`);
		return test;
	}
	
	// Object property checkers and assertions
	
	public static hasProperty(test: unknown, property: string, type?: EVariableType): test is TPropertyObject {
		if (type !== undefined && 'string' !== typeof type) throw new Error('Type is invalid');
		
		if (!CommonsType.isPropertyObject(test)) return false;
		
		if (-1 === Object.keys(test).indexOf(property)) return false;

		if (type === undefined) return true;
		
		switch (type) {
			case EVariableType.STRING:
				return CommonsType.isString(test[property]);
			case EVariableType.NUMBER:
				return CommonsType.isNumber(test[property]);
			case EVariableType.BOOLEAN:
				return CommonsType.isBoolean(test[property]);
			case EVariableType.OBJECT:
				return CommonsType.isObject(test[property]);
			case EVariableType.DATE:
				return CommonsType.isDate(test[property]);
			default:
				throw new Error('Unknown variable type');
		}
	}
	
	// The test is TPropertyObject is right, as we are testing the object's having the property and typecasting as a TPropertyObject, not typecasting the type of that property
	
	public static hasPropertyNumber(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasProperty(test, property, EVariableType.NUMBER);
	}
	
	public static hasPropertyString(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasProperty(test, property, EVariableType.STRING);
	}
	
	public static hasPropertyBoolean(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasProperty(test, property, EVariableType.BOOLEAN);
	}
	
	public static hasPropertyObject(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasProperty(test, property, EVariableType.OBJECT);
	}
	
	public static hasPropertyDate(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasProperty(test, property, EVariableType.DATE);
	}
	
	public static hasPropertyArray(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property) && CommonsType.isArray(test[property]);
	}
	
	public static hasPropertyEnum<E>(test: unknown, property: string, checker: (_: unknown) => _ is E): test is TPropertyObject {
		return CommonsType.hasPropertyString(test, property) && checker(test[property]);
	}
	
	public static hasPropertyEnumArray<E>(test: unknown, property: string, checker: (_: unknown) => _ is E): test is TPropertyObject {
		if (!CommonsType.hasPropertyStringArray(test, property)) return false;

		const array: string[] = test[property] as string[];
		for (const item of array) if (!checker(item)) return false;
		
		return true;
	}
	
	public static hasPropertyT<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property) && CommonsType.isT<T>(test[property], checker);
	}
	
	public static hasPropertyTArray<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property) && CommonsType.isTArray<T>(test[property], checker);
	}
	
	public static hasPropertyTKeyObject<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property) && CommonsType.isTKeyObject<T>(test[property], checker);
	}
	
	public static hasPropertyTEnumObject<E extends string, T>(
			test: unknown,
			property: string,
			checker: (t: unknown) => t is T,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property)
				&& CommonsType.isTEnumObject<E, T>(
						test[property],
						checker,
						keyChecker
				);
	}
	
	public static hasPropertyNumberArray(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyArray(test, property)
				&& CommonsType.isNumberArray(test[property]);
	}
	
	public static hasPropertyNumberKeyObject(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property)
				&& CommonsType.isNumberKeyObject(test[property]);
	}
	
	public static hasPropertyNumberEnumObject<E extends string>(
			test: unknown,
			property: string,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property)
				&& CommonsType.isNumberEnumObject<E>(
						test[property],
						keyChecker
				);
	}
	
	public static hasPropertyStringArray(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyArray(test, property)
				&& CommonsType.isStringArray(test[property]);
	}
	
	public static hasPropertyStringKeyObject(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property)
				&& CommonsType.isStringKeyObject(test[property]);
	}
	
	public static hasPropertyStringEnumObject<E extends string>(
			test: unknown,
			property: string,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property)
				&& CommonsType.isStringEnumObject<E>(
						test[property],
						keyChecker
				);
	}
	
	public static hasPropertyBooleanArray(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyArray(test, property)
				&& CommonsType.isBooleanArray(test[property]);
	}
	
	public static hasPropertyBooleanKeyObject(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property)
				&& CommonsType.isBooleanKeyObject(test[property]);
	}
	
	public static hasPropertyBooleanEnumObject<E extends string>(
			test: unknown,
			property: string,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property)
				&& CommonsType.isBooleanEnumObject<E>(
						test[property],
						keyChecker
				);
	}
	
	public static hasPropertyDateArray(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyTArray<Date>(test, property, CommonsType.isDate);
	}
	
	public static hasPropertyDateKeyObject(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property)
				&& CommonsType.isDateKeyObject(test[property]);
	}
	
	public static hasPropertyDateEnumObject<E extends string>(
			test: unknown,
			property: string,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property)
				&& CommonsType.isDateEnumObject<E>(
						test[property],
						keyChecker
				);
	}
	
	public static hasPropertyObjectArray(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyTArray<{}>(test, property, CommonsType.isObject);
	}
	
	public static hasPropertyObjectKeyObject(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property)
				&& CommonsType.isObjectKeyObject(test[property]);
	}
	
	public static hasPropertyObjectEnumObject<E extends string>(
			test: unknown,
			property: string,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		return CommonsType.hasPropertyObject(test, property)
				&& CommonsType.isObjectEnumObject<E>(
						test[property],
						keyChecker
				);
	}
	
	public static hasPropertyNumberOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyNumber(test, property);
	}
	
	public static hasPropertyStringOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyString(test, property);
	}
	
	public static hasPropertyBooleanOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyBoolean(test, property);
	}
	
	public static hasPropertyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyObject(test, property);
	}
	
	public static hasPropertyDateOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyDate(test, property);
	}
	
	public static hasPropertyArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyArray(test, property);
	}
	
	public static hasPropertyEnumOrUndefined<E>(test: unknown, property: string, checker: (_: unknown) => _ is E): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyEnum<E>(test, property, checker);
	}
	
	public static hasPropertyEnumArrayOrUndefined<E>(test: unknown, property: string, checker: (_: unknown) => _ is E): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyEnumArray<E>(test, property, checker);
	}
	
	public static hasPropertyTOrUndefined<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyT<T>(test, property, checker);
	}
	
	public static hasPropertyTArrayOrUndefined<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyTArray<T>(test, property, checker);
	}
	
	public static hasPropertyTKeyObjectOrUndefined<T>(test: unknown, property: string, checker: (t: unknown) => t is T): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyTKeyObject<T>(test, property, checker);
	}
	
	public static hasPropertyTEnumObjectOrUndefined<E extends string, T>(
			test: unknown,
			property: string,
			checker: (t: unknown) => t is T,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyTEnumObject<E, T>(
				test,
				property,
				checker,
				keyChecker
		);
	}
	
	public static hasPropertyNumberArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyNumberArray(test, property);
	}
	
	public static hasPropertyNumberKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyNumberKeyObject(test, property);
	}
	
	public static hasPropertyNumberEnumObjectOrUndefined<E extends string>(
			test: unknown,
			property: string,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyNumberEnumObject<E>(
				test,
				property,
				keyChecker
		);
	}
	
	public static hasPropertyStringArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyStringArray(test, property);
	}
	
	public static hasPropertyStringKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyStringKeyObject(test, property);
	}
	
	public static hasPropertyStringEnumObjectOrUndefined<E extends string>(
			test: unknown,
			property: string,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyStringEnumObject<E>(
				test,
				property,
				keyChecker
		);
	}
	
	public static hasPropertyBooleanArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyBooleanArray(test, property);
	}
	
	public static hasPropertyBooleanKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyBooleanKeyObject(test, property);
	}
	
	public static hasPropertyBooleanEnumObjectOrUndefined<E extends string>(
			test: unknown,
			property: string,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyBooleanEnumObject<E>(
				test,
				property,
				keyChecker
		);
	}
	
	public static hasPropertyDateArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyTArrayOrUndefined<Date>(test, property, CommonsType.isDate);
	}
	
	public static hasPropertyDateKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyDateKeyObject(test, property);
	}
	
	public static hasPropertyDateEnumObjectOrUndefined<E extends string>(
			test: unknown,
			property: string,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyDateEnumObject<E>(
				test,
				property,
				keyChecker
		);
	}
	
	public static hasPropertyObjectArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
		return CommonsType.hasPropertyTArrayOrUndefined<{}>(test, property, CommonsType.isObject);
	}
	
	public static hasPropertyObjectKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyObjectKeyObject(test, property);
	}
	
	public static hasPropertyObjectEnumObjectOrUndefined<E extends string>(
			test: unknown,
			property: string,
			keyChecker: (k: unknown) => k is E
	): test is TPropertyObject {
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		return CommonsType.hasPropertyObjectEnumObject<E>(
				test,
				property,
				keyChecker
		);
	}

	// Loosely typed checkers, converters, guards and assertions
	
	public static valueOrDefault<T>(value: T|undefined, defaultValue: T): T {
		return value === undefined ? defaultValue : value;
	}
	
	public static trimStringOrUndefined(value: any): string|undefined {
		if (CommonsType.isBlank(value)) return undefined;
		
		if ('' === (value = value.toString().trim())) return undefined;
		
		return value;
	}

	public static isBlank(test: unknown): test is undefined|null|string {
		return test === undefined || test === null || test === '';
	}
	
	public static isLooselyEqual(a: TPrimative|Date|undefined, b: TPrimative|Date|undefined, caseInsensitive: boolean = false, iso: boolean = false): boolean {
		if (CommonsType.isBlank(a) && CommonsType.isBlank(b)) return true;
		if (CommonsType.isDate(a) && CommonsType.isDate(b) && a.getTime() === b.getTime()) return true;
		if (CommonsType.isDate(a) && ('string' === typeof b) && CommonsDate.dateToYmdHis(a, iso) === b) return true;
		if (CommonsType.isDate(b) && ('string' === typeof a) && CommonsDate.dateToYmdHis(b, iso) === a) return true;
		if (('string' === typeof a) && ('string' === typeof b)) {
			if (a.trim() === b.trim()) return true;
			if (caseInsensitive && a.toLowerCase().trim() === b.toLowerCase().trim()) return true;
		}
		
		if (('number' === typeof a) && ('string' === typeof b) && `${a}` === b) return true;
		if (('number' === typeof b) && ('string' === typeof a) && `${b}` === a) return true;

		if (('boolean' === typeof a) && ('string' === typeof b) && `${a ? 'true' : 'false'}` === b.toLowerCase().trim()) return true;
		if (('boolean' === typeof b) && ('string' === typeof a) && `${b ? 'true' : 'false'}` === a.toLowerCase().trim()) return true;
		if (('boolean' === typeof a) && ('string' === typeof b) && `${a ? '1' : '0'}` === b.toLowerCase().trim()) return true;
		if (('boolean' === typeof b) && ('string' === typeof a) && `${b ? '1' : '0'}` === a.toLowerCase().trim()) return true;
		if (('boolean' === typeof a) && ('string' === typeof b) && `${a ? 'yes' : 'no'}` === b.toLowerCase().trim()) return true;
		if (('boolean' === typeof b) && ('string' === typeof a) && `${b ? 'yes' : 'no'}` === a.toLowerCase().trim()) return true;
		if (('boolean' === typeof a) && ('string' === typeof b) && `${a ? 'on' : 'off'}` === b.toLowerCase().trim()) return true;
		if (('boolean' === typeof b) && ('string' === typeof a) && `${b ? 'on' : 'off'}` === a.toLowerCase().trim()) return true;
		if (('boolean' === typeof a) && ('number' === typeof b) && (a ? 1 : 0) === b) return true;
		if (('boolean' === typeof b) && ('number' === typeof a) && (b ? 1 : 0) === a) return true;

		return a === b;
	}
	
	public static checkboxBoolean(a: TPrimative): boolean {
		return CommonsType.isLooselyEqual(true, a);
	}
	
	public static encode(value: any|null, iso: boolean = false): TEncoded {
		if (value === null) return null;
		
		if (CommonsType.isPrimative(value)) return value as TPrimative;
		
		if (CommonsType.isDate(value)) return CommonsDate.dateToYmdHis(value as Date, iso);

		if (Array.isArray(value)) {
			const encoded: TEncodedArray = [];
			const array: any[] = value as any[];
			
			for (const item of array) encoded.push(CommonsType.encode(item, iso));
			
			return encoded;
		}
		
		if (CommonsType.isObject(value)) {
			const encoded: TEncodedObject = {};
			const object: {} = value as {};
			
			for (const property of Object.keys(object)) {
				if (object[property] === undefined) continue;
				encoded[property] = CommonsType.encode(object[property], iso);
			}
			
			return encoded;
		}
		
		// unknown
		return value;
	}
	
	public static encodePropertyObject(object: TPropertyObject, iso: boolean = false): TEncodedObject {
		return CommonsType.encode(object, iso) as TEncodedObject;
	}
	
	public static decode(value: TEncoded, iso: boolean = false): any|null {
		if (value === null) return null;

		if (CommonsType.isPrimative(value)) {
			if (CommonsType.isString(value)) {
				if (CommonsDate.isYmdHis(value as string)) return CommonsDate.YmdHisToDate(value as string, iso);
				if (CommonsDate.isYmd(value as string)) return CommonsDate.YmdToDate(value as string, iso);
				if (COMMONS_REGEX_PATTERN_DATE_ECMA.test(value as string)) return new Date(value as string);
			}
			return value as TPrimative;
		}
		
		if (Array.isArray(value)) {
			const decoded: any[] = [];
			const array: TEncodedArray = value as TEncodedArray;
			
			for (const item of array) decoded.push(CommonsType.decode(item, iso));
			
			return decoded;
		}
		
		if (CommonsType.isObject(value)) {
			const decoded: TPropertyObject = {};
			const object: TEncodedObject = value as TEncodedObject;
			
			for (const property of Object.keys(object)) {
				if (object[property] === undefined) continue;
				decoded[property] = CommonsType.decode(object[property], iso);
			}
			
			return decoded;
		}
		
		// unknown
		return value;
	}
	
	public static decodePropertyObject(object: TEncodedObject, iso: boolean = false): TPropertyObject {
		return CommonsType.decode(object, iso) as TPropertyObject;
	}

	// other assertions and checkers

	public static assertArrayLengths(a: unknown, b: unknown): void|never {
		const assertedA: unknown[] = CommonsType.assertArray(a);
		const assertedB: unknown[] = CommonsType.assertArray(b);
	
		if (assertedA.length !== assertedB.length) throw new Error('Assertion fail: two arrays are not the same length');
	}
}
