import { TArrayWithErrors } from '../types/tarray-with-errors';

import { CommonsType } from './commons-type';
import { CommonsNumber } from './commons-number';

export abstract class CommonsArray {
	public static remove<T>(
			array: T[],
			item: T,
			isEqual?: (a: T, b: T) => boolean
	): boolean {
		let index: number = -1;

		if (isEqual) {
			for (let i = 0; i < array.length; i++) {
				if (isEqual(item, array[i])) {
					index = i;
					break;
				}
			}
		} else {
			index = array.indexOf(item);
		}
		if (index === -1) return false;
		
		array.splice(index, 1);
		
		return true;
	}
	
	public static chunk<T>(src: T[], size: number): T[][] {
		const chunks: T[][] = [];
		for (let i = 0, len = src.length; i < len; i += size) {
			chunks.push(src.slice(i, i + size));
		}
		
		return chunks;
	}
	
	public static unique<T>(
			src: T[],
			checker?: (_: unknown) => _ is T,
			isEqual?: (a: T, b: T) => boolean
	): T[] {
		// we can't use isTArray, as that only accepts T objects, not E enums etc.
		if (checker) {
			if (!CommonsType.isArray(src)) throw new Error('Invalid array supplied to CommonsArray.unique');
			for (const item of src) {
				if (!checker(item)) throw new Error('Invalid array item supplied to CommonsArray.unique');
			}
		}
		
		if (!isEqual) {
			return [ ...new Set(src) ];
		}
		
		const uniques: T[] = [];
		for (const item of src) {
			const existing: T|undefined = uniques
					.find((i: T): boolean => isEqual(item, i));
			
			if (!existing) uniques.push(item);
		}
		
		return uniques;
	}
	
	public static intersect<T>(
			as: T[],
			bs: T[],
			checker?: (_: unknown) => _ is T,
			isEqual?: (a: T, b: T) => boolean
	): T[] {
		// we can't use isTArray, as that only accepts T objects, not E enums etc.
		if (checker) {
			if (!CommonsType.isArray(as)) throw new Error('Invalid a array supplied to CommonsArray.intersect');
			for (const item of as) {
				if (!checker(item)) throw new Error('Invalid a array item supplied to CommonsArray.intersect');
			}

			if (!CommonsType.isArray(bs)) throw new Error('Invalid b array supplied to CommonsArray.intersect');
			for (const item of bs) {
				if (!checker(item)) throw new Error('Invalid b array item supplied to CommonsArray.intersect');
			}
		}

		if (!isEqual) {
			const intersection: T[] = as
					.filter((item: T): boolean => bs.includes(item));
					
			return CommonsArray.unique<T>(intersection);	// no checker needed as performed already
		}

		{	// scope
			const intersection: T[] = [];
			for (const item of as) {
				const match: T|undefined = bs
						.find((i: T): boolean => isEqual(item, i));
				
				if (match) intersection.push(item);
			}
	
			return CommonsArray.unique<T>(
					intersection,
					undefined,
					isEqual
			);	// no checker needed as performed already
		}
	}
	
	public static union<T>(
			as: T[],
			bs: T[],
			checker?: (_: unknown) => _ is T,
			isEqual?: (a: T, b: T) => boolean
	): T[] {
		// we can't use isTArray, as that only accepts T objects, not E enums etc.
		if (checker) {
			if (!CommonsType.isArray(as)) throw new Error('Invalid a array supplied to CommonsArray.union');
			for (const item of as) {
				if (!checker(item)) throw new Error('Invalid a array item supplied to CommonsArray.union');
			}

			if (!CommonsType.isArray(bs)) throw new Error('Invalid b array supplied to CommonsArray.union');
			for (const item of bs) {
				if (!checker(item)) throw new Error('Invalid b array item supplied to CommonsArray.union');
			}
		}

		const union: T[] = [ ...as, ...bs ];

		return CommonsArray.unique<T>(union, undefined, isEqual);	// no checker needed as performed already
	}
	
	public static difference<T>(
			as: T[],
			bs: T[],
			checker?: (_: unknown) => _ is T,
			isEqual?: (a: T, b: T) => boolean
	): T[] {
		// we can't use isTArray, as that only accepts T objects, not E enums etc.
		if (checker) {
			if (!CommonsType.isArray(as)) throw new Error('Invalid a array supplied to CommonsArray.difference');
			for (const item of as) {
				if (!checker(item)) throw new Error('Invalid a array item supplied to CommonsArray.difference');
			}

			if (!CommonsType.isArray(bs)) throw new Error('Invalid b array supplied to CommonsArray.difference');
			for (const item of bs) {
				if (!checker(item)) throw new Error('Invalid b array item supplied to CommonsArray.difference');
			}
		}
		
		const union: T[] = CommonsArray.union<T>(as, bs, undefined, isEqual);
		const intersection: T[] = CommonsArray.intersect<T>(as, bs, undefined, isEqual);
		
		if (!isEqual) {
			const difference: T[] = union
					.filter((item: T): boolean => !intersection.includes(item));
			
			return difference;	// already unique from union
		}
		
		{	// scope
			const difference: T[] = [];
			for (const item of union) {
				const match: T|undefined = intersection
						.find((i: T): boolean => isEqual(item, i));
				
				if (!match) difference.push(item);
			}
			
			return difference;	// already unique from union
		}
	}
	
	public static removeAll<T>(
			existings: T[],
			toRemoves: T[],
			checker?: (_: unknown) => _ is T,
			isEqual?: (a: T, b: T) => boolean
	): boolean {
		// we can't use isTArray, as that only accepts T objects, not E enums etc.
		if (checker) {
			if (!CommonsType.isArray(existings)) throw new Error('Invalid existings array supplied to CommonsArray.removeAll');
			for (const item of existings) {
				if (!checker(item)) throw new Error('Invalid existings array item supplied to CommonsArray.removeAll');
			}

			if (!CommonsType.isArray(toRemoves)) throw new Error('Invalid toRemoves array supplied to CommonsArray.removeAll');
			for (const item of toRemoves) {
				if (!checker(item)) throw new Error('Invalid toRemoves array item supplied to CommonsArray.removeAll');
			}
		}
		
		let anyRemoved: boolean = false;
		for (const item of toRemoves) {
			if (CommonsArray.remove(
					existings,
					item,
					isEqual
			)) anyRemoved = true;
		}
		
		return anyRemoved;
	}
	
	public static unionWithout<T>(
			as: T[],
			bs: T[],
			withouts: T[],
			checker?: (_: unknown) => _ is T,
			isEqual?: (a: T, b: T) => boolean
	): T[] {
		const union: T[] = CommonsArray.union(
				as,
				bs,
				checker,
				isEqual
		);
		
		CommonsArray.removeAll(
				union,
				withouts,
				checker,
				isEqual
		);

		return union;
	}

	// Essentially a one-way difference, i.e. only items in updateds not in original
	public static added<T>(
			originals: T[],
			news: T[],
			checker?: (_: unknown) => _ is T,
			isEqual?: (a: T, b: T) => boolean
	): T[] {
		// we can't use isTArray, as that only accepts T objects, not E enums etc.
		if (checker) {
			if (!CommonsType.isArray(originals)) throw new Error('Invalid originals array supplied to CommonsArray.added');
			for (const item of originals) {
				if (!checker(item)) throw new Error('Invalid originals array item supplied to CommonsArray.added');
			}

			if (!CommonsType.isArray(news)) throw new Error('Invalid news array supplied to CommonsArray.added');
			for (const item of news) {
				if (!checker(item)) throw new Error('Invalid news array item supplied to CommonsArray.added');
			}
		}
		
		const addeds: T[] = [];
		for (const item of news) {
			if (!isEqual) {
				if (!originals.includes(item)) addeds.push(item);
			} else {
				const match: T|undefined = originals
						.find((i: T): boolean => isEqual(item, i));
				if (!match) addeds.push(item);
			}
		}
		
		return addeds;
	}

	public static removed<T>(
			originals: T[],
			news: T[],
			checker?: (_: unknown) => _ is T,
			isEqual?: (a: T, b: T) => boolean
	): T[] {
		// this turns out to be just an inversion of added
		
		return CommonsArray.added(news, originals, checker, isEqual);
	}
	
	/* Adapted from: https://www.geeksforgeeks.org/print-subsets-given-size-set/ */
	private static setCombinationsRecurse<T>(
			source: T[],
			setSize: number,
			index: number,
			data: T[],
			i: number,
			out: T[][]
	): void {
		// Current combination is ready so add it
		if (index === setSize) {
			out.push(data.slice(0, setSize));
			return;
		}

		// When no more elements are there to put in data[]
		if (i >= source.length) return;

		// current is included, put next at next location
		data[index] = source[i];
		CommonsArray.setCombinationsRecurse<T>(source, setSize, index + 1, data, i + 1, out);

		// current is excluded, replace it with next (Note that i+1 is passed, but index is not changed)
		CommonsArray.setCombinationsRecurse<T>(source, setSize, index, data, i + 1, out);
	}

	/* Adapted from: https://www.geeksforgeeks.org/print-subsets-given-size-set/ */
	public static setCombinations<T>(
			source: T[],
			setSize: number
	): T[][] {
		const out: T[][] = [];

		// A temporary array to store all combination one by one
		const data: T[] = Array(setSize).fill('');

		// Generate all combination using temporary array, output to out
		CommonsArray.setCombinationsRecurse<T>(source, setSize, 0, data, 0, out);

		return out;
	}
	
	public static randomize<T>(array: T[]): void {
		const length: number = array.length;
		
		for (let i = length; i-- > 0;) {
			const j = CommonsNumber.randRange(0, length);
			[array[i], array[j]] = [array[j], array[i]];
		}
	}
	
	private static getMinDistance(order: number[]): number {
		let min: number = order.length;	// just to get the largest possible distance, so min doesn't start with 0
		
		for (let i = 1; i < order.length; i++) {
			const delta: number = Math.abs(order[i] - order[i - 1]);
			min = Math.min(min, delta);
		}
		
		return min;
	}
	
	public static shuffle<T>(array: T[], minDistance: number = 2): boolean {
		const length: number = array.length;
		if (length < 3) {
			if (length === 2) CommonsArray.randomize(array);
			return false;
		}
		
		minDistance = Math.min(minDistance, Math.floor(length / 2));
		if (minDistance < 1) {
			CommonsArray.randomize(array);
			return false;
		}
		
		const order: number[] = [];
		for (let i = length, j = 0; i-- > 0;) order.push(j++);
		
		let best: number[]|undefined = order;
		let bestDistance: number = 0;
		
		let ttl: number = Math.max(1000, length);
		while (ttl-- > 0) {
			CommonsArray.randomize(order);
			const distance: number = CommonsArray.getMinDistance(order);
			
			if (distance > bestDistance) {
				best = order.slice();
				bestDistance = distance;

				if (bestDistance >= minDistance) break;
			}
		}
		
		const clone: T[] = array.slice();
		
		for (let i = 0; i < length; i++) {
			array[i] = clone[best[i]];
		}
		
		return bestDistance >= minDistance;
	}
	
	public static splitMapErrors<T>(array: (T|Error)[]): TArrayWithErrors<T> {
		const errors: Error[] = [];
		const items: T[] = [];

		for (const item of array) {
			try {
				if (item instanceof Error) {
					errors.push(item);
					continue;
				}
			} catch (e) {
				// ignore, probably a Javascript dislike of (e ! class object) instanceof Error
			}

			items.push(item as T);
		}
		
		return {
				errors: errors,
				array: items
		};
	}
	
	public static mapWithErrors<S, D>(
			src: S[],
			callback: (item: S) => D|Error
	): TArrayWithErrors<D> {
		const results: (D|Error)[] = [];
		
		for (const item of src) {
			try {
				const result: D|Error = callback(item);
				results.push(result);
			} catch (e) {
				if (CommonsType.isError(e)) {
					results.push(e);
				} else {
					results.push(new Error(e));
				}
			}
		}
		
		return CommonsArray.splitMapErrors(results);
	}
	
	public static empty(array: unknown[]): void {
		if (array.length === 0) return;
		array.splice(0, array.length);
	}
	
	public static removeUndefineds<T>(array: (T|undefined)[]): T[] {
		return array
				.filter((item: T|undefined): boolean => item !== undefined)
				.map((item: T|undefined): T => item!);
	}
	
	public static async removeUndefinedsPromise<T>(array: Promise<(T|undefined)>[]): Promise<T[]> {
		return CommonsArray.removeUndefineds(await Promise.all(array));
	}
}
