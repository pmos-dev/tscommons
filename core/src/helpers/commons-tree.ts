import { CommonsType } from './commons-type';

export type TSegmentTree = {
		tally: number;
		children?: { [segment: string]: TSegmentTree };
};

export function isTSegmentTree(test: any): test is TSegmentTree {
	if (!CommonsType.isObject(test)) return false;

	const attempt: TSegmentTree = test as TSegmentTree;
	
	if (!CommonsType.hasPropertyNumber(attempt, 'tally')) return false;
	if (!CommonsType.hasPropertyObjectOrUndefined(attempt, 'children')) return false;
	
	if (CommonsType.hasProperty(attempt, 'children')) {
		for (const segment of Object.keys(attempt.children!)) {
			if (!isTSegmentTree(attempt.children![segment])) return false;
		}
	}
	
	return true;
}

export type TSegmentStack = {
		name: string;
		tally: number;
};

export abstract class CommonsTree {
	public static stringSegmentArrayToTree(segmentsArray: string[][]): TSegmentTree {
		const tree: TSegmentTree = { tally: 0, children: {} };
		
		for (const segments of segmentsArray) {
			let closure: TSegmentTree = tree;
			
			while (true) {
				const segment: string|undefined = segments.shift();
				if (segment === undefined) break;
				
				// No idea why this explicit typecast is needed, but otherwise tsc thinks closure is never??
				if (!CommonsType.hasProperty(closure, 'children')) (closure as TSegmentTree).children = {};
				
				if (!CommonsType.hasProperty(closure.children, segment)) {
					closure.children![segment] = { tally: 1 };
				} else closure.children![segment].tally++;
				
				closure = closure.children![segment];
			}
		}
	
		return tree.children![''];
	}
	
	private static getDepth(tree: TSegmentTree): number {
		if (undefined === tree.children) return 1;
		
		let depths: number[] = [];
		for (const segment of Object.keys(tree.children || {})) {
			depths.push(CommonsTree.getDepth(tree.children![segment]));
		}
		
		if (depths.length === 0) return 1;
		
		depths = depths.sort().reverse();
		return 1 + depths[0];
	}
	
	private static treeToStacksRecurse(tree: TSegmentTree, stacks: TSegmentStack[][], depth: number, ttl: number = 100): void {
		if (ttl === 0) throw new Error('Exceeded TTL');
		
		let remaining: number = tree.tally;
		
		for (const segment of Object.keys(tree.children || {})) {
			const sub: TSegmentTree = tree.children![segment];
			stacks[depth].push({
					name: segment,
					tally: sub.tally
			});
			
			remaining -= sub.tally;
			
			CommonsTree.treeToStacksRecurse(sub, stacks, depth + 1, ttl - 1);
		}
		
		if (remaining > 0) {
			for (let i = depth; i < stacks.length; i++) {
				stacks[i].push({
						name: '',
						tally: remaining
				});
			}
		}
	}
	
	public static treeToStacks(tree: TSegmentTree): TSegmentStack[][] {
		const depth: number = CommonsTree.getDepth(tree);
		
		const stacks: TSegmentStack[][] = [];
		for (let i = depth - 1; i-- > 0;) stacks.push([]);
		
		CommonsTree.treeToStacksRecurse(tree, stacks, 0);
		
		return stacks;
	}
}
