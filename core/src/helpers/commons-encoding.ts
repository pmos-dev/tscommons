export abstract class CommonsEncoding {
	public static unicodeToAscii(unicode: string): string {
		return unicode.replace(/[\u007F-\uFFFF]/g, (c: string): string => '\\u' + `0000${c.charCodeAt(0).toString(16)}`.substr(-4));
	}
	
	public static asciiToUnicode(ascii: string): string {
		return ascii.replace(/\\u[0-9a-f]{4}/g, (u: string): string => String.fromCharCode(parseInt(u.substr(-4), 16)));
	}
	
	public static asciiToHex(ascii: string): string {
		return ascii.split('')
				.map((c: string): string => `0${c.charCodeAt(0).toString(16)}`.slice(-2))
				.join('');
	}
	
	public static hexToAscii(hex: string): string {
		if (hex.length < 2) return '';
		return hex.match(/.{2}/g)!
				.map((h: string): string => String.fromCharCode(parseInt(h, 16)))
				.join('');
	}
	
	public static unicodeToHex(unicode: string): string {
		return CommonsEncoding.asciiToHex(CommonsEncoding.unicodeToAscii(unicode));
	}
	
	public static hexToUnicode(hex: string): string {
		return CommonsEncoding.asciiToUnicode(CommonsEncoding.hexToAscii(hex));
	}
}
