import { TKeyObject } from '../types/tkey-object';
import { TPropertyObject } from '../types/tproperty-object';

import { CommonsType } from './commons-type';

export abstract class CommonsBase {
	public static decToBaseNChunks(dec: number, base: number): number[] {
		if (dec === 0) return [ 0 ];
		
		const size = Math.floor(Math.log(dec) / Math.log(base)) + 1;

		const chunks: number[] = [];
		for (let i = size, j = 0; i-- > 0; j++) {
			const dividor = Math.pow(base, i);
			const div = Math.floor(dec / dividor);
			chunks.push(div);
				
			dec -= div * dividor;
		}

		return chunks;
	}
	
	public static digitToBaseN(digit: number, base: number): string {
		if (base < 1 || base > 62) throw new Error('Base is less than 1 or greater than 62. This cannot be represented using 0-9A-Za-z notation');
		if (digit < 0 || digit > (base - 1)) throw new Error(`Digit out of range of base ${digit} vs base ${base}`);

		if (digit < 10) return digit.toString();

		digit -= 10;
		if (digit < 26) return String.fromCharCode(65 + digit);
		
		digit -= 26;
		return String.fromCharCode(97 + digit);
	}
	
	public static baseNToDigit(digit: string, base: number): number {
		if (base < 1 || base > 62) throw new Error('Base is less than 1 or greater than 62. This cannot be represented using 0-9A-Za-z notation');

		const b = digit.charCodeAt(0);
		if (b < 48) throw new Error('Character is outside scope of given base');
		if (b < 58) {
			const d = b - 48;
			if (d >= base) throw new Error('Character is outside scope of given base');
			return d;
		}
		if (b < 91) {
			const d = (b - 65) + 10;
			if (d >= base) throw new Error('Character is outside scope of given base');
			return d;
		}
		if (b < 123) {
			const d = (b - 97) + 10 + 26;
			if (d >= base) throw new Error('Character is outside scope of given base');
			return d;
		}

		throw new Error('Character is outside scope of given base');
	}
	
	public static decToBaseN(dec: number, base: number): string {
		const chunks: number[] = CommonsBase.decToBaseNChunks(dec, base);
	
		let result = '';
		for (const chunk of chunks) result = `${result}${CommonsBase.digitToBaseN(chunk, base)}`;
	
		return result;
	}
	
	public static baseNToDec(baseN: string, base: number): number {
		let dec = 0;
		while (baseN !== '') {
			const digit = CommonsBase.baseNToDigit(baseN.substring(0, 1), base);
			baseN = baseN.substring(1);
				
			dec *= base;
			dec += digit;
		}
	
		return dec;
	}
	
	public static generateRandomBaseNId(length: number, base: number): string {
		let sb = '';
		for (let i = length; i-- > 0;) {
			const digit = Math.floor(Math.random() * base);
			sb = `${sb}${CommonsBase.digitToBaseN(digit, base)}`;
		}

		return sb;
	}

	public static isBaseNIdArray(
			test: unknown,
			checker: (t: string) => boolean
	): test is string[] {
		if (!CommonsType.isStringArray(test)) return false;

		for (const subtest of test) {
			if (!checker(subtest)) return false;
		}
	
		return true;
	}

	public static isBaseNIdKeyObject(
			test: unknown,
			checker: (t: string) => boolean
	): test is TKeyObject<string> {
		if (!CommonsType.isStringKeyObject(test)) return false;
		
		for (const key of Object.keys(test)) {
			if (!checker(test[key])) return false;
		}
		
		return true;
	}

	public static isBaseNIdOrUndefinedKeyObject(
			test: unknown,
			checker: (t: string) => boolean
	): test is TKeyObject<string|undefined> {
		if (!CommonsType.isStringOrUndefinedKeyObject(test)) return false;

		for (const key of Object.keys(test)) {
			if (!CommonsType.hasProperty(test, key) || test[key] === undefined) continue;

			if (!checker(test[key]!)) return false;
		}
		
		return true;
	}

	public static isBaseNIdArrayKeyObject(
			test: unknown,
			checker: (t: string) => boolean
	): test is TKeyObject<string[]> {
		if (!CommonsType.isStringArrayKeyObject(test)) return false;

		for (const key of Object.keys(test)) {
			if (!CommonsBase.isBaseNIdArray(test[key], checker)) return false;
		}

		return true;
	}

	public static hasPropertyBaseNId(
			test: unknown,
			property: string,
			checker: (t: string) => boolean
	): test is TPropertyObject {
		if (!CommonsType.hasPropertyString(test, property)) return false;
		
		return checker(test[property]);
	}
	
	public static hasPropertyBaseNIdArray(
			test: unknown,
			property: string,
			checker: (t: string) => boolean
	): test is TPropertyObject {
		if (!CommonsType.hasPropertyStringArray(test, property)) return false;
		
		return CommonsBase.isBaseNIdArray(test[property], checker);
	}
	
	public static hasPropertyBaseNIdKeyObject(
			test: unknown,
			property: string,
			checker: (t: string) => boolean
	): test is TPropertyObject {
		if (!CommonsType.hasPropertyStringKeyObject(test, property)) return false;

		return CommonsBase.isBaseNIdArrayKeyObject(test[property], checker);
	}
	
	public static hasPropertyBaseNIdOrUndefined(
			test: unknown,
			property: string,
			checker: (t: string) => boolean
	): test is TPropertyObject {
		if (!CommonsType.hasPropertyStringOrUndefined(test, property)) return false;
		
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		
		return CommonsBase.hasPropertyBaseNId(test, property, checker);
	}
	
	public static hasPropertyBaseNIdArrayOrUndefined(
			test: unknown,
			property: string,
			checker: (t: string) => boolean
	): test is TPropertyObject {
		if (!CommonsType.hasPropertyStringArrayOrUndefined(test, property)) return false;
		
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		
		return CommonsBase.hasPropertyBaseNIdArray(test, property, checker);
	}
	
	public static hasPropertyBaseNIdKeyObjectOrUndefined(
			test: unknown,
			property: string,
			checker: (t: string) => boolean
	): test is TPropertyObject {
		if (!CommonsType.hasPropertyStringKeyObjectOrUndefined(test, property)) return false;
		
		if (!CommonsType.hasProperty(test, property) || !CommonsType.isDefined(test[property])) return true;
		
		return CommonsBase.hasPropertyBaseNIdKeyObject(test, property, checker);
	}
}

export class CommonsBase36 extends CommonsBase {
	private static BASE: number = 36;
	private static REGEX_PATTERN_BASE36: string = '^[0-9A-Z]+$';
	private static REGEX_PATTERN_BASE36_ID: string = '^[0-9A-Z]{10}$';

	public static decToChunks(dec: number): number[] {
		return super.decToBaseNChunks(dec, CommonsBase36.BASE);
	}
	
	public static digitToBase(digit: number): string {
		return super.digitToBaseN(digit, CommonsBase36.BASE);
	}
	
	public static baseToDigit(digit: string): number {
		return super.baseNToDigit(digit.toUpperCase(), CommonsBase36.BASE);
	}
	
	public static decToBase(dec: number): string {
		return super.decToBaseN(dec, CommonsBase36.BASE);
	}
	
	public static baseToDec(base: string): number {
		return super.baseNToDec(base.toUpperCase(), CommonsBase36.BASE);
	}
	
	public static generateRandomId(): string {
		return super.generateRandomBaseNId(10, CommonsBase36.BASE);
	}
	
	public static is(test: string): boolean {
		const regex: RegExp = new RegExp(CommonsBase36.REGEX_PATTERN_BASE36);	// ensures a new test each time
		return regex.test(test);
	}
	
	public static isId(test: string): boolean {
		const regex: RegExp = new RegExp(CommonsBase36.REGEX_PATTERN_BASE36_ID);	// ensures a new test each time
		return regex.test(test);
	}

	public static isIdArray(test: unknown): test is string[] {
		return CommonsBase.isBaseNIdArray(test, CommonsBase36.isId);
	}

	public static isIdKeyObject(test: unknown): test is TKeyObject<string> {
		return CommonsBase.isBaseNIdKeyObject(test, CommonsBase36.isId);
	}

	public static isIdOrUndefinedKeyObject(test: unknown): test is TKeyObject<string|undefined> {
		return CommonsBase.isBaseNIdOrUndefinedKeyObject(test, CommonsBase36.isId);
	}

	public static isIdArrayKeyObject(test: unknown): test is TKeyObject<string[]> {
		return CommonsBase.isBaseNIdArrayKeyObject(test, CommonsBase36.isId);
	}

	public static hasPropertyId(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNId(test, property, CommonsBase36.isId);
	}
	
	public static hasPropertyIdArray(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdArray(test, property, CommonsBase36.isId);
	}
	
	public static hasPropertyIdKeyObject(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdKeyObject(test, property, CommonsBase36.isId);
	}
	
	public static hasPropertyIdOrUndefined(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdOrUndefined(test, property, CommonsBase36.isId);
	}
	
	public static hasPropertyIdArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdArrayOrUndefined(test, property, CommonsBase36.isId);
	}
	
	public static hasPropertyIdKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdKeyObjectOrUndefined(test, property, CommonsBase36.isId);
	}
}

export class CommonsBase41 extends CommonsBase {
	private static BASE: number = 41;
	private static REGEX_PATTERN_BASE41: string = '^[0-9A-Za-e]+$';
	private static REGEX_PATTERN_BASE41_ID: string = '^[0-9A-Za-e]{6}$';

	public static decToChunks(dec: number): number[] {
		return super.decToBaseNChunks(dec, CommonsBase41.BASE);
	}
	
	public static digitToBase(digit: number): string {
		return super.digitToBaseN(digit, CommonsBase41.BASE);
	}
	
	public static baseToDigit(digit: string): number {
		return super.baseNToDigit(digit.toUpperCase(), CommonsBase41.BASE);
	}
	
	public static decToBase(dec: number): string {
		return super.decToBaseN(dec, CommonsBase41.BASE);
	}
	
	public static baseToDec(base: string): number {
		return super.baseNToDec(base.toUpperCase(), CommonsBase41.BASE);
	}
	
	public static generateRandomId(): string {
		return super.generateRandomBaseNId(6, CommonsBase41.BASE);
	}
	
	public static is(test: string): boolean {
		const regex: RegExp = new RegExp(CommonsBase41.REGEX_PATTERN_BASE41);	// ensures a new test each time
		return regex.test(test);
	}
	
	public static isId(test: string): boolean {
		const regex: RegExp = new RegExp(CommonsBase41.REGEX_PATTERN_BASE41_ID);	// ensures a new test each time
		return regex.test(test);
	}

	public static isIdArray(test: unknown): test is string[] {
		return CommonsBase.isBaseNIdArray(test, CommonsBase41.isId);
	}

	public static isIdKeyObject(test: unknown): test is TKeyObject<string> {
		return CommonsBase.isBaseNIdKeyObject(test, CommonsBase41.isId);
	}

	public static isIdOrUndefinedKeyObject(test: unknown): test is TKeyObject<string|undefined> {
		return CommonsBase.isBaseNIdOrUndefinedKeyObject(test, CommonsBase41.isId);
	}

	public static isIdArrayKeyObject(test: unknown): test is TKeyObject<string[]> {
		return CommonsBase.isBaseNIdArrayKeyObject(test, CommonsBase41.isId);
	}

	public static hasPropertyId(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNId(test, property, CommonsBase41.isId);
	}
	
	public static hasPropertyIdArray(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdArray(test, property, CommonsBase41.isId);
	}
	
	public static hasPropertyIdKeyObject(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdKeyObject(test, property, CommonsBase41.isId);
	}
	
	public static hasPropertyIdOrUndefined(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdOrUndefined(test, property, CommonsBase41.isId);
	}
	
	public static hasPropertyIdArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdArrayOrUndefined(test, property, CommonsBase41.isId);
	}
	
	public static hasPropertyIdKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdKeyObjectOrUndefined(test, property, CommonsBase41.isId);
	}
}

export class CommonsBase62 extends CommonsBase {
	private static BASE: number = 62;
	private static REGEX_PATTERN_BASE62: string = '^[0-9A-Za-z]+$';
	private static REGEX_PATTERN_BASE62_ID: string = '^[0-9A-Za-z]{8}$';

	public static decToChunks(dec: number): number[] {
		return super.decToBaseNChunks(dec, CommonsBase62.BASE);
	}
	
	public static digitToBase(digit: number): string {
		return super.digitToBaseN(digit, CommonsBase62.BASE);
	}
	
	public static baseToDigit(digit: string): number {
		return super.baseNToDigit(digit.toUpperCase(), CommonsBase62.BASE);
	}
	
	public static decToBase(dec: number): string {
		return super.decToBaseN(dec, CommonsBase62.BASE);
	}
	
	public static baseToDec(base: string): number {
		return super.baseNToDec(base.toUpperCase(), CommonsBase62.BASE);
	}
	
	public static generateRandomId(): string {
		return super.generateRandomBaseNId(8, CommonsBase62.BASE);
	}
	
	public static is(test: string): boolean {
		const regex: RegExp = new RegExp(CommonsBase62.REGEX_PATTERN_BASE62);	// ensures a new test each time
		return regex.test(test);
	}
	
	public static isId(test: string): boolean {
		const regex: RegExp = new RegExp(CommonsBase62.REGEX_PATTERN_BASE62_ID);	// ensures a new test each time
		return regex.test(test);
	}

	public static isIdArray(test: unknown): test is string[] {
		return CommonsBase.isBaseNIdArray(test, CommonsBase62.isId);
	}

	public static isIdKeyObject(test: unknown): test is TKeyObject<string> {
		return CommonsBase.isBaseNIdKeyObject(test, CommonsBase62.isId);
	}

	public static isIdOrUndefinedKeyObject(test: unknown): test is TKeyObject<string|undefined> {
		return CommonsBase.isBaseNIdOrUndefinedKeyObject(test, CommonsBase62.isId);
	}

	public static isIdArrayKeyObject(test: unknown): test is TKeyObject<string[]> {
		return CommonsBase.isBaseNIdArrayKeyObject(test, CommonsBase62.isId);
	}

	public static hasPropertyId(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNId(test, property, CommonsBase62.isId);
	}
	
	public static hasPropertyIdArray(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdArray(test, property, CommonsBase62.isId);
	}
	
	public static hasPropertyIdKeyObject(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdKeyObject(test, property, CommonsBase62.isId);
	}
	
	public static hasPropertyIdOrUndefined(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdOrUndefined(test, property, CommonsBase62.isId);
	}
	
	public static hasPropertyIdArrayOrUndefined(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdArrayOrUndefined(test, property, CommonsBase62.isId);
	}
	
	public static hasPropertyIdKeyObjectOrUndefined(test: unknown, property: string): test is TPropertyObject {
		return CommonsBase.hasPropertyBaseNIdKeyObjectOrUndefined(test, property, CommonsBase62.isId);
	}
}
