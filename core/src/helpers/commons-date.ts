import { TDateRange } from '../types/tdate-range';

import {
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS,
		COMMONS_REGEX_PATTERN_DATE_YMD,
		COMMONS_REGEX_PATTERN_TIME_HIS,
		COMMONS_REGEX_PATTERN_DATETIME_DMYHI,
		COMMONS_REGEX_PATTERN_DATE_DMY,
		COMMONS_REGEX_PATTERN_TIME_HI,
		COMMONS_REGEX_PATTERN_DATETIME_YMDHIS_COMPRESSED
} from '../consts/commons-regex';

import { CommonsNumber } from './commons-number';

type TComponents = {
		y: string;
		m: string;
		d: string;
		h: string;
		i: string;
		s: string;
};

export abstract class CommonsDate {
	private static parseIsoString(date: Date): TComponents {
		const match: RegExpMatchArray|null = date.toISOString().match(/^([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})(?:\.[0-9]+)?Z$/);
		if (!match) throw new Error('Unable to parse ISO string');

		return {
				y: match[1],
				m: match[2],
				d: match[3],
				h: match[4],
				i: match[5],
				s: match[6]
		};
	}
	
	private static fromDate(date: Date): TComponents {
		return {
				y: date.getFullYear().toString().padStart(4, '0'),
				m: (date.getMonth() + 1).toString().padStart(2, '0'),
				d: date.getDate().toString().padStart(2, '0'),
				h: date.getHours().toString().padStart(2, '0'),
				i: date.getMinutes().toString().padStart(2, '0'),
				s: date.getSeconds().toString().padStart(2, '0')
		};
	}
	
	public static dateToYmd(date: Date, iso: boolean = false): string {
		const components: TComponents = iso ? CommonsDate.parseIsoString(date) : CommonsDate.fromDate(date);
		
		return `${components.y}-${components.m}-${components.d}`;
	}

	public static dateTodmY(date: Date, iso: boolean = false): string {
		const components: TComponents = iso ? CommonsDate.parseIsoString(date) : CommonsDate.fromDate(date);
		
		return `${components.d}/${components.m}/${components.y}`;
	}
	
	public static dateToHi(date: Date, iso: boolean = false): string {
		const components: TComponents = iso ? CommonsDate.parseIsoString(date) : CommonsDate.fromDate(date);
		
		return `${components.h}:${components.i}`;
	}
	
	public static dateToHis(date: Date, iso: boolean = false): string {
		const components: TComponents = iso ? CommonsDate.parseIsoString(date) : CommonsDate.fromDate(date);
		
		return `${CommonsDate.dateToHi(date, iso)}:${components.s}`;
	}
	
	public static dateToYmdHis(date: Date, iso: boolean = false): string {
		return `${CommonsDate.dateToYmd(date, iso)} ${CommonsDate.dateToHis(date, iso)}`;
	}

	public static dateTodmYHi(date: Date, iso: boolean = false): string {
		return `${CommonsDate.dateTodmY(date, iso)} ${CommonsDate.dateToHi(date, iso)}`;
	}
	
	public static isYmdHis(test: string): boolean {
		const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_DATETIME_YMDHIS);
		return regex.test(test);
	}
	
	public static YmdHisToDate(date: string, iso: boolean = false): Date {
		const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_DATETIME_YMDHIS);
		if (match === null) throw new Error('Unable to parse date');
		
		if (iso) {
			return new Date(Date.parse(`${match[1]}-${match[2]}-${match[3]}T${match[4]}:${match[5]}:${match[6]}.000Z`));
		}
		
		const timestamp: Date = new Date();
		
		// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
		// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
		timestamp.setHours(12);	// midday is never affected by daylight saving
		timestamp.setDate(2);	// second is never affected by leap years and the like
		
		timestamp.setFullYear(parseInt(match[1], 10));
		timestamp.setMonth(parseInt(match[2], 10) - 1);
		timestamp.setDate(parseInt(match[3], 10));
		timestamp.setHours(parseInt(match[4], 10));
		timestamp.setMinutes(parseInt(match[5], 10));
		timestamp.setSeconds(parseInt(match[6], 10));
		CommonsDate.truncateToSecond(timestamp);

		return timestamp;
	}
	
	public static isdmYHi(test: string): boolean {
		const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_DATETIME_DMYHI);
		return regex.test(test);
	}
	
	public static dmYHiToDate(date: string, iso: boolean = false): Date {
		const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_DATETIME_DMYHI);
		if (match === null) throw new Error('Unable to parse date');
		
		if (iso) {
			return new Date(Date.parse(`${match[3]}-${match[2]}-${match[1]}T${match[4]}:${match[5]}:00.000Z`));
		}
		
		const timestamp: Date = new Date();
		
		// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
		// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
		timestamp.setHours(12);	// midday is never affected by daylight saving
		timestamp.setDate(2);	// second is never affected by leap years and the like
		
		timestamp.setFullYear(parseInt(match[3], 10));
		timestamp.setMonth(parseInt(match[2], 10) - 1);
		timestamp.setDate(parseInt(match[1], 10));
		timestamp.setHours(parseInt(match[4], 10));
		timestamp.setMinutes(parseInt(match[5], 10));
		CommonsDate.truncateToMinute(timestamp);

		return timestamp;
	}
	
	public static isYmd(test: string): boolean {
		const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_DATE_YMD);
		return regex.test(test);
	}
	
	public static isdmY(test: string): boolean {
		const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_DATE_DMY);
		return regex.test(test);
	}
	
	public static YmdToDate(date: string, iso: boolean = false): Date {
		const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_DATE_YMD);
		if (match === null) throw new Error('Unable to parse date');
		
		if (iso) {
			return new Date(Date.parse(`${match[1]}-${match[2]}-${match[3]}T00:00:00.000Z`));
		}
		
		const timestamp: Date = new Date();
		
		// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
		// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
		timestamp.setHours(12);	// midday is never affected by daylight saving
		timestamp.setDate(2);	// second is never affected by leap years and the like
		
		timestamp.setFullYear(parseInt(match[1], 10));
		timestamp.setMonth(parseInt(match[2], 10) - 1);
		timestamp.setDate(parseInt(match[3], 10));
		CommonsDate.truncateToDate(timestamp);

		return timestamp;
	}
	
	public static dmYToDate(date: string, iso: boolean = false): Date {
		const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_DATE_DMY);
		if (match === null) throw new Error('Unable to parse date');
		
		if (iso) {
			return new Date(Date.parse(`${match[3]}-${match[2]}-${match[1]}T00:00:00.000Z`));
		}
		
		const timestamp: Date = new Date();
		
		// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
		// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
		timestamp.setHours(12);	// midday is never affected by daylight saving
		timestamp.setDate(2);	// second is never affected by leap years and the like
		
		timestamp.setFullYear(parseInt(match[3], 10));
		timestamp.setMonth(parseInt(match[2], 10) - 1);
		timestamp.setDate(parseInt(match[1], 10));
		CommonsDate.truncateToDate(timestamp);

		return timestamp;
	}
	
	public static isHis(test: string): boolean {
		const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_TIME_HIS);
		return regex.test(test);
	}
	
	public static HisToDate(date: string, iso: boolean = false): Date {
		const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_TIME_HIS);
		if (match === null) throw new Error('Unable to parse date');
		
		if (iso) {
			return new Date(Date.parse(`1970-01-01T${match[1]}:${match[2]}:${match[3]}.000Z`));
		}
		
		const timestamp: Date = new Date();

		// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
		// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
		timestamp.setHours(12);	// midday is never affected by daylight saving
		timestamp.setDate(2);	// second is never affected by leap years and the like
		
		timestamp.setFullYear(1970);
		timestamp.setMonth(0);
		timestamp.setDate(1);
		timestamp.setHours(parseInt(match[1], 10));
		timestamp.setMinutes(parseInt(match[2], 10));
		timestamp.setSeconds(parseInt(match[3], 10));
		CommonsDate.truncateToSecond(timestamp);

		return timestamp;
	}

	public static isHi(test: string): boolean {
		const regex: RegExp = new RegExp(COMMONS_REGEX_PATTERN_TIME_HI);
		return regex.test(test);
	}
	
	public static HiToDate(date: string, iso: boolean = false): Date {
		const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_TIME_HI);
		if (match === null) throw new Error('Unable to parse date');
		
		if (iso) {
			return new Date(Date.parse(`1970-01-01T${match[1]}:${match[2]}:00.000Z`));
		}
		
		const timestamp: Date = new Date();

		// due to how JavaScript automatically adjusts times, dates and months depending on things like the number of days and leap years, we have to compensate by initially setting the date to a generic first of month one
		// otherwise, if the current date is 2020-07-31, and you want to set to 02 (feb), calling setMonth(1) actually ends up as rolling into 2 (mar)
		timestamp.setHours(12);	// midday is never affected by daylight saving
		timestamp.setDate(2);	// second is never affected by leap years and the like
		
		timestamp.setFullYear(1970);
		timestamp.setMonth(0);
		timestamp.setDate(1);
		timestamp.setHours(parseInt(match[1], 10));
		timestamp.setMinutes(parseInt(match[2], 10));
		CommonsDate.truncateToMinute(timestamp);

		return timestamp;
	}
	
	public static dateWithoutTime(date: Date, iso: boolean = false): Date {
		return CommonsDate.YmdToDate(CommonsDate.dateToYmd(date, iso), iso);
	}
	
	public static dateWithTime(date: Date, time: Date, iso: boolean = false): Date {
		const ymd: string = CommonsDate.dateToYmd(date, iso);
		const his: string = CommonsDate.dateToHis(time, iso);
		
		return CommonsDate.YmdHisToDate(`${ymd} ${his}`);
	}
	
	public static todayTime(time: Date, iso: boolean = false): Date {
		return CommonsDate.dateWithTime(new Date(), time, iso);
	}
	
	public static isSameDate(a: Date, b: Date, iso: boolean = false): boolean {
		return CommonsDate.dateWithoutTime(a, iso).getTime() === CommonsDate.dateWithoutTime(b, iso).getTime();
	}
	
	public static isToday(test: Date, iso: boolean = false): boolean {
		return CommonsDate.isSameDate(test, new Date(), iso);
	}
	
	public static isYesterday(test: Date, iso: boolean = false): boolean {
		const now: Date = new Date();
		now.setDate(now.getDate() - 1);
		
		return CommonsDate.isSameDate(test, now, iso);
	}
	
	public static isTomorrow(test: Date, iso: boolean = false): boolean {
		const now: Date = new Date();
		now.setDate(now.getDate() + 1);
		
		return CommonsDate.isSameDate(test, now, iso);
	}
	
	public static isLast24Hours(test: Date): boolean {
		const now: Date = new Date();
		const delta: number = now.getTime() - test.getTime();
		
		return delta < (24 * 60 * 60 * 1000);
	}
	
	public static isLeapYear(year: number): boolean {
		return new Date(year, 1, 29).getDate() === 29;
	}
	
	public static listTextMonths(): string[] {
		return 'January,February,March,April,May,June,July,August,September,October,November,December'.split(',');
	}
	
	public static getTextMonth(month: number): string {
		if (month < 0 || month > 11) return '';
		
		return CommonsDate.listTextMonths()[month];
	}
	
	public static getNumericMonth(month: string): number {
		return CommonsDate.listTextMonths()
				.map((m: string): string => m.toUpperCase())
				.map((m: string): string => month.length === 3 ? m.substring(0, 3) : m)
				.indexOf(month.toUpperCase());
	}
	
	public static listTextDays(): string[] {
		return 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday'.split(',');
	}
	
	public static getTextDay(day: number): string {
		if (day < 0 || day > 6) return '';
		
		return CommonsDate.listTextDays()[day];
	}
	
	public static getNumericDay(day: string): number {
		return CommonsDate.listTextDays()
				.map((m: string): string => m.toUpperCase())
				.map((m: string): string => day.length === 3 ? m.substring(0, 3) : m)
				.indexOf(day.toUpperCase());
	}
	
	public static getWeekDate(date: Date, startDay: number = 0): Date {
		if (startDay < 0 || startDay > 6) throw new Error('Trying to get week start for day outside 0-6');
		
		const clone: Date = new Date(date.getTime());
		CommonsDate.truncateToDate(clone);
		
		while (clone.getDay() !== startDay) {
			clone.setDate(clone.getDate() - 1);
		}
		
		return clone;
	}

	public static getNumberOfDaysInMonth(month: number, year: number): number {
		switch (CommonsDate.getTextMonth(month)) {
			case 'September':
			case 'April':
			case 'June':
			case 'November':
				return 30;
			case 'February':
				return CommonsDate.isLeapYear(year) ? 29 : 28;
			default:
				return 31;
		}
	}

	public static getDayOfYear(date: Date): number {
		let days: number = 0;
		
		for (let i = 0; i < date.getMonth(); i++) {
			days += CommonsDate.getNumberOfDaysInMonth(i, date.getFullYear());
		}
		
		days += date.getDate();
		
		return days;
	}
	
	public static listTwoDigitDays(days: number): string[] {
		return CommonsNumber.fixedDigitRange(1, days, 2);
	}

	public static dateToBashTimestamp(date: Date, iso: boolean = false): string {
		const parts: string[] = CommonsDate.dateTodmY(date, iso).split('/');
		
		return `${parts[0]} ${CommonsDate.getTextMonth(date.getMonth()).toUpperCase().substring(0, 3)} ${parts[2]} ${CommonsDate.dateToHis(date)}`;
	}
	
	public static dateToCompressed(date: Date, iso: boolean = false): string {
		return CommonsDate.dateToYmdHis(date, iso).replace(/[-: ]/g, '');
	}
	
	public static compressedToDate(date: string, iso: boolean = false): Date {
		const match: string[]|null = date.match(COMMONS_REGEX_PATTERN_DATETIME_YMDHIS_COMPRESSED);
		if (match === null) throw new Error('Unable to parse compressed date');
		
		return CommonsDate.YmdHisToDate(`${match[1]}-${match[2]}-${match[3]} ${match[4]}:${match[5]}:${match[6]}`, iso);
	}
	
	public static buildDateFromYmdAndOptionals(
			ymd: string,
			hour?: number,
			minute?: number,
			second?: number,
			millisecond?: number,
			iso: boolean = false
	): Date {
		const base: Date = CommonsDate.YmdToDate(ymd, iso);
		
		if (hour !== undefined) base.setHours(hour);
		if (minute !== undefined) base.setMinutes(minute);
		if (second !== undefined) base.setSeconds(second);
		if (millisecond !== undefined) base.setMilliseconds(millisecond);
		
		return base;
	}
	
	public static truncateToSecond(date: Date): void {
		date.setMilliseconds(0);
	}
	
	public static truncateToMinute(date: Date): void {
		CommonsDate.truncateToSecond(date);
		date.setSeconds(0);
	}
	
	public static truncateToHour(date: Date): void {
		CommonsDate.truncateToMinute(date);
		date.setMinutes(0);
	}
	
	public static truncateToDate(date: Date): void {
		CommonsDate.truncateToHour(date);
		date.setHours(0);
	}
	
	public static getPastTime(
			date: Date,
			time: Date
	): Date {
		const clone: Date = new Date(date.getTime());
		
		clone.setHours(time.getHours());
		clone.setMinutes(time.getMinutes());
		clone.setSeconds(time.getSeconds());
		clone.setMilliseconds(time.getMilliseconds());
		
		if (clone.getTime() > date.getTime()) {
			// timestamp is 'yesterday'
			clone.setDate(clone.getDate() - 1);
		}
		
		return clone;
	}
	
	public static buildClosestDateFromTime(
			original: Date,
			hour: number,
			minute: number,
			second?: number,
			millisecond?: number,
			magic12Hours: boolean = false
	): Date {
		if (magic12Hours && hour < 13) {
			const magicHour: number = (hour + 12) % 24;
			
			const normalResult: Date = CommonsDate.buildClosestDateFromTime(
					original,
					hour,
					minute,
					second,
					millisecond
			);
			const magicResult: Date = CommonsDate.buildClosestDateFromTime(
					original,
					magicHour,
					minute,
					second,
					millisecond
			);
			
			const normalDelta: number = Math.abs(original.getTime() - normalResult.getTime());
			const magicDelta: number = Math.abs(original.getTime() - magicResult.getTime());
			
			if (magicDelta < normalDelta) return magicResult;
			return normalResult;
		}
		
		const clone: Date = new Date(original.getTime());
		
		if (hour !== undefined) clone.setHours(hour);
		if (minute !== undefined) clone.setMinutes(minute);
		if (second !== undefined) clone.setSeconds(second);
		if (millisecond !== undefined) clone.setMilliseconds(millisecond);

		clone.setDate(clone.getDate() - 1);
		
		let closest: Date|undefined;
		let minDelta: number|undefined;
		for (let i = -1; i <= 1; i++) {
			const delta: number = Math.abs(clone.getTime() - original.getTime());
			if (minDelta === undefined || delta < minDelta) {
				closest = new Date(clone.getTime());
				minDelta = delta;
			}
			
			clone.setDate(clone.getDate() + 1);
		}
			
		if (!closest) throw new Error('Unable to match closest. This should not be possible');
			
		return closest;
	}
	
	// NB, this may not cope with daylight saving changes
	public static fillEmptyMinutes(
			timestamps: Date[],
			interval: number = 1
	): Date[] {
		let min: Date|undefined;
		let max: Date|undefined;
		
		for (const t of timestamps) {
			if (min === undefined || t.getTime() < min.getTime()) min = new Date(t.getTime());
			if (max === undefined || t.getTime() > max.getTime()) max = new Date(t.getTime());
		}
		
		if (min === undefined || max === undefined) return [];

		CommonsDate.truncateToMinute(min);
		
		const range: Date[] = [];
		while (min.getTime() <= max.getTime()) {
			range.push(new Date(min.getTime()));
			
			min.setMinutes(min.getMinutes() + interval);
		}
		
		return range;
	}
	
	// NB, this may not cope with daylight saving changes
	public static fillEmptyHours(timestamps: Date[]): Date[] {
		let min: Date|undefined;
		let max: Date|undefined;
		
		for (const t of timestamps) {
			if (min === undefined || t.getTime() < min.getTime()) min = new Date(t.getTime());
			if (max === undefined || t.getTime() > max.getTime()) max = new Date(t.getTime());
		}
		
		if (min === undefined || max === undefined) return [];

		CommonsDate.truncateToHour(min);
		
		const range: Date[] = [];
		while (min.getTime() <= max.getTime()) {
			range.push(new Date(min.getTime()));
			
			min.setHours(min.getHours() + 1);
		}
		
		return range;
	}
	
	// NB, this may not cope with daylight saving changes
	public static fillEmptyDays(timestamps: Date[]): Date[] {
		let min: Date|undefined;
		let max: Date|undefined;
		
		for (const t of timestamps) {
			if (min === undefined || t.getTime() < min.getTime()) min = new Date(t.getTime());
			if (max === undefined || t.getTime() > max.getTime()) max = new Date(t.getTime());
		}
		
		if (min === undefined || max === undefined) return [];
		
		CommonsDate.truncateToDate(min);
		
		const range: Date[] = [];
		while (min.getTime() <= max.getTime()) {
			range.push(new Date(min.getTime()));
			
			min.setDate(min.getDate() + 1);
		}
		
		return range;
	}
	
	public static getDateRangeFromDateArray(dates: Readonly<(Readonly<Date>)[]>): TDateRange {
		if (dates.length === 0) throw new Error('No dates to derive range from');
		
		const min: Date = new Date(dates[0].getTime());
		const max: Date = new Date(dates[0].getTime());
		
		for (const date of dates.slice(1)) {
			if (date.getTime() < min.getTime()) min.setTime(date.getTime());
			if (date.getTime() > max.getTime()) max.setTime(date.getTime());
		}
		
		return {
				from: min,
				to: max
		};
	}
	
	public static getDateArrayFromDateRange(range: Readonly<TDateRange>): Date[] {
		const dates: Date[] = [ range.from, range.to ];
		
		return CommonsDate.fillEmptyDays(dates);
	}
	
	public static getHourArrayFromDateRange(range: Readonly<TDateRange>): Date[] {
		const dates: Date[] = [ range.from, range.to ];
		
		return CommonsDate.fillEmptyHours(dates);
	}
	
	public static phpDate(format: string, date?: Date, iso: boolean = false): string {
		if (!date) date = new Date();
		
		const components: TComponents = iso ? CommonsDate.parseIsoString(date) : CommonsDate.fromDate(date);

		const fixedDate: Date = date;
		return format
				.split('')
				.map((c: string): string => {
					switch (c) {
						case 'd':
							return components.d;
						case 'D':
							return CommonsDate.getTextDay(fixedDate.getDay()).substr(0, 3);
						case 'j':
							return parseInt(components.d, 10).toString(10);
						case 'l':
							return CommonsDate.getTextDay(fixedDate.getDay());
						case 'S':
							return CommonsNumber.ordinalUnit(parseInt(components.d, 10));
						case 'w':
							return fixedDate.getDay().toString(10);
						case 'F':
							return CommonsDate.getTextMonth(parseInt(components.m, 10) - 1);
						case 'm':
							return components.m;
						case 'M':
							return CommonsDate.getTextMonth(parseInt(components.m, 10) - 1).substr(0, 3);
						case 'n':
							return parseInt(components.m, 10).toString(10);
						case 'Y':
							return components.y;
						case 'y':
							return components.y.substr(2);
						case 'a': {
							const h: number = parseInt(components.h, 10);
							return h < 12 ? 'am' : 'pm';
						}
						case 'A': {
							const h: number = parseInt(components.h, 10);
							return h < 12 ? 'AM' : 'PM';
						}
						case 'g': {
							const h: number = parseInt(components.h, 10) % 12;
							if (h === 0) return '12';
							return h.toString(10);
						}
						case 'G':
							return parseInt(components.h, 10).toString(10);
						case 'h': {
							const h: number = parseInt(components.h, 10) % 12;
							if (h === 0) return '12';
							return h.toString(10).padStart(2, '0');
						}
						case 'H':
							return components.h;
						case 'i':
							return components.i;
						case 's':
							return components.s;
						default:
							return c;
					}
				})
				.join('');
	}
	
	public static getDeltaHours(range: TDateRange, inclusive: boolean = false): number {
		const from: Date = new Date(range.from.getTime());
		const to: Date = new Date(range.to.getTime());
		
		CommonsDate.truncateToHour(from);
		CommonsDate.truncateToHour(to);
		
		// we use this slightly ineffecient way to avoid BST
		let count: number = 0;
		while (from.getTime() < to.getTime()) {
			count++;
			from.setHours(from.getHours() + 1);
		}
		
		return count + (inclusive ? 1 : 0);
	}
	
	public static getDeltaDays(range: TDateRange, inclusive: boolean = false, iso: boolean = false): number {
		const from: Date = CommonsDate.dateWithoutTime(range.from, iso);
		const to: Date = CommonsDate.dateWithoutTime(range.to, iso);
		
		// we use this slightly ineffecient way to avoid BST
		let count: number = 0;
		while (from.getTime() < to.getTime()) {
			count++;
			from.setDate(from.getDate() + 1);
		}
		
		return count + (inclusive ? 1 : 0);
	}
	
	public static getDeltaMonths(range: TDateRange, inclusive: boolean = false, iso: boolean = false): number {
		const from: Date = CommonsDate.dateWithoutTime(range.from, iso);
		const to: Date = CommonsDate.dateWithoutTime(range.to, iso);

		from.setDate(1);
		to.setDate(1);
		
		// we use this slightly ineffecient way to avoid BST
		let count: number = 0;
		while (from.getTime() < to.getTime()) {
			count++;
			from.setMonth(from.getMonth() + 1);
		}
		
		return count + (inclusive ? 1 : 0);
	}
	
	public static getDeltaYears(range: TDateRange, inclusive: boolean = false, iso: boolean = false): number {
		const from: Date = CommonsDate.dateWithoutTime(range.from, iso);
		const to: Date = CommonsDate.dateWithoutTime(range.to, iso);

		from.setDate(1);
		from.setMonth(0);
		to.setDate(1);
		to.setMonth(0);
		
		// we use this slightly ineffecient way to avoid BST
		let count: number = 0;
		while (from.getTime() < to.getTime()) {
			count++;
			from.setFullYear(from.getFullYear() + 1);
		}
		
		return count + (inclusive ? 1 : 0);
	}
	
	public static prettyDate(
			date: Date,
			absolute: boolean = false,
			now?: Date,
			includeYear: boolean = true,
			iso: boolean = false
	): string {
		if (!now) now = new Date();

		if (!absolute) {	// default enabled
			const clone: Date = new Date(now.getTime());
			
			if (CommonsDate.isSameDate(date, clone)) return 'Today';
			
			clone.setDate(now.getDate() - 1);
			if (CommonsDate.isSameDate(date, clone)) return 'Yesterday';
			
			clone.setDate(now.getDate() + 1);
			if (CommonsDate.isSameDate(date, clone)) return 'Tomorrow';
		}
		
		return CommonsDate.phpDate(
				`D d M${includeYear ? ' Y' : ''}`,
				date,
				iso
		);
	}
	
	private static getDeltaSeconds(
			timestamp: Date,
			future: Date
	): number|undefined {
		const delta: number = (future.getTime() - timestamp.getTime()) / 1000;
		
		if (delta < 0) return undefined;
		
		return delta;
	}
	
	public static prettyTime(
			time: Date,
			absolute: boolean = false,
			now?: Date,
			iso: boolean = false
	): string {
		if (!now) now = new Date();

		if (!absolute) {	// default enabled
			const seconds: number|undefined = CommonsDate.getDeltaSeconds(time, now);
			
			if (seconds !== undefined) {
				if (seconds < 60) return 'Just now';
	
				if (seconds < 120) return '1 min ago';
				
				if (seconds < (10 * 60)) {
					const mins: number = Math.floor(seconds / 60);
					return `${mins} mins ago`;
				}
			}
		}
			
		return CommonsDate.phpDate(
				'H:i',
				time,
				iso
		);
	}
	
	public static prettyDateTime(
			timestamp: Date,
			absolute: boolean = false,
			now?: Date,
			includeYear: boolean = true,
			iso: boolean = false
	): string {
		if (!now) now = new Date();

		if (!absolute) {	// default enabled
			const seconds: number|undefined = CommonsDate.getDeltaSeconds(timestamp, now);
			
			if (seconds !== undefined && seconds < (10 * 60)) {
				return CommonsDate.prettyTime(
						timestamp,
						false,
						now,
						iso
				);
			}
		}
		
		return `${CommonsDate.prettyDate(timestamp, absolute, now, includeYear, iso)} ${CommonsDate.prettyTime(timestamp, absolute, now, iso)}`;
	}
	
	public static prettyTimeRange(
			range: TDateRange,
			absolute: boolean = true,	// inverse
			now?: Date,
			iso: boolean = false
	): string {
		const sFrom: string = CommonsDate.prettyTime(
				range.from,
				absolute,
				now,
				iso
		);
		const sTo: string = CommonsDate.prettyTime(
				range.to,
				absolute,
				now,
				iso
		);
		
		if (CommonsDate.isSameDate(range.from, range.to) && sFrom === sTo) return sFrom;
		
		return `${sFrom} - ${sTo}`;
	}
	
	public static prettyDateTimeRange(
			range: TDateRange,
			absolute?: boolean,	// not default value, so can apply the different defaults in prettyDate and prettyTimeRange
			now?: Date,
			includeYear: boolean = true,
			iso: boolean = false
	): string {
		const absoluteDate: boolean = absolute === undefined ? false : absolute;
		const absoluteTime: boolean = absolute === undefined ? true : absolute;

		if (CommonsDate.isSameDate(range.from, range.to)) {
			return `${CommonsDate.prettyDate(range.from, absoluteDate, now, includeYear, iso)} ${CommonsDate.prettyTimeRange(range, absoluteTime, now, iso)}`;
		}
		
		return `${CommonsDate.prettyDate(range.from, absoluteDate, now, includeYear, iso)} ${CommonsDate.prettyTime(range.from, absoluteTime, now, iso)} - ${CommonsDate.prettyDate(range.to, absoluteDate, now, includeYear, iso)} ${CommonsDate.prettyTime(range.to, absoluteTime, now, iso)}`;
	}

	public static sort(values: Date[]): Date[] {
		return values
				.sort((a: Date, b: Date): number => a.getTime() - b.getTime());
	}
}
