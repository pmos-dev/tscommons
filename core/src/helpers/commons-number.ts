import { CommonsString } from './commons-string';

export abstract class CommonsNumber {
	public static fillIntRange(a: number, b: number): number[] {
		const range: number[] = [];
		for (let v = a; v <= b; v++) range.push(v);
		
		return range;
	}
	
	public static symmetricFloor(value: number): number {
		if (value < 0) return Math.ceil(value);
		return Math.floor(value);
	}
	
	public static symmetricCeil(value: number): number {
		if (value < 0) return Math.floor(value);
		return Math.ceil(value);
	}
	
	public static fixedDigitRange(min: number, max: number, digits?: number): string[] {
		const ds: number = digits ? digits : Math.floor(Math.log10(max));

		const nums: string[] = [];
		for (let i = min; i <= max; i++) {
			let n: string = i.toString();
			while (n.length < ds) n = `0${n}`;
			nums.push(n);
		}
		
		return nums;
	}
	
	public static randRange(min: number, max: number, inclusive: boolean = false): number {
		if (min === max) return min;
		
		const delta: number = (max - min) + (inclusive ? 1 : 0);
		
		return min + Math.floor(Math.random() * delta);
	}
	
	public static longRandom(length: number): string {
		let rand: string = CommonsNumber.randRange(1, 10).toString();
		while (rand.length < length) {
			rand += CommonsNumber.randRange(0, 10).toString();
		}
		
		return rand;
	}
	
	public static hertz(frequency: number): number {
		return 1000 / frequency;
	}
	
	public static sort(values: number[]): number[] {
		return values
				.sort((a: number, b: number): number => a - b);
	}
	
	public static percentile(values: number[], percentile: number): number|undefined {
		if (values.length === 0) return undefined;
		
		if (percentile < 0 || percentile > 1) throw new Error('Invalid percentile');

		const sorted: number[] = CommonsNumber.sort(values.slice());
		if (percentile === 0) return sorted[0];
		if (percentile === 1) return sorted[values.length - 1];
		
		const index: number = Math.round((values.length - 1) * percentile);
		
		return sorted[index];
	}
	
	public static quartile(values: number[], q: number): number|undefined {
		if (values.length === 0) return undefined;

		if (![ 1, 2, 3].includes(q)) throw new Error('Invalid quartile');
		
		return CommonsNumber.percentile(values, q / 4);
	}
	
	public static average(values: number[]): number|undefined {
		if (values.length === 0) return undefined;
		
		let total: number = 0;
		for (const value of values) total += value;
		
		return total / values.length;
	}
	
	public static quartileAverage(values: number[]): number|undefined {
		if (values.length === 0) return undefined;

		const q1: number|undefined = CommonsNumber.quartile(values, 1);
		const q2: number|undefined = CommonsNumber.quartile(values, 2);
		const q3: number|undefined = CommonsNumber.quartile(values, 3);
		
		if (q1 === undefined || q2 === undefined || q3 === undefined) return undefined;
		
		return CommonsNumber.average([ q1, q2, q3 ]);
	}
	
	public static mean(values: number[]): number|undefined {
		return CommonsNumber.average(values);
	}
	
	public static standardDeviation(values: number[]): number|undefined {
		if (values.length === 0) return 0;
		
		const mean: number|undefined = CommonsNumber.mean(values);
		if (mean === undefined) return undefined;
		
		return CommonsNumber.mean(
				values
						.map((value: number): number => Math.pow((value - mean), 2))
		);
	}
	
	public static modes(values: number[]): number[] {
		if (values.length === 0) return [];
		
		const tallies: Map<number, number> = new Map<number, number>();
		
		for (const value of values) {
			if (!tallies.has(value)) tallies.set(value, 1);
			else tallies.set(value, tallies.get(value)! + 1);
		}
		
		let maxTally: number|undefined;
		let minTally: number|undefined;
		for (const value of values) {
			const tally: number = tallies.get(value)!;
			if (maxTally === undefined || tally > maxTally) maxTally = tally;
			if (minTally === undefined || tally < minTally) minTally = tally;
		}
		if (maxTally === minTally) return [];	// no meaningful modes, i.e. every value is a mode
		
		const maxValues: number[] = [];
		for (const value of values) {
			if (tallies.get(value)! === maxTally) maxValues.push(value);
		}
		
		return maxValues;
	}
	
	public static median(values: number[]): number|undefined {
		if (values.length === 0) return undefined;
		
		return CommonsNumber.quartile(values, 2);
	}
	
	public static trimDecimalZeros(value: number|string): string {
		let s: string = value.toString();
		
		if (s.indexOf('.') === -1) return s;
		
		s = CommonsString.rtrim(s, '0');
		if (s.substr(s.length - 1, 1) === '.') s = CommonsString.rtrim(s, '.');
		
		return s;
	}
	
	public static prettyFigure(value: number, absolute: boolean = false): string {
		if (value < 1) return CommonsNumber.trimDecimalZeros(value.toPrecision(3));
		if (value < 10) return CommonsNumber.trimDecimalZeros(value.toPrecision(4));
		if (value < 100) return CommonsNumber.trimDecimalZeros(value.toPrecision(4));
		if (value < 1000) return CommonsNumber.trimDecimalZeros(value.toPrecision(4));
		
		value = Math.round(value);
		if (value < (1 * 1000)) return value.toString();
		if (absolute || value < (10 * 1000)) return value.toLocaleString();
		if (value < (1000 * 1000)) return `${parseFloat((value / 1000).toPrecision(3))}K`;
		return `${parseFloat((value / (1000 * 1000)).toPrecision(2))}M`;
	}
	
	public static prettyFileSize(bytes: number, binaryPrefix: boolean = false): string {
		const magnitude: number = binaryPrefix ? 1024 : 1000;

		if (bytes < Math.pow(magnitude, 1)) return bytes.toString();
		if (bytes < (10 * Math.pow(magnitude, 1))) return bytes.toLocaleString();

		let exponent: number = 2;
		for (const unit of 'KMGTPEZY'.split('')) {
			if (bytes < Math.pow(magnitude, exponent)) return `${parseFloat((bytes / Math.pow(magnitude, exponent - 1)).toPrecision(3))}${unit}`;
			exponent++;
		}

		return `Oversize`;
	}

	public static prettyPercent(original: number, whole: boolean = false): string {
		let value: number = original * 100;
		
		if (whole) {
			value = Math.round(value);
			return `${value}%`;
		}
		
		if (value === 100) return '100%';
		if (value > 100) return Math.round(value) + '%';
		
		const precision: string = value.toPrecision(2);
		if (/[0-9]e\+/.test(precision)) {
			// scientific notation. We don't want this.
			// this only occurs here because 99.5+ rounds to 100+, and 2 deciminal precision doesn't work for 100
			return CommonsNumber.prettyPercent(original, true);
		}
		
		return CommonsNumber.trimDecimalZeros(precision) + '%';
	}
	
	private static asDuration(value: number, unit: string, shortUnit: string, short: boolean = false): string {
		value = Math.round(value);
		return `${value.toLocaleString()}${short ? '' : ' '}${short ? shortUnit : CommonsString.pluralise(unit, value, true)}`;
	}
	
	public static prettyDuration(millis: number, short: boolean = false): string {
		if (isNaN(millis)) return 'NaN';
		
		if (millis < 500) return CommonsNumber.asDuration(millis, 'millisecond', 'ms', short);
		if (millis < 1000) {
			return CommonsNumber.asDuration(millis / 1000, 'second', 's', short);
		}
		
		const seconds: number = millis / 1000;
		if (seconds <= 60) return CommonsNumber.asDuration(seconds, 'second', 's', short);
		
		const minutes: number = millis / (60 * 1000);
		if (minutes <= 60) return CommonsNumber.asDuration(minutes, 'minute', 'mins', short);
		
		const hours: number = millis / (60 * 60 * 1000);
		if (hours <= 24) return CommonsNumber.asDuration(hours, 'hour', 'hrs', short);
		
		const days: number = millis / (24 * 60 * 60 * 1000);
		return CommonsNumber.asDuration(days, 'day', 'days', short);
	}
	
	public static ordinalUnit(value: number): string {
		if (isNaN(value) || value !== Math.floor(value)) throw new Error('Cannot compute ordinal unit for invalid integer');

		const s: string = value.toString();
		if (s.length === 1) {
			switch (s) {
				case '1':	return 'st';
				case '2':	return 'nd';
				case '3':	return 'rd';
				default:	return 'th';
			}
		}

		const last: string = s.substr(s.length - 2);
		if (last[0] === '1') return 'th';
		
		return CommonsNumber.ordinalUnit(parseInt(last[1], 10));
	}
	
	public static prettyOrdinal(value: number): string {
		value = CommonsNumber.symmetricFloor(value);
		
		return `${value.toLocaleString()}${CommonsNumber.ordinalUnit(Math.abs(value))}`;
	}
	
	public static sampleIndices(length: number, samples: number, blockSize: number, allowUnderflow: boolean = false): number[] {
		if (length === 0) return [];
		if (blockSize > length && !allowUnderflow) return [];
	
		if (samples < 1) throw new Error('Minimum of 2 samples required');
		if (blockSize < 1) throw new Error('Minimum of block size 1 required');
		
		let step: number = Math.floor(length / samples);
		if (step < blockSize) step = blockSize;
	
		const indices: number[] = [];
		let index: number = 0;
		while (true) {
			indices.push(index);
			index += step;
	
			if ((index + blockSize) > length) break;
		}
	
		return indices;
	}
	
	public static roughFactor(items: number, factor: number): number[] {
		if (factor === 0) throw new Error('Cannot factor into zero');
		
		if (items < factor) return [ items ];
		
		const div: number = Math.round(items / factor);
		const center: number = div * factor;
		
		const results: number[] = [];
		for (let i = div; i-- > 0;) results.push(factor);
		
		if (items === center) return results;
		
		let ttl: number = items;	// just to protect against infinite loops
		if (items < center) {
			// subtract from the rightmost back
			
			let index: number = results.length - 1;
			while (ttl-- > 0) {
				let sum: number = 0;
				for (const r of results) sum += r;
				if (sum === items) break;
				
				results[index]--;
				index--;
				if (index < 0) index = results.length - 1;
			}
		} else {
			// add from the leftmost forward
			
			let index: number = 0;
			while (ttl-- > 0) {
				let sum: number = 0;
				for (const r of results) sum += r;
				if (sum === items) break;
				
				results[index]++;
				index++;
				if (index === results.length) index = 0;
			}
		}
		
		if (ttl < 0) throw new Error('TTL expired during attempt roughtFactor');
		
		return results;
	}
	
	// this isn't super-accurate, e.g. it computes years as being 364 days, but it's good enough
	public static parsePeriod(delta: string): number {
		delta = delta.trim().toLowerCase();
		if (/^-?[0-9]+(?:\.[0-9]+)?$/.test(delta)) return Math.floor(parseFloat(delta));	// default to milliseconds
		
		const initial: RegExpExecArray|null = /^(-?[0-9]+(?:\.[0-9]+)?)[ ]?([a-z]+)( .+)?$/.exec(delta);
		if (!initial) return NaN;

		let value: number = parseFloat(initial[1]);
		if (isNaN(value)) return NaN;

		const unit: string = initial[2];
		const remainder: string = (initial[3] || '').trim();

		// tslint:disable:no-switch-case-fall-through
		switch (unit) {
			case 'y':
			case 'year':
			case 'years':
				value *= 12;
				// no break fall-through intentional

			case 'month':
			case 'months':
				value *= (52 / 12);
				// no break fall-through intentional

			case 'w':
			case 'week':
			case 'weeks':
				value *= 7;
				// no break fall-through intentional

			case 'd':
			case 'day':
			case 'days':
				value *= 24;
				// no break fall-through intentional

			case 'h':
			case 'hour':
			case 'hours':
				value *= 60;
				// no break fall-through intentional

			case 'm':
			case 'min':
			case 'mins':
			case 'minute':
			case 'minutes':
				value *= 60;
				// no break fall-through intentional

			case 's':
			case 'sec':
			case 'secs':
			case 'second':
			case 'seconds':
				value *= 1000;
				// no break fall-through intentional

			case 'ms':
			case 'milli':
			case 'millis':
			case 'millisecond':
			case 'milliseconds':
				// default to milliseconds
				break;
				
			default:
				return NaN;
		}
		// tslint:enable:no-switch-case-fall-through
		
		if (remainder.length > 0) {
			const add: number = CommonsNumber.parsePeriod(remainder);
			if (isNaN(add)) return NaN;
			
			value += add;
		}
		
		return value;
	}
}
