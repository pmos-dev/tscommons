import { TPropertyObject } from '../types/tproperty-object';

import { CommonsType } from './commons-type';

export abstract class CommonsObject {
	public static mapObject<T>(src: TPropertyObject, mapper: (_value: any, _key?: string) => T): TPropertyObject {
		if (!CommonsType.isObject(src)) throw new Error('Trying to map a non-object');

		const dest: TPropertyObject = {};
		for (const key of Object.keys(src)) dest[key] = mapper(src[key], key);
	
		return dest;
	}
	
	public static isEmpty(src: TPropertyObject): boolean {
		return Object.keys(src).length === 0;
	}
	
	public static stripNulls(value: any): any {
		if (value === undefined || value === null) return undefined;

		if (CommonsType.isDate(value)) return value;
		if (CommonsType.isArray(value)) {
			return value
					.map((item: any): any => CommonsObject.stripNulls(item));
		}
		
		if (CommonsType.isObject(value)) {
			const rebuild: TPropertyObject = {};
			for (const key of Object.keys(value)) {
				const v: any = value[key];
				
				if (v === undefined || v === null) continue;
				
				rebuild[key] = CommonsObject.stripNulls(v);
			}
			
			return rebuild;
		}

		return value;
	}
	
}
