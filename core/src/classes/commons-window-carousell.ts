type TWindowCarousellItem<T> = {
	empty: boolean;
	sort?: number;
	
	item?: T;
};

export class CommonsWindowCarousell<T> {
	private static lowestItem<T>(items: TWindowCarousellItem<T>[]): TWindowCarousellItem<T>|undefined {
		if (items.length === 0) return undefined;
		
		const empty: TWindowCarousellItem<T>|undefined = items
				.find((item: TWindowCarousellItem<T>): boolean => item.empty);
		
		if (empty !== undefined) {
			return empty;
		} else {
			const lowestItems: TWindowCarousellItem<T>[] = items
					.filter((item: TWindowCarousellItem<T>): boolean => !item.empty)
					.sort((a: TWindowCarousellItem<T>, b: TWindowCarousellItem<T>): number => {
						if (a.sort === undefined && b.sort === undefined) return 0;
						if (a.sort === undefined) return -1;
						if (b.sort === undefined) return 1;
						
						if (a.sort < b.sort) return -1;
						if (a.sort > b.sort) return 1;
						return 0;
					});
			
			// It shouldn't be possible to have a non-empty array, no empties and also no filled at the same time.
			// But check anyway, and return undefined if so.
			if (lowestItems.length === 0) return undefined;
			
			return lowestItems[0];
		}
	}

	private display: TWindowCarousellItem<T>[] = [];
	private queue: TWindowCarousellItem<T>[] = [];

	private upto: number = 0;
	
	constructor(
			private windowSize: number,
			private poolSize: number
	) {
		for (let i = 0; i < this.poolSize; i++) {
			this.display.push({ empty: true });
		}
	}
	
	public getArray(): (T|undefined)[] {
		let i: number = 0;
		const window: TWindowCarousellItem<T>[] = this.display
				.filter((_: TWindowCarousellItem<T>|undefined): boolean => i++ < this.windowSize);
		
		const items: (T|undefined)[] = window
				.map((item: TWindowCarousellItem<T>): T|undefined => {
					if (item.empty) return undefined;
					return item.item;
				});
		
		return items;
	}
	
	public addItem(item: T, shiftFirstIfLargerThan?: number): T[] {
		const shifteds: T[] = [];

		if (shiftFirstIfLargerThan !== undefined) {
			if (shiftFirstIfLargerThan < 1) throw new Error('Cannot shift for zero or negative length');
			
			while (this.getQueueSize() >= (shiftFirstIfLargerThan - 1)) {
				const shifted: TWindowCarousellItem<T>|undefined = this.queue.shift();
				if (shifted && shifted.item) shifteds.push(shifted.item);
			}
		}
			
		// it's ok if the upto isn't +1 on the last queue entry (as per shiftFirstIfLargerThan) because it is only used for sorting
		this.queue.push({
			empty: false,
			sort: this.upto++,
			item: item
		});

		return shifteds;
	}
	
	public getQueueSize(): number {
		return this.queue.length;
	}
	
	public getWindowSize(): number {
		return this.windowSize;
	}
	
	public getPoolSize(): number {
		return this.poolSize;
	}
	
	public rotate(): T|undefined {
		const lowest: TWindowCarousellItem<T>|undefined = CommonsWindowCarousell.lowestItem(this.display);
		
		const last: TWindowCarousellItem<T>|undefined = this.display.pop();
		if (last === undefined) throw new Error('Last item in display is undefined. This should not be possible');
		
		let removed: TWindowCarousellItem<T>|undefined;
		
		let use: TWindowCarousellItem<T>|undefined;
		if (this.queue.length === 0) {
			// rotate
			use = last;
		} else {
			if (last.empty || last === lowest) {
				if (last === lowest && last.item) removed = last;
				use = this.queue.shift();
			} else {
				// rotate
				use = last;
			}
		}
		
		if (use !== undefined) this.display.unshift(use);
		
		if (removed && !removed.empty && removed.item) return removed.item;
		return undefined;
	}
}
