import { CommonsType } from '../helpers/commons-type';

export type TDateRange = {
		from: Date;
		to: Date;
};

export function isTDateRange(test: unknown): test is TDateRange {
	if (!CommonsType.hasPropertyDate(test, 'from')) return false;
	if (!CommonsType.hasPropertyDate(test, 'to')) return false;
	
	return true;
}
