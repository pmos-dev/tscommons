import { CommonsType } from '../helpers/commons-type';

export type TInterquartileModel = {
		min: number;
		q1: number;
		median: number;
		q3: number;
		max: number;
};

export function isTInterquartileModel(test: unknown): test is TInterquartileModel {
	if (!CommonsType.hasPropertyNumber(test, 'min')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'q1')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'median')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'q3')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'max')) return false;

	return true;
}

export type TInterquartileHoursModel = TInterquartileModel & {
		hour: number;
};

export function isTInterquartileHoursModel(test: unknown): test is TInterquartileHoursModel {
	if (!isTInterquartileModel(test)) return false;
	
	if (!CommonsType.hasPropertyNumber(test, 'hour')) return false;

	return true;
}

export type TInterquartileDowsModel = TInterquartileModel & {
		dow: number;
};

export function isTInterquartileDowsModel(test: unknown): test is TInterquartileDowsModel {
	if (!isTInterquartileModel(test)) return false;
	
	if (!CommonsType.hasPropertyNumber(test, 'dow')) return false;

	return true;
}
