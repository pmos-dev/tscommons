import { TPrimative } from './tprimative';

export type TEncoded = TPrimative|TEncodedArray|TEncodedObject|null;

export type TEncodedArray = TEncoded[];

export type TEncodedObject = {
		[ name: string ]: TEncoded;
};
