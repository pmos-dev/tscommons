export type TKeyObject<T> = {
		[key: string]: T;
};
