export const MAX_SIGNED_32BIT_INTEGER: number = 2147483647;

// JavaScript only supports up to (2^53)-1, so true 64 bit is not possible
export const MAX_SIGNED_53BIT_INTEGER: number = 9007199254740991;
export const MAX_SIGNED_64BIT_INTEGER: number = MAX_SIGNED_53BIT_INTEGER;
