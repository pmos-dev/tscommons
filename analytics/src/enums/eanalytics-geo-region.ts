import { CommonsType } from 'tscommons-core';

export enum EAnalyticsGeoRegion {
		UK = 'uk',
		WORLD = 'world',
		ALL = 'all'
}

export function toEAnalyticsGeoRegion(type: string): EAnalyticsGeoRegion|undefined {
	switch (type) {
		case EAnalyticsGeoRegion.UK.toString():
			return EAnalyticsGeoRegion.UK;
		case EAnalyticsGeoRegion.WORLD.toString():
			return EAnalyticsGeoRegion.WORLD;
		case EAnalyticsGeoRegion.ALL.toString():
			return EAnalyticsGeoRegion.ALL;
	}
	return undefined;
}

export function fromEAnalyticsGeoRegion(type: EAnalyticsGeoRegion): string {
	switch (type) {
		case EAnalyticsGeoRegion.UK:
			return EAnalyticsGeoRegion.UK.toString();
		case EAnalyticsGeoRegion.WORLD:
			return EAnalyticsGeoRegion.WORLD.toString();
		case EAnalyticsGeoRegion.ALL:
			return EAnalyticsGeoRegion.ALL.toString();
	}
	
	throw new Error('Unknown EAnalyticsGeoRegion');
}

export function isEAnalyticsGeoRegion(test: unknown): test is EAnalyticsGeoRegion {
	if (!CommonsType.isString(test)) return false;
	
	return toEAnalyticsGeoRegion(test) !== undefined;
}

export function keyToEAnalyticsGeoRegion(key: string): EAnalyticsGeoRegion {
	switch (key) {
		case 'UK':
			return EAnalyticsGeoRegion.UK;
		case 'WORLD':
			return EAnalyticsGeoRegion.WORLD;
		case 'ALL':
			return EAnalyticsGeoRegion.ALL;
	}
	
	throw new Error(`Unable to obtain EAnalyticsGeoRegion for key: ${key}`);
}

export const EANALYTICS_GEO_REGIONS: EAnalyticsGeoRegion[] = Object.keys(EAnalyticsGeoRegion)
		.map((e: string): EAnalyticsGeoRegion => keyToEAnalyticsGeoRegion(e));
