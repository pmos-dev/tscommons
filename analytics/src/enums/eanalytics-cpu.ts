import { CommonsType } from 'tscommons-core';

export enum EAnalyticsCpu {
		A68K = '68k',	// the A doesn't mean anything, but enums can't start with numbers
		AMD64 = 'amd64',
		ARM64 = 'arm64',
		AVR = 'avr',
		IA3264 = 'ia3264',
		IRIX64 = 'irix64',
		MIPS64 = 'mips64',
		PARISC = 'parisc',
		PPC = 'ppc',
		SPARC64 = 'sparc64'
}

export function fromEAnalyticsCpu(value: EAnalyticsCpu): string {
	switch (value) {
		case EAnalyticsCpu.A68K:
			return EAnalyticsCpu.A68K.toString();
		case EAnalyticsCpu.AMD64:
			return EAnalyticsCpu.AMD64.toString();
		case EAnalyticsCpu.ARM64:
			return EAnalyticsCpu.ARM64.toString();
		case EAnalyticsCpu.AVR:
			return EAnalyticsCpu.AVR.toString();
		case EAnalyticsCpu.IA3264:
			return EAnalyticsCpu.IA3264.toString();
		case EAnalyticsCpu.IRIX64:
			return EAnalyticsCpu.IRIX64.toString();
		case EAnalyticsCpu.MIPS64:
			return EAnalyticsCpu.MIPS64.toString();
		case EAnalyticsCpu.PARISC:
			return EAnalyticsCpu.PARISC.toString();
		case EAnalyticsCpu.PPC:
			return EAnalyticsCpu.PPC.toString();
		case EAnalyticsCpu.SPARC64:
			return EAnalyticsCpu.SPARC64.toString();
		default:
			throw new Error('Unknown EAnalyticsCpu');
	}
}

export function toEAnalyticsCpu(value: string): EAnalyticsCpu|undefined {
	switch (value.toLowerCase()) {
		case EAnalyticsCpu.A68K.toString():
			return EAnalyticsCpu.A68K;
		case EAnalyticsCpu.AMD64.toString():
			return EAnalyticsCpu.AMD64;
		case EAnalyticsCpu.ARM64.toString():
			return EAnalyticsCpu.ARM64;
		case EAnalyticsCpu.AVR.toString():
			return EAnalyticsCpu.AVR;
		case EAnalyticsCpu.IA3264.toString():
			return EAnalyticsCpu.IA3264;
		case EAnalyticsCpu.IRIX64.toString():
			return EAnalyticsCpu.IRIX64;
		case EAnalyticsCpu.MIPS64.toString():
			return EAnalyticsCpu.MIPS64;
		case EAnalyticsCpu.PARISC.toString():
			return EAnalyticsCpu.PARISC;
		case EAnalyticsCpu.PPC.toString():
			return EAnalyticsCpu.PPC;
		case EAnalyticsCpu.SPARC64.toString():
			return EAnalyticsCpu.SPARC64;
		default:
			return undefined;
	}
}

export function isEAnalyticsCpu(test: any): test is EAnalyticsCpu {
	if (!CommonsType.isString(test)) return false;

	const value: string = test as string;

	return toEAnalyticsCpu(value) !== undefined;
}
