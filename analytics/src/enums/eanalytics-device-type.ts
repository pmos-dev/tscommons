import { CommonsType } from 'tscommons-core';

export enum EAnalyticsDeviceType {
		BOT = 'bot',
		CONSOLE = 'console',
		DESKTOP = 'desktop',
		EMBEDDED = 'embedded',
		PHONE = 'phone',
		TABLET = 'tablet',
		TV = 'tv',
		WEARABLE = 'wearable'
}

export function fromEAnalyticsDeviceType(value: EAnalyticsDeviceType): string {
	switch (value) {
		case EAnalyticsDeviceType.BOT:
			return EAnalyticsDeviceType.BOT.toString();
		case EAnalyticsDeviceType.CONSOLE:
			return EAnalyticsDeviceType.CONSOLE.toString();
		case EAnalyticsDeviceType.DESKTOP:
			return EAnalyticsDeviceType.DESKTOP.toString();
		case EAnalyticsDeviceType.EMBEDDED:
			return EAnalyticsDeviceType.EMBEDDED.toString();
		case EAnalyticsDeviceType.PHONE:
			return EAnalyticsDeviceType.PHONE.toString();
		case EAnalyticsDeviceType.TABLET:
			return EAnalyticsDeviceType.TABLET.toString();
		case EAnalyticsDeviceType.TV:
			return EAnalyticsDeviceType.TV.toString();
		case EAnalyticsDeviceType.WEARABLE:
			return EAnalyticsDeviceType.WEARABLE.toString();
		default:
			throw new Error('Unknown EAnalyticsDeviceType');
	}
}

export function toEAnalyticsDeviceType(value: string): EAnalyticsDeviceType|undefined {
	switch (value.toLowerCase()) {
		case EAnalyticsDeviceType.BOT.toString():
			return EAnalyticsDeviceType.BOT;
		case EAnalyticsDeviceType.CONSOLE.toString():
			return EAnalyticsDeviceType.CONSOLE;
		case EAnalyticsDeviceType.DESKTOP.toString():
			return EAnalyticsDeviceType.DESKTOP;
		case EAnalyticsDeviceType.EMBEDDED.toString():
			return EAnalyticsDeviceType.EMBEDDED;
		case EAnalyticsDeviceType.PHONE.toString():
			return EAnalyticsDeviceType.PHONE;
		case EAnalyticsDeviceType.TABLET.toString():
			return EAnalyticsDeviceType.TABLET;
		case EAnalyticsDeviceType.TV.toString():
			return EAnalyticsDeviceType.TV;
		case EAnalyticsDeviceType.WEARABLE.toString():
			return EAnalyticsDeviceType.WEARABLE;
		default:
			return undefined;
	}
}

// different detection libraries return different ones, so we include a selection here
export function parseEAnalyticsDeviceType(value: string): EAnalyticsDeviceType|undefined {
	const attempt: EAnalyticsDeviceType|undefined = toEAnalyticsDeviceType(value);
	if (attempt !== undefined) return attempt;
	
	switch (value.toLowerCase()) {
		case 'bot':
			return EAnalyticsDeviceType.BOT;
			
		case 'console':
			return EAnalyticsDeviceType.CONSOLE;
			
		case 'desktop':
			return EAnalyticsDeviceType.DESKTOP;
			
		case 'embedded':
			return EAnalyticsDeviceType.EMBEDDED;

		case 'mobile':
		case 'phone':
			return EAnalyticsDeviceType.PHONE;
		
		case 'tablet':
			return EAnalyticsDeviceType.TABLET;
		
		case 'tv':
		case 'smarttv':
			return EAnalyticsDeviceType.TV;
		
		case 'watch':
		case 'wearable':
			return EAnalyticsDeviceType.WEARABLE;

		default:
			return undefined;
	}
}

export function isEAnalyticsDeviceType(test: any): test is EAnalyticsDeviceType {
	if (!CommonsType.isString(test)) return false;

	const value: string = test as string;

	return toEAnalyticsDeviceType(value) !== undefined;
}
