import { CommonsType } from 'tscommons-core';

export interface IAnalyticsLiveAssociate {
		uid: string;
		session: string;
}

export function isIAnalyticsLiveAssociate(test: any): test is IAnalyticsLiveAssociate {
	if (!CommonsType.hasPropertyString(test, 'uid')) return false;
	if (!CommonsType.hasPropertyString(test, 'session')) return false;
	
	return true;
}
