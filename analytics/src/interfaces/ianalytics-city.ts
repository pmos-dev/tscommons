import { CommonsType } from 'tscommons-core';
import { ICommonsGeographic, isICommonsGeographic } from 'tscommons-geographics';

export interface IAnalyticsCity {
		city?: string;
		country?: string;
		continent?: string;
		postCode?: string;
		location?: ICommonsGeographic;
}

export function isIAnalyticsCity(test: any): test is IAnalyticsCity {
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'city')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'country')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'continent')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'postCode')) return false;
	if (!CommonsType.hasPropertyTOrUndefined<ICommonsGeographic>(test, 'location', isICommonsGeographic)) return false;
	
	return true;
}
