import { CommonsType } from 'tscommons-core';

import { IAnalyticsGeo, isIAnalyticsGeo } from './ianalytics-geo';

export interface IAnalyticsLiveHit {
		timestamp: Date;
		session?: string;
		geo?: IAnalyticsGeo;
}

export function isIAnalyticsLiveHit(test: any): test is IAnalyticsLiveHit {
	if (!CommonsType.hasPropertyDate(test, 'timestamp')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'session')) return false;
	if (!CommonsType.hasPropertyT<IAnalyticsGeo>(test, 'geo', isIAnalyticsGeo)) return false;
	
	return true;
}
