import { CommonsType } from 'tscommons-core';
import { CommonsBase62 } from 'tscommons-core';

import { EAnalyticsCaptureMethod } from '../enums/eanalytics-capture-method';

import { IAnalyticsGeo, isIAnalyticsGeo } from './ianalytics-geo';
import { IAnalyticsDevice, isIAnalyticsDevice } from './ianalytics-device';

export interface IAnalyticsHit {
		timestamp: Date;
		day: Date;
		hour: Date;
		captureMethod: EAnalyticsCaptureMethod;
		uid: string;
		session?: string;
		unload?: Date;
		geo?: IAnalyticsGeo;
		device?: IAnalyticsDevice;
}

export function isIAnalyticsHit(test: any): test is IAnalyticsHit {
	if (!CommonsType.hasPropertyDate(test, 'timestamp')) return false;
	if (!CommonsType.hasPropertyDate(test, 'day')) return false;
	if (!CommonsType.hasPropertyDate(test, 'hour')) return false;
	if (!CommonsType.hasPropertyString(test, 'captureMethod') || ![ EAnalyticsCaptureMethod.JPEG, EAnalyticsCaptureMethod.PNG, EAnalyticsCaptureMethod.JSON ].includes(test.captureMethod)) return false;
	if (!CommonsType.hasPropertyString(test, 'uid') || !CommonsBase62.isId(test.uid)) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'session') || (test.session !== undefined && !CommonsBase62.isId(test.session))) return false;
	if (!CommonsType.hasPropertyDateOrUndefined(test, 'unload')) return false;
	if (!CommonsType.hasPropertyTOrUndefined<IAnalyticsGeo>(test, 'geo', isIAnalyticsGeo)) return false;
	if (!CommonsType.hasPropertyTOrUndefined<IAnalyticsDevice>(test, 'device', isIAnalyticsDevice)) return false;

	return true;
}
