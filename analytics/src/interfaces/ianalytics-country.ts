import { CommonsType } from 'tscommons-core';

export interface IAnalyticsCountry {
		country?: string;
		continent?: string;
}

export function isIAnalyticsCountry(test: any): test is IAnalyticsCountry {
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'country')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'continent')) return false;
	
	return true;
}
