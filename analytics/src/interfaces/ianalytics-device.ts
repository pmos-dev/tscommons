import { CommonsType } from 'tscommons-core';

import { EAnalyticsDeviceType, isEAnalyticsDeviceType } from '../enums/eanalytics-device-type';
import { EAnalyticsCpu, isEAnalyticsCpu } from '../enums/eanalytics-cpu';
import { EAnalyticsEngine, isEAnalyticsEngine } from '../enums/eanalytics-engine';

export interface IAnalyticsDevice {
		type?: EAnalyticsDeviceType;
		cpu?: EAnalyticsCpu;
		vendor?: string;
		model?: string;
		os?: string;
		osVersion?: number;
		engine?: EAnalyticsEngine;
		engineVersion?: number;
		browser?: string;
		browserVersion?: number;
}

export function isIAnalyticsDevice(test: any): test is IAnalyticsDevice {
	if (!CommonsType.isObject(test)) return false;
	
	if (!CommonsType.hasPropertyEnumOrUndefined<EAnalyticsDeviceType>(test, 'type', isEAnalyticsDeviceType)) return false;
	if (!CommonsType.hasPropertyEnumOrUndefined<EAnalyticsCpu>(test, 'cpu', isEAnalyticsCpu)) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'vendor')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'model')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'os')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'osVersion')) return false;
	if (!CommonsType.hasPropertyEnumOrUndefined<EAnalyticsEngine>(test, 'engine', isEAnalyticsEngine)) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'engineVersion')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'browser')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'browserVersion')) return false;

	return true;
}
