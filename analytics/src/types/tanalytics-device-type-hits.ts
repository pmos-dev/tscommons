import { CommonsType } from 'tscommons-core';

import { EAnalyticsDeviceType, isEAnalyticsDeviceType } from '../enums/eanalytics-device-type';

export type TAnalyticsDeviceTypeHits = {
		type: EAnalyticsDeviceType;
		tally: number;
};

export function isTAnalyticsDeviceTypeHits(test: any): test is TAnalyticsDeviceTypeHits {
	if (!CommonsType.hasPropertyEnum<EAnalyticsDeviceType>(test, 'type', isEAnalyticsDeviceType)) return false;
	if (!CommonsType.hasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
