import { CommonsType } from 'tscommons-core';

export type TAnalyticsDeviceVersionedHits = {
		name: string;
		version: number|null;
		tally: number;
};

export function isTAnalyticsDeviceVersionedHits(test: unknown): test is TAnalyticsDeviceVersionedHits {
	if (!CommonsType.isObject(test)) return false;	// needed for the !== null checks below
	
	if (!CommonsType.hasPropertyString(test, 'name')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'version') && test['version'] !== null) return false;
	if (!CommonsType.hasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
