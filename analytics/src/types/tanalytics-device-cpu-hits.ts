import { CommonsType } from 'tscommons-core';

import { EAnalyticsCpu, isEAnalyticsCpu } from '../enums/eanalytics-cpu';

export type TAnalyticsDeviceCpuHits = {
		cpu: EAnalyticsCpu;
		tally: number;
};

export function isTAnalyticsDeviceCpuHits(test: any): test is TAnalyticsDeviceCpuHits {
	if (!CommonsType.hasPropertyEnum<EAnalyticsCpu>(test, 'cpu', isEAnalyticsCpu)) return false;
	if (!CommonsType.hasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
