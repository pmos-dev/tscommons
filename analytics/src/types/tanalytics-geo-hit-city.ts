import { CommonsType } from 'tscommons-core';

export type TAnalyticsGeoHitCity = {
		city: string;
		tally: number;
};

export function isTAnalyticsGeoHitCity(test: any): test is TAnalyticsGeoHitCity {
	if (!CommonsType.hasPropertyString(test, 'city')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
