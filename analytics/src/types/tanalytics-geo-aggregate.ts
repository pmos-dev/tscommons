import { CommonsType } from 'tscommons-core';

export type TAnalyticsGeoAggregate = {
		longitude: number;
		latitude: number;
		t: number;
};

export function isTAnalyticsGeoAggregate(test: any): test is TAnalyticsGeoAggregate {
	if (!CommonsType.hasPropertyNumber(test, 'longitude')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'latitude')) return false;
	if (!CommonsType.hasPropertyNumber(test, 't')) return false;
	
	return true;
}
