import { CommonsType } from 'tscommons-core';

export type TAnalyticsTimestampTally = {
		timestamp: Date;
		tally: number;
};

export function isTAnalyticsTimestampTally(test: any): test is TAnalyticsTimestampTally {
	if (!CommonsType.hasPropertyDate(test, 'timestamp')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
