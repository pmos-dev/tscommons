import { CommonsType } from 'tscommons-core';

export type TAnalyticsGeoHitCountry = {
		country: string;
		tally: number;
};

export function isTAnalyticsGeoHitCountry(test: any): test is TAnalyticsGeoHitCountry {
	if (!CommonsType.hasPropertyString(test, 'country')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'tally')) return false;
	
	return true;
}
