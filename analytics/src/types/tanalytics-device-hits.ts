import { CommonsType } from 'tscommons-core';

export type TAnalyticsDeviceHits = {
		vendor: string|null;
		model: string|null;
		tally: number;
};

export function isTAnalyticsDeviceHits(test: unknown): test is TAnalyticsDeviceHits {
	if (!CommonsType.isObject(test)) return false;	// needed for the !== null checks below
	
	if (!CommonsType.hasPropertyString(test, 'vendor') && test['vendor'] !== null) return false;
	if (!CommonsType.hasPropertyString(test, 'model') && test['model'] !== null) return false;
	if (!CommonsType.hasPropertyNumber(test, 'tally')) return false;
	
	if (!test['vendor'] && !test['model']) return false;
	
	return true;
}
