import { CommonsType } from 'tscommons-core';

export enum ECommonsMoveDirection {
		UP = 'up',
		DOWN = 'down',
		TOP = 'top',
		BOTTOM = 'bottom',
		LEFT = 'left',
		RIGHT = 'right'
}

export function toECommonsMoveDirection(type: string): ECommonsMoveDirection|undefined {
	switch (type) {
		case ECommonsMoveDirection.UP.toString():
			return ECommonsMoveDirection.UP;
		case ECommonsMoveDirection.DOWN.toString():
			return ECommonsMoveDirection.DOWN;
		case ECommonsMoveDirection.TOP.toString():
			return ECommonsMoveDirection.TOP;
		case ECommonsMoveDirection.BOTTOM.toString():
			return ECommonsMoveDirection.BOTTOM;
		case ECommonsMoveDirection.LEFT.toString():
			return ECommonsMoveDirection.LEFT;
		case ECommonsMoveDirection.RIGHT.toString():
			return ECommonsMoveDirection.RIGHT;
	}
	return undefined;
}

export function fromECommonsMoveDirection(type: ECommonsMoveDirection): string {
	switch (type) {
		case ECommonsMoveDirection.UP:
			return ECommonsMoveDirection.UP.toString();
		case ECommonsMoveDirection.DOWN:
			return ECommonsMoveDirection.DOWN.toString();
		case ECommonsMoveDirection.TOP:
			return ECommonsMoveDirection.TOP.toString();
		case ECommonsMoveDirection.BOTTOM:
			return ECommonsMoveDirection.BOTTOM.toString();
		case ECommonsMoveDirection.LEFT:
			return ECommonsMoveDirection.LEFT.toString();
		case ECommonsMoveDirection.RIGHT:
			return ECommonsMoveDirection.RIGHT.toString();
	}
	
	throw new Error('Unknown ECommonsMoveDirection');
}

export function isECommonsMoveDirection(test: unknown): test is ECommonsMoveDirection {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsMoveDirection(test) !== undefined;
}

export function keyToECommonsMoveDirection(key: string): ECommonsMoveDirection {
	switch (key) {
		case 'UP':
			return ECommonsMoveDirection.UP;
		case 'DOWN':
			return ECommonsMoveDirection.DOWN;
		case 'TOP':
			return ECommonsMoveDirection.TOP;
		case 'BOTTOM':
			return ECommonsMoveDirection.BOTTOM;
		case 'LEFT':
			return ECommonsMoveDirection.LEFT;
		case 'RIGHT':
			return ECommonsMoveDirection.RIGHT;
	}
	
	throw new Error(`Unable to obtain ECommonsMoveDirection for key: ${key}`);
}

export const ECOMMONS_MOVE_DIRECTIONS: ECommonsMoveDirection[] = Object.keys(ECommonsMoveDirection)
		.map((e: string): ECommonsMoveDirection => keyToECommonsMoveDirection(e));
