import { CommonsType } from 'tscommons-core';

import { ICommonsModel, isICommonsModel } from './icommons-model';

export interface ICommonsFirstClass extends ICommonsModel {
		id: number;
}

export function isICommonsFirstClass(test: unknown): test is ICommonsFirstClass {
	if (!isICommonsModel(test)) return false;
	
	if (!CommonsType.hasPropertyNumber(test, 'id')) return false;

	return true;
}
