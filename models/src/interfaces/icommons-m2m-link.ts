import { CommonsType } from 'tscommons-core';

import { ICommonsFirstClass } from './icommons-first-class';
import { ICommonsModel, isICommonsModel } from './icommons-model';

// tslint:disable-next-line:no-empty-interface
export interface ICommonsM2MLink<
		_A extends ICommonsFirstClass,
		_B extends ICommonsFirstClass
> extends ICommonsModel {
		// can't define the a and b fields as we don't know what they will be called
}

export function isICommonsM2MLink<
		A extends ICommonsFirstClass,
		B extends ICommonsFirstClass
>(test: unknown, aFieldName: string, bFieldName: string): test is ICommonsM2MLink<A, B> {
	if (!isICommonsModel(test)) return false;

	if (!CommonsType.hasPropertyNumber(test, aFieldName)) return false;
	if (!CommonsType.hasPropertyNumber(test, bFieldName)) return false;
	
	return true;
}
