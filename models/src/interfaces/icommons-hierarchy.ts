import { CommonsType } from 'tscommons-core';

import { ICommonsFirstClass } from './icommons-first-class';
import { ICommonsSecondClass, isICommonsSecondClass } from './icommons-second-class';

export interface ICommonsHierarchy<
		P extends ICommonsFirstClass
> extends ICommonsSecondClass<P> {
		// can't define the firstClassField, ordered or parent fields as we don't know what it will be called
		ordered: number;
		parent: number|undefined;	// the root can be undefined
}

export function isICommonsHierarchy<
		P extends ICommonsFirstClass
>(test: unknown, firstClassFieldName: string): test is ICommonsHierarchy<P> {
	if (!isICommonsSecondClass(test, firstClassFieldName)) return false;

	if (!CommonsType.hasPropertyNumber(test, 'ordered')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'parent')) return false;
	
	return true;
}
