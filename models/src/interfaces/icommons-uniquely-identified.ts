import { CommonsType, CommonsBase62 } from 'tscommons-core';

export interface ICommonsUniquelyIdentified {
		uid: string;
}

export function isICommonsUniquelyIdentified(test: unknown): test is ICommonsUniquelyIdentified {
	if (!CommonsType.isPropertyObject(test)) return false;

	if (!CommonsType.hasPropertyString(test, 'uid')) return false;
	
	return true;
}

// tslint:disable-next-line:no-empty-interface
export interface ICommonsUniquelyIdentifiedBase62 extends ICommonsUniquelyIdentified {}

export function isICommonsUniquelyIdentifiedBase62(test: unknown): test is ICommonsUniquelyIdentifiedBase62 {
	if (!isICommonsUniquelyIdentified(test)) return false;

	const attempt: ICommonsUniquelyIdentifiedBase62 = test as ICommonsUniquelyIdentifiedBase62;
	
	if (!CommonsBase62.isId(attempt.uid)) return false;
	
	return true;
}
