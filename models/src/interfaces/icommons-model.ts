import { CommonsType } from 'tscommons-core';

// tslint:disable-next-line:no-empty-interface
export interface ICommonsModel {}

export function isICommonsModel(test: unknown): test is ICommonsModel {
	if (!CommonsType.isPropertyObject(test)) return false;
	
	return true;
}
