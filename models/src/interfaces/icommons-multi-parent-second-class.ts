import { CommonsType } from 'tscommons-core';

import { ICommonsFirstClass, isICommonsFirstClass } from './icommons-first-class';

// tslint:disable-next-line:no-empty-interface
export interface ICommonsMultiParentSecondClass extends ICommonsFirstClass {
		// can't define the firstClassField as we don't know what it will be called
}

export function isICommonsMultiParentSecondClass(test: unknown, firstClassFieldNames: string[]): test is ICommonsMultiParentSecondClass {
	if (!isICommonsFirstClass(test)) return false;

	for (const firstClassFieldName of firstClassFieldNames) {
		if (!CommonsType.hasPropertyNumber(test, firstClassFieldName)) return false;
	}
	
	return true;
}
