import { CommonsType } from 'tscommons-core';

import { ICommonsFirstClass, isICommonsFirstClass } from './icommons-first-class';

// tslint:disable-next-line:no-empty-interface
export interface ICommonsSecondClass<
		_P extends ICommonsFirstClass
> extends ICommonsFirstClass {
		// can't define the firstClassField as we don't know what it will be called
}

export function isICommonsSecondClass<
		P extends ICommonsFirstClass
>(test: unknown, firstClassFieldName: string): test is ICommonsSecondClass<P> {
	if (!isICommonsFirstClass(test)) return false;

	if (!CommonsType.hasPropertyNumber(test, firstClassFieldName)) return false;
	
	return true;
}
