// This is a true interface similar to Java as opposed to a pseudo-struct interface as per TypeScript

export interface ICommonsOrderable {
	getOrderedField(): string;
}
