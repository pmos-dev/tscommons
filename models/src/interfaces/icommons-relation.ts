import { ICommonsM2MLink, isICommonsM2MLink } from './icommons-m2m-link';
import { ICommonsFirstClass } from './icommons-first-class';

export interface ICommonsRelation<
		A extends ICommonsFirstClass,
		B extends ICommonsFirstClass
> extends ICommonsM2MLink<A, B> {
		src: number;
		dest: number;
}

export function isICommonsRelation<
		A extends ICommonsFirstClass,
		B extends ICommonsFirstClass
>(test: unknown): test is ICommonsRelation<A, B> {
	if (!isICommonsM2MLink(test, 'src', 'dest')) return false;
	
	return true;
}
