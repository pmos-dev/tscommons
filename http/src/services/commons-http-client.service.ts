import { CommonsType } from 'tscommons-core';
import { CommonsObject } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';

import { THttpHeaderOrParamObject } from '../types/thttp-header-or-param-object';
import { TCommonsHttpRequestOptions, TCommonsHttpInternalRequestOptions } from '../types/tcommons-http-request-options';

import { ECommonsHttpContentType } from '../enums/ecommons-http-content-type';
import { ECommonsHttpResponseDataType } from '../enums/ecommons-http-response-data-type';

export class CommonsHttpTimeoutError extends Error {}

// this is a true interface
export interface ICommonsHttpClientImplementation {
	internalHead(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			options: TCommonsHttpInternalRequestOptions
	): Promise<void>;
	
	internalGet(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array>;

	internalPost<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array>;

	internalPut<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array>;

	internalPatch<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array>;

	internalDelete(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array>;
}

type TReattemptDebugCallback = (
		reattempt: number,
		url: string,
		params: TEncodedObject|undefined,
		headers: TEncodedObject|undefined,
		options: TCommonsHttpRequestOptions|undefined
) => void;

export class CommonsHttpClientService {
	private static buildHeadersOrParams(data: TEncodedObject|undefined): THttpHeaderOrParamObject {
		if (!data) return {};
		if (!CommonsType.isObject(data)) throw new Error('REST data is not an object');
		
		return CommonsObject
				.mapObject(data, (value: any): string => {
					if (value === undefined || value === null) return '';
					return value.toString();
				});
	}
	
	private static appendContentType(
			contentType: ECommonsHttpContentType,
			headers: THttpHeaderOrParamObject|undefined
	): THttpHeaderOrParamObject {
		// immutable; deep doesn't matter as it isn't valid for http headers anyway
		const clone: THttpHeaderOrParamObject = headers === undefined ? {} : { ...headers };
		
		switch (contentType) {
			case ECommonsHttpContentType.FORM_URL:
				clone['Content-Type'] = 'application/x-www-form-urlencoded';
				break;
			case ECommonsHttpContentType.JSON:
				clone['Content-Type'] = 'application/json';
				break;
		}

		return clone;
	}
	
	private reattemptDebugCallback: TReattemptDebugCallback = (
			_reattempt: number,
			_url: string,
			_params: TEncodedObject|undefined,
			_headers: TEncodedObject|undefined,
			_options: TCommonsHttpRequestOptions|undefined
	): void => {
		// do nothing by default
	}
	
	constructor(
			private implementation: ICommonsHttpClientImplementation
	) {}
	
	public setReattemptDebugCallback(callback: TReattemptDebugCallback): void {
		this.reattemptDebugCallback = callback;
	}
	
	public async head<
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			url: string,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional,
			options: TCommonsHttpRequestOptions = {}
	): Promise<void> {
		let maxReattempts: number = options.maxReattempts || 0;
		let reattempt: number = 0;
		
		while (true) {
			try {
				return await this.implementation.internalHead(
						url,
						CommonsHttpClientService.buildHeadersOrParams(params),
						CommonsHttpClientService.buildHeadersOrParams(headers),
						options
				);
			} catch (e) {
				if (e instanceof CommonsHttpTimeoutError && maxReattempts > 0) {
					maxReattempts--;
					reattempt++;
					
					this.reattemptDebugCallback(
							reattempt,
							url,
							params,
							headers,
							options
					);
					
					continue;
				}
				
				throw e;
			}
		}
		
	}
	
	public async get<
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			url: string,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<string|Uint8Array> {
		let maxReattempts: number = options.maxReattempts || 0;
		let reattempt: number = 0;

		while (true) {
			try {
				return await this.implementation.internalGet(
						url,
						CommonsHttpClientService.buildHeadersOrParams(params),
						CommonsHttpClientService.buildHeadersOrParams(headers),
						options.responseDataType || ECommonsHttpResponseDataType.STRING,
						options
				);
			} catch (e) {
				if (e instanceof CommonsHttpTimeoutError && maxReattempts > 0) {
					maxReattempts--;
					reattempt++;
					
					this.reattemptDebugCallback(
							reattempt,
							url,
							params,
							headers,
							options
					);

					continue;
				}
				
				throw e;
			}
		}
	}
	
	public post<
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<string|Uint8Array> {
		const builtHeaders: THttpHeaderOrParamObject = CommonsHttpClientService.appendContentType(
				options.bodyDataEncoding || ECommonsHttpContentType.FORM_URL,
				CommonsHttpClientService.buildHeadersOrParams(headers)
		);

		let maxReattempts: number = options.maxReattempts || 0;
		let reattempt: number = 0;

		while (true) {
			try {
				return this.implementation.internalPost<B>(
						url,
						body,
						CommonsHttpClientService.buildHeadersOrParams(params),
						builtHeaders,
						options.bodyDataEncoding || ECommonsHttpContentType.FORM_URL,
						options.responseDataType || ECommonsHttpResponseDataType.STRING,
						options
				);
			} catch (e) {
				if (e instanceof CommonsHttpTimeoutError && maxReattempts > 0) {
					maxReattempts--;
					reattempt++;
					
					this.reattemptDebugCallback(
							reattempt,
							url,
							params,
							headers,
							options
					);

					continue;
				}
				
				throw e;
			}
		}
	}
	
	public put<
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<string|Uint8Array> {
		const builtHeaders: THttpHeaderOrParamObject = CommonsHttpClientService.appendContentType(
				options.bodyDataEncoding || ECommonsHttpContentType.FORM_URL,
				CommonsHttpClientService.buildHeadersOrParams(headers)
		);

		let maxReattempts: number = options.maxReattempts || 0;
		let reattempt: number = 0;

		while (true) {
			try {
				return this.implementation.internalPut<B>(
						url,
						body,
						CommonsHttpClientService.buildHeadersOrParams(params),
						builtHeaders,
						options.bodyDataEncoding || ECommonsHttpContentType.FORM_URL,
						options.responseDataType || ECommonsHttpResponseDataType.STRING,
						options
				);
			} catch (e) {
				if (e instanceof CommonsHttpTimeoutError && maxReattempts > 0) {
					maxReattempts--;
					reattempt++;
					
					this.reattemptDebugCallback(
							reattempt,
							url,
							params,
							headers,
							options
					);

					continue;
				}
				
				throw e;
			}
		}
	}
	
	public patch<
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<string|Uint8Array> {
		const builtHeaders: THttpHeaderOrParamObject = CommonsHttpClientService.appendContentType(
				options.bodyDataEncoding || ECommonsHttpContentType.FORM_URL,
				CommonsHttpClientService.buildHeadersOrParams(headers)
		);

		let maxReattempts: number = options.maxReattempts || 0;
		let reattempt: number = 0;

		while (true) {
			try {
				return this.implementation.internalPatch<B>(
						url,
						body,
						CommonsHttpClientService.buildHeadersOrParams(params),
						builtHeaders,
						options.bodyDataEncoding || ECommonsHttpContentType.FORM_URL,
						options.responseDataType || ECommonsHttpResponseDataType.STRING,
						options
				);
			} catch (e) {
				if (e instanceof CommonsHttpTimeoutError && maxReattempts > 0) {
					maxReattempts--;
					reattempt++;
					
					this.reattemptDebugCallback(
							reattempt,
							url,
							params,
							headers,
							options
					);

					continue;
				}
				
				throw e;
			}
		}
	}
	
	public delete<
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			url: string,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<string|Uint8Array> {
		let maxReattempts: number = options.maxReattempts || 0;
		let reattempt: number = 0;

		while (true) {
			try {
				return this.implementation.internalDelete(
						url,
						CommonsHttpClientService.buildHeadersOrParams(params),
						CommonsHttpClientService.buildHeadersOrParams(headers),
						options.responseDataType || ECommonsHttpResponseDataType.STRING,
						options
				);
			} catch (e) {
				if (e instanceof CommonsHttpTimeoutError && maxReattempts > 0) {
					maxReattempts--;
					reattempt++;
					
					this.reattemptDebugCallback(
							reattempt,
							url,
							params,
							headers,
							options
					);

					continue;
				}
				
				throw e;
			}
		}
	}
}
