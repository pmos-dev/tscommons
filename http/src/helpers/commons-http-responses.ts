import { CommonsHttpError } from '../classes/commons-http-error.class';
import { CommonsHttpBadRequestError } from '../classes/commons-http-bad-request-error.class';
import { CommonsHttpUnauthorizedError } from '../classes/commons-http-unauthorized-error.class';
import { CommonsHttpForbiddenError } from '../classes/commons-http-forbidden-error.class';
import { CommonsHttpNotFoundError } from '../classes/commons-http-not-found-error.class';
import { CommonsHttpConflictError } from '../classes/commons-http-conflict-error.class';
import { CommonsHttpInternalServerError } from '../classes/commons-http-internal-server-error.class';
import { CommonsHttpNotImplementedError } from '../classes/commons-http-not-implemented-error.class';
import { CommonsHttpServiceUnavailableError } from '../classes/commons-http-service-unavailable-error.class';
import { CommonsHttpSuccess } from '../classes/commons-http-success.class';
import { CommonsHttpOkSuccess } from '../classes/commons-http-ok-success.class';
import { CommonsHttpCreatedSuccess } from '../classes/commons-http-created-success.class';
import { CommonsHttpNoContentSuccess } from '../classes/commons-http-no-content-success.class';

import { ECommonsHttpResponseCode, toECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

export abstract class CommonsHttpResponses {
	public static buildErrorFromResponseCode(code: number, message: string): CommonsHttpError {
		const httpCode: ECommonsHttpResponseCode|undefined = toECommonsHttpResponseCode(code);
		if (httpCode === undefined) {
			return new CommonsHttpError(message, code);
		}
		
		switch (httpCode) {
			case ECommonsHttpResponseCode.BAD_REQUEST:
				return new CommonsHttpBadRequestError(message);
			case ECommonsHttpResponseCode.UNAUTHORIZED:
				return new CommonsHttpUnauthorizedError(message);
			case ECommonsHttpResponseCode.FORBIDDEN:
				return new CommonsHttpForbiddenError(message);
			case ECommonsHttpResponseCode.NOT_FOUND:
				return new CommonsHttpNotFoundError(message);
			case ECommonsHttpResponseCode.CONFLICT:
				return new CommonsHttpConflictError(message);
			case ECommonsHttpResponseCode.INTERNAL_SERVER_ERROR:
				return new CommonsHttpInternalServerError(message);
			case ECommonsHttpResponseCode.NOT_IMPLEMENTED:
				return new CommonsHttpNotImplementedError(message);
			case ECommonsHttpResponseCode.SERVICE_UNAVAILABLE:
				return new CommonsHttpServiceUnavailableError(message);
		}
		
		throw new Error(`Reached unknown ECommonsHttpResponseCode state for ${httpCode}`);
	}

	public static buildSuccessFromResponseCode(code: number): CommonsHttpSuccess {
		const httpCode: ECommonsHttpResponseCode|undefined = toECommonsHttpResponseCode(code);
		if (httpCode === undefined) {
			return new CommonsHttpSuccess(code);
		}
		
		switch (httpCode) {
			case ECommonsHttpResponseCode.OK:
				return new CommonsHttpOkSuccess();
			case ECommonsHttpResponseCode.CREATED:
				return new CommonsHttpCreatedSuccess();
			case ECommonsHttpResponseCode.NO_CONTENT:
				return new CommonsHttpNoContentSuccess();
		}
		
		throw new Error(`Reached unknown ECommonsHttpResponseCode state for ${httpCode}`);
	}
}
