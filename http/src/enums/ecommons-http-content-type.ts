import { CommonsType } from 'tscommons-core';

export enum ECommonsHttpContentType {
		FORM_URL = 'form_url',
		JSON = 'json'
}

export function toECommonsHttpContentType(type: string): ECommonsHttpContentType|undefined {
	switch (type) {
		case ECommonsHttpContentType.FORM_URL.toString():
			return ECommonsHttpContentType.FORM_URL;
		case ECommonsHttpContentType.JSON.toString():
			return ECommonsHttpContentType.JSON;
	}
	return undefined;
}

export function fromECommonsHttpContentType(type: ECommonsHttpContentType): string {
	switch (type) {
		case ECommonsHttpContentType.FORM_URL:
			return ECommonsHttpContentType.FORM_URL.toString();
		case ECommonsHttpContentType.JSON:
			return ECommonsHttpContentType.JSON.toString();
	}
	
	throw new Error('Unknown ECommonsHttpContentType');
}

export function isECommonsHttpContentType(test: unknown): test is ECommonsHttpContentType {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsHttpContentType(test) !== undefined;
}

export function keyToECommonsHttpContentType(key: string): ECommonsHttpContentType {
	switch (key) {
		case 'FORM_URL':
			return ECommonsHttpContentType.FORM_URL;
		case 'JSON':
			return ECommonsHttpContentType.JSON;
	}
	
	throw new Error(`Unable to obtain ECommonsHttpContentType for key: ${key}`);
}

export const ECOMMONS_HTTP_CONTENT_TYPES: ECommonsHttpContentType[] = Object.keys(ECommonsHttpContentType)
		.map((e: string): ECommonsHttpContentType => keyToECommonsHttpContentType(e));
