export enum ECommonsHttpResponseCode {
		OK = 200,
		CREATED = 201,
		NO_CONTENT = 204,
		BAD_REQUEST = 400,
		UNAUTHORIZED = 401,
		FORBIDDEN = 403,
		NOT_FOUND = 404,
		CONFLICT = 409,
		INTERNAL_SERVER_ERROR = 500,
		NOT_IMPLEMENTED = 501,
		SERVICE_UNAVAILABLE = 503
}

export function fromECommonsHttpResponseCode(method: ECommonsHttpResponseCode): number {
	switch (method) {
		case ECommonsHttpResponseCode.OK:
			return ECommonsHttpResponseCode.OK;
		case ECommonsHttpResponseCode.CREATED:
			return ECommonsHttpResponseCode.CREATED;
		case ECommonsHttpResponseCode.NO_CONTENT:
			return ECommonsHttpResponseCode.NO_CONTENT;
		case ECommonsHttpResponseCode.BAD_REQUEST:
			return ECommonsHttpResponseCode.BAD_REQUEST;
		case ECommonsHttpResponseCode.UNAUTHORIZED:
			return ECommonsHttpResponseCode.UNAUTHORIZED;
		case ECommonsHttpResponseCode.FORBIDDEN:
			return ECommonsHttpResponseCode.FORBIDDEN;
		case ECommonsHttpResponseCode.NOT_FOUND:
			return ECommonsHttpResponseCode.NOT_FOUND;
		case ECommonsHttpResponseCode.CONFLICT:
			return ECommonsHttpResponseCode.CONFLICT;
		case ECommonsHttpResponseCode.INTERNAL_SERVER_ERROR:
			return ECommonsHttpResponseCode.INTERNAL_SERVER_ERROR;
		case ECommonsHttpResponseCode.NOT_IMPLEMENTED:
			return ECommonsHttpResponseCode.NOT_IMPLEMENTED;
		case ECommonsHttpResponseCode.SERVICE_UNAVAILABLE:
			return ECommonsHttpResponseCode.SERVICE_UNAVAILABLE;
	}
	
	throw new Error('Unknown ECommonsHttpResponseCode');
}

export function toECommonsHttpResponseCode(code: number): ECommonsHttpResponseCode|undefined {
	switch (code) {
		case ECommonsHttpResponseCode.OK:
			return ECommonsHttpResponseCode.OK;
		case ECommonsHttpResponseCode.CREATED:
			return ECommonsHttpResponseCode.CREATED;
		case ECommonsHttpResponseCode.NO_CONTENT:
			return ECommonsHttpResponseCode.NO_CONTENT;
		case ECommonsHttpResponseCode.BAD_REQUEST:
			return ECommonsHttpResponseCode.BAD_REQUEST;
		case ECommonsHttpResponseCode.UNAUTHORIZED:
			return ECommonsHttpResponseCode.UNAUTHORIZED;
		case ECommonsHttpResponseCode.FORBIDDEN:
			return ECommonsHttpResponseCode.FORBIDDEN;
		case ECommonsHttpResponseCode.NOT_FOUND:
			return ECommonsHttpResponseCode.NOT_FOUND;
		case ECommonsHttpResponseCode.CONFLICT:
			return ECommonsHttpResponseCode.CONFLICT;
		case ECommonsHttpResponseCode.INTERNAL_SERVER_ERROR:
			return ECommonsHttpResponseCode.INTERNAL_SERVER_ERROR;
		case ECommonsHttpResponseCode.NOT_IMPLEMENTED:
			return ECommonsHttpResponseCode.NOT_IMPLEMENTED;
		case ECommonsHttpResponseCode.SERVICE_UNAVAILABLE:
			return ECommonsHttpResponseCode.SERVICE_UNAVAILABLE;
	}
	
	return undefined;
}
