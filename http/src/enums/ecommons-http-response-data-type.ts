import { CommonsType } from 'tscommons-core';

export enum ECommonsHttpResponseDataType {
		STRING = 'string',
		UINT8ARRAY = 'uint8array'
}

export function toECommonsHttpResponseDataType(type: string): ECommonsHttpResponseDataType|undefined {
	switch (type) {
		case ECommonsHttpResponseDataType.STRING.toString():
			return ECommonsHttpResponseDataType.STRING;
		case ECommonsHttpResponseDataType.UINT8ARRAY.toString():
			return ECommonsHttpResponseDataType.UINT8ARRAY;
	}
	return undefined;
}

export function fromECommonsHttpResponseDataType(type: ECommonsHttpResponseDataType): string {
	switch (type) {
		case ECommonsHttpResponseDataType.STRING:
			return ECommonsHttpResponseDataType.STRING.toString();
		case ECommonsHttpResponseDataType.UINT8ARRAY:
			return ECommonsHttpResponseDataType.UINT8ARRAY.toString();
	}
	
	throw new Error('Unknown ECommonsHttpResponseDataType');
}

export function isECommonsHttpResponseDataType(test: unknown): test is ECommonsHttpResponseDataType {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsHttpResponseDataType(test) !== undefined;
}

export function keyToECommonsHttpResponseDataType(key: string): ECommonsHttpResponseDataType {
	switch (key) {
		case 'STRING':
			return ECommonsHttpResponseDataType.STRING;
		case 'UINT8ARRAY':
			return ECommonsHttpResponseDataType.UINT8ARRAY;
	}
	
	throw new Error(`Unable to obtain ECommonsHttpResponseDataType for key: ${key}`);
}

export const ECOMMONS_HTTP_RESPONSE_DATA_TYPES: ECommonsHttpResponseDataType[] = Object.keys(ECommonsHttpResponseDataType)
		.map((e: string): ECommonsHttpResponseDataType => keyToECommonsHttpResponseDataType(e));
