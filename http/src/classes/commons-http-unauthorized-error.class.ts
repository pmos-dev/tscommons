import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

import { CommonsHttpError } from './commons-http-error.class';

export class CommonsHttpUnauthorizedError extends CommonsHttpError {
	constructor(m: string) {
		super(m, ECommonsHttpResponseCode.UNAUTHORIZED);
		Object.setPrototypeOf(this, CommonsHttpUnauthorizedError.prototype);
	}
}
