import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

import { CommonsHttpError } from './commons-http-error.class';

export class CommonsHttpBadRequestError extends CommonsHttpError {
	constructor(m: string) {
		super(m, ECommonsHttpResponseCode.BAD_REQUEST);
		Object.setPrototypeOf(this, CommonsHttpBadRequestError.prototype);
	}
}
