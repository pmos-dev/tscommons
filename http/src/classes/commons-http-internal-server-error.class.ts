import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

import { CommonsHttpError } from './commons-http-error.class';

export class CommonsHttpInternalServerError extends CommonsHttpError {
	constructor(m: string) {
		super(m, ECommonsHttpResponseCode.INTERNAL_SERVER_ERROR);
		Object.setPrototypeOf(this, CommonsHttpInternalServerError.prototype);
	}
}
