import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

import { CommonsHttpSuccess } from './commons-http-success.class';

export class CommonsHttpCreatedSuccess extends CommonsHttpSuccess {
	constructor() {
		super(ECommonsHttpResponseCode.CREATED);
	}
}
