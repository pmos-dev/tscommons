import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

export class CommonsHttpSuccess {
	constructor(
			public readonly httpResponseCode: ECommonsHttpResponseCode|number
	) {}
}
