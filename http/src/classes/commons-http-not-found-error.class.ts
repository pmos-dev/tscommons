import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

import { CommonsHttpError } from './commons-http-error.class';

export class CommonsHttpNotFoundError extends CommonsHttpError {
	constructor(m: string) {
		super(m, ECommonsHttpResponseCode.NOT_FOUND);
		Object.setPrototypeOf(this, CommonsHttpNotFoundError.prototype);
	}
}
