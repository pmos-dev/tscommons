import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

import { CommonsHttpError } from './commons-http-error.class';

export class CommonsHttpServiceUnavailableError extends CommonsHttpError {
	constructor(m: string) {
		super(m, ECommonsHttpResponseCode.SERVICE_UNAVAILABLE);
		Object.setPrototypeOf(this, CommonsHttpServiceUnavailableError.prototype);
	}
}
