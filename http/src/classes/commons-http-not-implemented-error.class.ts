import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

import { CommonsHttpError } from './commons-http-error.class';

export class CommonsHttpNotImplementedError extends CommonsHttpError {
	constructor(m: string) {
		super(m, ECommonsHttpResponseCode.NOT_IMPLEMENTED);
		Object.setPrototypeOf(this, CommonsHttpNotImplementedError.prototype);
	}
}
