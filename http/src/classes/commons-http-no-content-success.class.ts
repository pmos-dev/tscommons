import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

import { CommonsHttpSuccess } from './commons-http-success.class';

export class CommonsHttpNoContentSuccess extends CommonsHttpSuccess {
	constructor() {
		super(ECommonsHttpResponseCode.NO_CONTENT);
	}
}
