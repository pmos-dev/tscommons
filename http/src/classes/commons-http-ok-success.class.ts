import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

import { CommonsHttpSuccess } from './commons-http-success.class';

export class CommonsHttpOkSuccess extends CommonsHttpSuccess {
	constructor() {
		super(ECommonsHttpResponseCode.OK);
	}
}
