import { ECommonsHttpResponseCode } from '../enums/ecommons-http-response-code';

import { CommonsHttpError } from './commons-http-error.class';

export class CommonsHttpForbiddenError extends CommonsHttpError {
	constructor(m: string) {
		super(m, ECommonsHttpResponseCode.FORBIDDEN);
		Object.setPrototypeOf(this, CommonsHttpForbiddenError.prototype);
	}
}
