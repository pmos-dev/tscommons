import { CommonsType } from 'tscommons-core';

import { ECommonsHttpContentType, isECommonsHttpContentType } from '../enums/ecommons-http-content-type';
import { ECommonsHttpResponseDataType, isECommonsHttpResponseDataType } from '../enums/ecommons-http-response-data-type';

export type TCommonsHttpRequestOptions = {
		timeout?: number;
		bodyDataEncoding?: ECommonsHttpContentType;
		responseDataType?: ECommonsHttpResponseDataType;
		maxReattempts?: number;
};

export type TCommonsHttpInternalRequestOptions = Omit<
		TCommonsHttpRequestOptions,
		'maxReattempts' | 'bodyDataEncoding' | 'responseDataType'
>;

export function isTCommonsHttpRequestOptions(test: unknown): test is TCommonsHttpRequestOptions {
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'timeout')) return false;
	if (!CommonsType.hasPropertyEnumOrUndefined<ECommonsHttpContentType>(test, 'bodyDataEncoding', isECommonsHttpContentType)) return false;
	if (!CommonsType.hasPropertyEnumOrUndefined<ECommonsHttpResponseDataType>(test, 'responseDataType', isECommonsHttpResponseDataType)) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'maxReattempts')) return false;
	
	return true;
}
