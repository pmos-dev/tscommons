import { TEncodedObject } from 'tscommons-core';
import { ICommonsHttpClientImplementation } from 'tscommons-http';
import { TCommonsHttpRequestOptions } from 'tscommons-http';
import { ECommonsHttpContentType } from 'tscommons-http';
import { ICommonsFirstClass } from 'tscommons-models';
import { ICommonsAdamantineManagedSecondClassUser } from 'tscommons-models-adamantine';

import { CommonsAdamantineManagedFirstClassUserModelSessionRestClientService } from './commons-adamantine-managed-first-class-user-model-session-rest-client.service';

export class CommonsAdamantineManagedSecondClassUserModelSessionRestClientService<
		M extends ICommonsAdamantineManagedSecondClassUser<P>,
		P extends ICommonsFirstClass
> extends CommonsAdamantineManagedFirstClassUserModelSessionRestClientService<M> {
	constructor(
			implementation: ICommonsHttpClientImplementation,
			rootUrl: string,
			authRestCall: string,
			private firstClassField: string,
			decodeM: (encoded: TEncodedObject) => M,
			isM: (test: unknown) => test is M,
			contentType: ECommonsHttpContentType = ECommonsHttpContentType.FORM_URL
	) {
		super(
				implementation,
				rootUrl,
				authRestCall,
				decodeM,
				isM,
				contentType
		);
	}
	
	public async logonWithFirstClassAndPw(
			username: string,
			pw: string,
			firstClass: P,
			data: TEncodedObject = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		data['pw'] = pw;
		data[this.firstClassField] = firstClass.id;
		
		return await this.logon(
				username,
				data,
				options
		);
	}
}
