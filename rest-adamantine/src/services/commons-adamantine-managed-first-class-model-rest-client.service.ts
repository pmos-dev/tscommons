import { CommonsType } from 'tscommons-core';
import { CommonsBase62 } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';
import { TCommonsHttpRequestOptions } from 'tscommons-http';
import { CommonsHttpNotFoundError } from 'tscommons-http';
import { ICommonsUniquelyIdentified } from 'tscommons-models';
import { ICommonsManaged } from 'tscommons-models-adamantine';
import { CommonsRestClientService } from 'tscommons-rest';

import { CommonsAdamantineManagedModelRestClientService } from './commons-adamantine-managed-model-rest-client.service';

export abstract class CommonsAdamantineManagedFirstClassModelRestClientService<
		M extends (
				ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)
		),
		T extends TEncodedObject
> extends CommonsAdamantineManagedModelRestClientService<M, T> {
	constructor(
			restClientService: CommonsRestClientService,
			path: string,
			protected isT: (test: unknown) => test is T,
			protected decodeM: (data: T) => M,
			protected isUidSupported: boolean = false
	) {
		super(restClientService, path);
	}

	public async listIds(
			options: TCommonsHttpRequestOptions = {}
	): Promise<number[]> {
		const ids: unknown = await this.restClientService.getRest<number[]>(
				`${this.path}data/ids`,
				undefined,
				undefined,
				options
		);
		if (!CommonsType.isNumberArray(ids)) throw new Error('Invalid ids returned');
		
		return ids;
	}
	
	public async listNames(
			options: TCommonsHttpRequestOptions = {}
	): Promise<string[]> {
		const names: unknown = await this.restClientService.getRest<number[]>(
				`${this.path}data/names`,
				undefined,
				undefined,
				options
		);
		if (!CommonsType.isStringArray(names)) throw new Error('Invalid names returned');
		
		return names;
	}
	
	public async listUids(
			options: TCommonsHttpRequestOptions = {}
	): Promise<string[]> {
		if (!this.isUidSupported) throw new Error('UID support is not enabled for this model');
		
		const uids: unknown = await this.restClientService.getRest<number[]>(
				`${this.path}data/uids`,
				undefined,
				undefined,
				options
		);
		if (!CommonsBase62.isIdArray(uids)) throw new Error('Invalid uids returned');
		
		return uids;
	}

	public async getById(
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<T>(
					`${this.path}data/ids/${id}`,
					undefined,
					undefined,
					options
			);
			if (!this.isT(encoded)) throw new Error('Invalid model item');
			
			return this.decodeM(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}

	public async getByName(
			name: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M|undefined> {
		try {
			const encoded: unknown = await this.restClientService.getRest<T>(
					`${this.path}data/names/${name}`,
					undefined,
					undefined,
					options
			);
			if (!this.isT(encoded)) throw new Error('Invalid model item');
			
			return this.decodeM(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}

	public async getByUid(
			uid: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M|undefined> {
		if (!this.isUidSupported) throw new Error('UID support is not enabled for this model');

		try {
			const encoded: unknown = await this.restClientService.getRest<T>(
					`${this.path}data/uids/${uid}`,
					undefined,
					undefined,
					options
			);
			if (!this.isT(encoded)) throw new Error('Invalid model item');
			
			return this.decodeM(encoded);
		} catch (e) {
			if (e instanceof CommonsHttpNotFoundError) {
				return undefined;
			}
			
			throw e;
		}
	}

	public async create(
			data: TPropertyObject,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.postRest<T, TEncodedObject>(
				`${this.path}data`,
				CommonsType.encodePropertyObject(data),
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Creation failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async updateForId(
			id: number,
			data: TPropertyObject,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.patchRest<T, TEncodedObject>(
				`${this.path}data/ids/${id}`,
				CommonsType.encodePropertyObject(data),
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Update failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async updateForName(
			name: string,
			data: TPropertyObject,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.patchRest<T, TEncodedObject>(
				`${this.path}data/names/${name}`,
				CommonsType.encodePropertyObject(data),
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Update failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async updateForUid(
			uid: string,
			data: TPropertyObject,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		if (!this.isUidSupported) throw new Error('UID support is not enabled for this model');

		const encoded: unknown = await this.restClientService.patchRest<T, TEncodedObject>(
				`${this.path}data/uids/${uid}`,
				CommonsType.encodePropertyObject(data),
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Update failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async deleteForId(
			id: number,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		const result: unknown = await this.restClientService.deleteRest<boolean>(
				`${this.path}data/ids/${id}`,
				undefined,
				undefined,
				options
		);
		if (!CommonsType.isBoolean(result)) throw new Error('Delete failure: non-boolean returned');
		
		return result;
	}

	public async deleteForName(
			name: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		const result: unknown = await this.restClientService.deleteRest<boolean>(
				`${this.path}data/names/${name}`,
				undefined,
				undefined,
				options
		);
		if (!CommonsType.isBoolean(result)) throw new Error('Delete failure: non-boolean returned');
		
		return result;
	}

	public async deleteForUid(
			uid: string,
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		if (!this.isUidSupported) throw new Error('UID support is not enabled for this model');

		const result: unknown = await this.restClientService.deleteRest<boolean>(
				`${this.path}data/uids/${uid}`,
				undefined,
				undefined,
				options
		);
		if (!CommonsType.isBoolean(result)) throw new Error('Delete failure: non-boolean returned');
		
		return result;
	}
	
}
