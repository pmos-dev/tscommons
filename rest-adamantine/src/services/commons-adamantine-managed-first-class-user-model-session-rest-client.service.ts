import { CommonsType } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';
import { ICommonsHttpClientImplementation } from 'tscommons-http';
import { TCommonsHttpRequestOptions } from 'tscommons-http';
import { ECommonsHttpContentType } from 'tscommons-http';
import { ICommonsAdamantineManagedFirstClassUser } from 'tscommons-models-adamantine';
import { CommonsUserPwSessionRestClientService } from 'tscommons-rest';

// first class isn't any different from the superclass, as we're not using a firstclass parent
export class CommonsAdamantineManagedFirstClassUserModelSessionRestClientService<
		M extends ICommonsAdamantineManagedFirstClassUser
> extends CommonsUserPwSessionRestClientService {
	constructor(
			implementation: ICommonsHttpClientImplementation,
			rootUrl: string,
			authRestCall: string,
			private decodeM: (encoded: TEncodedObject) => M,
			private isM: (test: unknown) => test is M,
			contentType: ECommonsHttpContentType = ECommonsHttpContentType.FORM_URL
	) {
		super(
				implementation,
				rootUrl,
				authRestCall,
				contentType
		);
	}
	
	public async getSessionUser(
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.getRest<TEncodedObject>(
				`${this.authRestCall}`,
				undefined,
				undefined,
				options
		);
		if (!CommonsType.isEncodedObject(encoded)) throw new Error('Invalid session user object returned');
		
		const user: M = this.decodeM(encoded);
		if (!this.isM(user)) throw new Error('Invalid session user could not be decoded');
		
		return user;
	}
}
