import { TEncodedObject } from 'tscommons-core';
import { TCommonsHttpRequestOptions } from 'tscommons-http';
import { ICommonsManaged } from 'tscommons-models-adamantine';
import { ICommonsUniquelyIdentified } from 'tscommons-models';
import { ICommonsManagedModelMetadata } from 'tscommons-models-adamantine';
import { TCommonsManagedModelMetadata, isTCommonsManagedModelMetadata, decodeICommonsManagedModelMetadata } from 'tscommons-models-adamantine';
import { CommonsRestClientService } from 'tscommons-rest';

export abstract class CommonsAdamantineManagedModelRestClientService<
		_M extends (
				ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)
		),
		_T extends TEncodedObject
> {
	constructor(
			protected restClientService: CommonsRestClientService,
			protected path: string
	) {}

	public async getMetadata(
			options: TCommonsHttpRequestOptions = {}
	): Promise<ICommonsManagedModelMetadata> {
		const metadata: unknown = await this.restClientService.getRest<TCommonsManagedModelMetadata>(
				`${this.path}metadata`,
				undefined,
				undefined,
				options
		);
		if (!isTCommonsManagedModelMetadata(metadata)) throw new Error('Invalid metadata returned');
		
		return decodeICommonsManagedModelMetadata(metadata);
	}
	
}
