import { TEncodedObject } from 'tscommons-core';
import { TCommonsHttpRequestOptions } from 'tscommons-http';
import { ICommonsUniquelyIdentified } from 'tscommons-models';
import { ICommonsOrdered } from 'tscommons-models';
import { ECommonsMoveDirection } from 'tscommons-models';
import { ICommonsManaged } from 'tscommons-models-adamantine';
import { CommonsRestClientService } from 'tscommons-rest';

import { CommonsAdamantineManagedFirstClassModelRestClientService } from './commons-adamantine-managed-first-class-model-rest-client.service';

export abstract class CommonsAdamantineManagedOrderedModelRestClientService<
		M extends (
				(ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)) & ICommonsOrdered
		),
		T extends TEncodedObject
> extends CommonsAdamantineManagedFirstClassModelRestClientService<M, T> {
	constructor(
			restClientService: CommonsRestClientService,
			path: string,
			isT: (test: unknown) => test is T,
			decodeM: (data: T) => M,
			isUidSupported: boolean = false
	) {
		super(
				restClientService,
				path,
				isT,
				decodeM,
				isUidSupported
		);
	}

	public async moveForIdAndDirection(
			id: number,
			direction: ECommonsMoveDirection,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.patchRest<
				T,
				{
						direction: ECommonsMoveDirection;
				}
		>(
				`${this.path}data/ids/${id}/move`,
				{
						direction: direction
				},
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Move failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async moveForNameAndDirection(
			name: string,
			direction: ECommonsMoveDirection,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.patchRest<
				T,
				{
						direction: ECommonsMoveDirection;
				}
		>(
				`${this.path}data/names/${name}/move`,
				{
						direction: direction
				},
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Move failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async moveForUidAndDirection(
			uid: string,
			direction: ECommonsMoveDirection,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		if (!this.isUidSupported) throw new Error('UID support is not enabled for this model');

		const encoded: unknown = await this.restClientService.patchRest<
				T,
				{
						direction: ECommonsMoveDirection;
				}
		>(
				`${this.path}data/uids/${uid}/move`,
				{
						direction: direction
				},
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Move failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

}
