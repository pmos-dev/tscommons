import { TEncodedObject } from 'tscommons-core';
import { TCommonsHttpRequestOptions } from 'tscommons-http';
import { ICommonsFirstClass } from 'tscommons-models';
import { ICommonsUniquelyIdentified } from 'tscommons-models';
import { ICommonsOrientatedOrdered } from 'tscommons-models';
import { ECommonsMoveDirection } from 'tscommons-models';
import { ICommonsManaged } from 'tscommons-models-adamantine';
import { CommonsRestClientService } from 'tscommons-rest';

import { CommonsAdamantineManagedSecondClassModelRestClientService } from './commons-adamantine-managed-second-class-model-rest-client.service';

export abstract class CommonsAdamantineManagedOrientatedOrderedModelRestClientService<
		M extends (
				(ICommonsManaged | (ICommonsManaged & ICommonsUniquelyIdentified)) & ICommonsOrientatedOrdered<P>
		),
		P extends ICommonsFirstClass,
		T extends TEncodedObject
> extends CommonsAdamantineManagedSecondClassModelRestClientService<M, P, T> {
	constructor(
			restClientService: CommonsRestClientService,
			path: string,
			isT: (test: unknown) => test is T,
			decodeM: (data: T) => M,
			isUidSupported: boolean = false
	) {
		super(
				restClientService,
				path,
				isT,
				decodeM,
				isUidSupported
		);
	}

	public async moveForFirstClassAndIdAndDirection(
			firstClass: P,
			id: number,
			direction: ECommonsMoveDirection,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.patchRest<
				T,
				{
						direction: ECommonsMoveDirection;
				}
		>(
				`${this.path}data/${firstClass.id}/ids/${id}/move`,
				{
						direction: direction
				},
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Update failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async moveForFirstClassAndNameAndDirection(
			firstClass: P,
			name: string,
			direction: ECommonsMoveDirection,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		const encoded: unknown = await this.restClientService.patchRest<
				T,
				{
						direction: ECommonsMoveDirection;
				}
		>(
				`${this.path}data/${firstClass.id}/names/${name}/move`,
				{
						direction: direction
				},
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Update failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}

	public async moveForUidAndDirection(
			uid: string,
			direction: ECommonsMoveDirection,
			options: TCommonsHttpRequestOptions = {}
	): Promise<M> {
		if (!this.isUidSupported) throw new Error('UID support is not enabled for this model');

		const encoded: unknown = await this.restClientService.patchRest<
				T,
				{
						direction: ECommonsMoveDirection;
				}
		>(
				`${this.path}data/uids/${uid}/move`,
				{
						direction: direction
				},
				undefined,
				undefined,
				options
		);
		if (!this.isT(encoded)) throw new Error('Update failure: invalid model item returned');
		
		return this.decodeM(encoded);
	}
}
