import { CommonsType } from 'tscommons-core';

export interface ICommonsGeographic {
		latitude: number;
		longitude: number;
		radius?: number;
}

export function isICommonsGeographic(test: unknown): test is ICommonsGeographic {
	if (!CommonsType.isObject(test)) return false;
	
	const attempt: ICommonsGeographic = test as ICommonsGeographic;
	
	if (!CommonsType.hasPropertyNumber(attempt, 'latitude')) return false;
	if (!CommonsType.hasPropertyNumber(attempt, 'longitude')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(attempt, 'radius')) return false;

	return true;
}
