import { CommonsType } from 'tscommons-core';

export interface ICommonsXy {
		x: number;
		y: number;
		radius?: number;
}

export function isICommonsXy(test: unknown): test is ICommonsXy {
	if (!CommonsType.isObject(test)) return false;
	
	const attempt: ICommonsXy = test as ICommonsXy;
	
	if (!CommonsType.hasPropertyNumber(attempt, 'x')) return false;
	if (!CommonsType.hasPropertyNumber(attempt, 'y')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(attempt, 'radius')) return false;

	return true;
}
