import { CommonsType } from 'tscommons-core';

import { ICommonsGeographic } from '../interfaces/icommons-geographic';
import { ICommonsXy } from '../interfaces/icommons-xy';

const MAX_ITERATIONS: number = 20;

export abstract class CommonsGeographics {
	public static computeDistanceAndBearing(geo1: ICommonsGeographic, geo2: ICommonsGeographic): number {
		
		// Ported from android.location Java source code, released under Apache License.
		// Based on http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf
		// using the "Inverse Formula" (section 4)
		// Convert lat/long to radians
	
		// prevent changes modifying the original object
		const lat1: number = geo1.latitude * Math.PI / 180.0;
		const lon1: number = geo1.longitude * Math.PI / 180.0;
		const lat2: number = geo2.latitude * Math.PI / 180.0;
		const lon2: number = geo2.longitude * Math.PI / 180.0;

		const a: number = 6378137.0; // WGS84 major axis
		const b: number = 6356752.3142; // WGS84 semi-major axis
		const f: number = (a - b) / a;
		const aSqMinusBSqOverBSq: number = (a * a - b * b) / (b * b);
		const L: number = lon2 - lon1;
		let A: number = 0.0;
		const U1: number = Math.atan((1.0 - f) * Math.tan(lat1));
		const U2: number = Math.atan((1.0 - f) * Math.tan(lat2));
		const cosU1: number = Math.cos(U1);
		const cosU2: number = Math.cos(U2);
		const sinU1: number = Math.sin(U1);
		const sinU2: number = Math.sin(U2);
		const cosU1cosU2: number = cosU1 * cosU2;
		const sinU1sinU2: number = sinU1 * sinU2;
		let sigma: number = 0.0;
		let deltaSigma: number = 0.0;
		let cosSqAlpha: number = 0.0;
		let cos2SM: number = 0.0;
		let cosSigma: number = 0.0;
		let sinSigma: number = 0.0;
		let cosLambda: number = 0.0;
		let sinLambda: number = 0.0;
		let lambda: number = L; // initial guess
		for (let iter = 0; iter < MAX_ITERATIONS; iter++) {
			const lambdaOrig: number = lambda;
			cosLambda = Math.cos(lambda);
			sinLambda = Math.sin(lambda);
			const t1: number = cosU2 * sinLambda;
			const t2: number = cosU1 * sinU2 - sinU1 * cosU2 * cosLambda;
			const sinSqSigma: number = t1 * t1 + t2 * t2; // (14)
			sinSigma = Math.sqrt(sinSqSigma);
			cosSigma = sinU1sinU2 + cosU1cosU2 * cosLambda; // (15)
			sigma = Math.atan2(sinSigma, cosSigma); // (16)
			const sinAlpha: number = (sinSigma === 0) ? 0.0 : cosU1cosU2 * sinLambda / sinSigma; // (17)
			cosSqAlpha = 1.0 - sinAlpha * sinAlpha;
			cos2SM = (cosSqAlpha === 0) ? 0.0 : cosSigma - 2.0 * sinU1sinU2 / cosSqAlpha; // (18)
			const uSquared: number = cosSqAlpha * aSqMinusBSqOverBSq; // defn
			A = 1 + (uSquared / 16384.0) * (4096.0 + uSquared * (-768 + uSquared * (320.0 - 175.0 * uSquared)));
			const B: number = (uSquared / 1024.0) * (256.0 + uSquared * (-128.0 + uSquared * (74.0 - 47.0 * uSquared)));
			const C: number = (f / 16.0) * cosSqAlpha * (4.0 + f * (4.0 - 3.0 * cosSqAlpha)); // (10)
			const cos2SMSq: number = cos2SM * cos2SM;
			deltaSigma = B * sinSigma * (cos2SM + (B / 4.0) * (cosSigma * (-1.0 + 2.0 * cos2SMSq) - (B / 6.0) * cos2SM * (-3.0 + 4.0 * sinSigma * sinSigma) * (-3.0 + 4.0 * cos2SMSq)));
			lambda = L + (1.0 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SM + C * cosSigma * (-1.0 + 2.0 * cos2SM * cos2SM))); // (11)
			const delta: number = (lambda - lambdaOrig) / lambda;
			if (Math.abs(delta) < 1.0e-12) {
				break;
			}
		}
	
		return b * A * (sigma - deltaSigma);
	}
	
	public static getEarthCircumferenceInMeters(): number {
		return 40075016.68;
	}
	
	public static geographicToXy(geographic: ICommonsGeographic): ICommonsXy {
		const x: number = (geographic.longitude / 180) * (CommonsGeographics.getEarthCircumferenceInMeters() / 2);
		let y: number = Math.log(Math.tan((90 + geographic.latitude) * Math.PI / 360)) / (Math.PI / 180);
		y = (y / 180) * (CommonsGeographics.getEarthCircumferenceInMeters() / 2);
		
		return { x: x, y: y };
	}
	
	public static geographicToXyPercentage(geographic: ICommonsGeographic): ICommonsXy {
		let x: number = geographic.longitude / 180;
		let y: number = Math.log(Math.tan((90 + geographic.latitude) * Math.PI / 360)) / (Math.PI / 180);
		y = y / 180;

		x += 1; x /= 2;
		y += 1; y /= 2; y = 1 - y;
		
		return { x: x, y: y };
	}
	
	/*public static distance(a: ICommonsGeographic, b: ICommonsGeographic): number {
		const xya: ICommonsXy = CommonsGeographics.geographicToXy(a);
		const xyb: ICommonsXy = CommonsGeographics.geographicToXy(b);
		
		const dx: number = xya.x - xyb.x;
		const dy: number = xya.y - xyb.y;

		const pyth: number = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
		
		return pyth;
	}*/
	
	public static isOverlap(a: ICommonsGeographic, b: ICommonsGeographic): boolean {
		if (!CommonsType.hasPropertyNumber(a, 'radius')) throw new Error('No A radius is available for overlap comparisons');
		if (!CommonsType.hasPropertyNumber(b, 'radius')) throw new Error('No B radius is available for overlap comparisons');
		
		const pyth: number = CommonsGeographics.computeDistanceAndBearing(a, b);
		
		return pyth <= (a.radius! + b.radius!);
	}
	
	public static isAnyOverlap(a: ICommonsGeographic, geos: ICommonsGeographic[]) {
		for (const b of geos) {
			if (CommonsGeographics.isOverlap(a, b)) return true;
		}
		
		return false;
	}
}
