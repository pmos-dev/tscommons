import { TEncodedObject } from 'tscommons-core';
import { ICommonsHttpClientImplementation } from 'tscommons-http';
import { TCommonsHttpRequestOptions } from 'tscommons-http';
import { ECommonsHttpContentType } from 'tscommons-http';

import { CommonsUserSessionRestClientService } from './commons-user-session-rest-client.service';

export class CommonsUserPwSessionRestClientService extends CommonsUserSessionRestClientService {
	constructor(
			implementation: ICommonsHttpClientImplementation,
			rootUrl: string,
			authRestCall: string,
			contentType: ECommonsHttpContentType = ECommonsHttpContentType.FORM_URL
	) {
		super(implementation, rootUrl, authRestCall, contentType);
	}
	
	public async logonWithPw(
			username: string,
			pw: string,
			data: TEncodedObject = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		data['pw'] = pw;
		
		return await this.logon(
				username,
				data,
				options
		);
	}
}
