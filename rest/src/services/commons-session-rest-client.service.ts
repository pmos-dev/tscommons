import { ICommonsHttpClientImplementation } from 'tscommons-http';
import { TCommonsHttpRequestOptions } from 'tscommons-http';
import { ECommonsHttpContentType } from 'tscommons-http';

import { ECommonsAuthorizationMethod } from '../enums/ecommons-authorization-method';

import { CommonsAuthorizedRestClientService } from './commons-authorized-rest-client.service';

export class CommonsSessionRestClientService extends CommonsAuthorizedRestClientService {
	private activeSession: string|undefined;
	private debug: boolean = false;

	constructor(
			implementation: ICommonsHttpClientImplementation,
			rootUrl: string,
			protected authRestCall: string,
			contentType: ECommonsHttpContentType = ECommonsHttpContentType.FORM_URL
	) {
		super(implementation, rootUrl, contentType);
	}
	
	public setDebug(state: boolean): void {
		this.debug = state;
	}
	
	public getSession(): string|undefined {
		return this.activeSession;
	}
	
	public setSession(
			sid: string|undefined
	): void {
		this.activeSession = sid;
		
		if (sid) {
			this.setAuthorization(
					ECommonsAuthorizationMethod.SESSION,
					this.activeSession
			);
		} else {
			this.setAuthorization(undefined, undefined);
		}
	}
	
	public async validate(
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		if (!this.activeSession) return false;
		
		try {
			await this.headRest(
					this.authRestCall,
					undefined,
					undefined,
					options
			);
			
			return true;
		} catch (e) {
			// @ts-ignore
			if (this.debug && console) console.log(e);
			
			this.setSession(undefined);
			
			return false;
		}
	}
	
	public async logoff(
			options: TCommonsHttpRequestOptions = {}
	): Promise<void> {
		await this.deleteRest(
				this.authRestCall,
					undefined,
					undefined,
					options
		);

		this.setSession(undefined);
	}
}
