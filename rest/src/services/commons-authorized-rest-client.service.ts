import { ICommonsHttpClientImplementation } from 'tscommons-http';
import { TEncoded, TEncodedObject } from 'tscommons-core';
import { TCommonsHttpRequestOptions } from 'tscommons-http';
import { ECommonsHttpContentType } from 'tscommons-http';

import { ECommonsAuthorizationMethod, fromECommonsAuthorizationMethod } from '../enums/ecommons-authorization-method';

import { CommonsRestClientService } from './commons-rest-client.service';

export class CommonsAuthorizedRestClientService extends CommonsRestClientService {
	private authorizationMethod: ECommonsAuthorizationMethod|undefined;
	private authorizationValue: string|undefined;

	constructor(
			implementation: ICommonsHttpClientImplementation,
			rootUrl: string,
			contentType: ECommonsHttpContentType = ECommonsHttpContentType.FORM_URL
	) {
		super(implementation, rootUrl, contentType);
	}
	
	public setAuthorization(
			method: ECommonsAuthorizationMethod|undefined,
			value: string|undefined
	): void {
		this.authorizationMethod = method;
		this.authorizationValue = value;
	}
	
	public hasAuthorization(): boolean {
		return this.authorizationMethod !== undefined && this.authorizationValue !== undefined;
	}

	protected patchHeaders<H extends TEncodedObject>(headers?: H): H | (H & {
			'Authorization': string;
	}) {
		if (!this.authorizationMethod || !this.authorizationValue) return { ...headers } as H;
		
		return {
				...headers,
				Authorization: `${fromECommonsAuthorizationMethod(this.authorizationMethod)} ${this.authorizationValue}`
		} as H & {
				'Authorization': string;
		};
	}

	public async headRest<
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<void> {
		return await super.headRest<P, H | (H & {
				'Authorization': string;
		})>(
				script,
				params,
				this.patchHeaders(headers),
				options
		);
	}
	
	public getRest<
			T extends TEncoded = TEncoded,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<T> {
		return super.getRest<T, P, H | (H & {
				'Authorization': string;
		})>(
				script,
				params,
				this.patchHeaders(headers),
				options
		);
	}
	
	public postRest<
			T extends TEncoded = TEncoded,
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			body: B,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<T> {
		return super.postRest<T, B, P, H | (H & {
				'Authorization': string;
		})>(
				script,
				body,
				params,
				this.patchHeaders(headers),
				options
		);
	}
	
	public putRest<
			T extends TEncoded = TEncoded,
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			body: B,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<T> {
		return super.putRest<T, B, P, H | (H & {
				'Authorization': string;
		})>(
				script,
				body,
				params,
				this.patchHeaders(headers),
				options
		);
	}
	
	public patchRest<
			T extends TEncoded = TEncoded,
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			body: B,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<T|undefined> {
		return super.patchRest<T, B, P, H | (H & {
				'Authorization': string;
		})>(
				script,
				body,
				params,
				this.patchHeaders(headers),
				options
		);
	}
	
	public deleteRest<
			T extends TEncoded = TEncoded,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<T|undefined> {
		return super.deleteRest<T, P, H | (H & {
				'Authorization': string;
		})>(
				script,
				params,
				this.patchHeaders(headers),
				options
		);
	}
}
