import { CommonsType } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';
import { ICommonsHttpClientImplementation } from 'tscommons-http';
import { TCommonsHttpRequestOptions } from 'tscommons-http';
import { ECommonsHttpContentType } from 'tscommons-http';

import { CommonsSessionRestClientService } from './commons-session-rest-client.service';

export class CommonsUserSessionRestClientService extends CommonsSessionRestClientService {
	constructor(
			implementation: ICommonsHttpClientImplementation,
			rootUrl: string,
			authRestCall: string,
			contentType: ECommonsHttpContentType = ECommonsHttpContentType.FORM_URL
	) {
		super(implementation, rootUrl, authRestCall, contentType);
	}
	
	public async logon(
			username: string,
			data: TEncodedObject = {},
			options: TCommonsHttpRequestOptions = {}
	): Promise<boolean> {
		this.setSession(undefined);
		
		const result: unknown = await this.postRest<
				{ sid: string }
		>(
				`${this.authRestCall}/${username}`,
				data,
				undefined,
				undefined,
				options
		);
		if (!CommonsType.hasPropertyString(result, 'sid')) return false;
		
		this.setSession(result['sid']);
		
		return true;
	}
}
