import { TEncoded, TEncodedObject } from 'tscommons-core';
import { CommonsHttpClientService } from 'tscommons-http';
import { ICommonsHttpClientImplementation } from 'tscommons-http';
import { TCommonsHttpRequestOptions } from 'tscommons-http';
import { ECommonsHttpContentType } from 'tscommons-http';
import { ECommonsHttpResponseDataType } from 'tscommons-http';

export class CommonsRestClientService extends CommonsHttpClientService {
	constructor(
			implementation: ICommonsHttpClientImplementation,
			private rootUrl: string,
			private contentType: ECommonsHttpContentType = ECommonsHttpContentType.FORM_URL
	) {
		super(implementation);
	}

	public headRest<
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<void> {
		return super.head<P, H>(
				`${this.rootUrl}${script}`,
				params,
				headers,
				options
		);
	}
	
	public async getRest<
			T extends TEncoded = TEncoded,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<T> {
		const result: string = (await this.get<P, H>(
				`${this.rootUrl}${script}`,
				params,
				headers,
				{
						...options,
						responseDataType: ECommonsHttpResponseDataType.STRING
				}
		)) as string;
		
		try {
			return JSON.parse(result);
		} catch (e) {
			throw new Error(`Unable to decode JSON response from rest: ${result.substr(0, 255)}`);
		}
	}
	
	public async postRest<
			T extends TEncoded = TEncoded,
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			body: B,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<T> {
		const result: string = (await this.post<B, P, H>(
				`${this.rootUrl}${script}`,
				body,
				params,
				headers,
				{
						...options,
						bodyDataEncoding: options.bodyDataEncoding || this.contentType,
						responseDataType: ECommonsHttpResponseDataType.STRING
				}
		)) as string;
		
		try {
			return JSON.parse(result);
		} catch (e) {
			throw new Error(`Unable to decode JSON response from rest: ${result.substr(0, 255)}`);
		}
	}
	
	public async putRest<
			T extends TEncoded = TEncoded,
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			body: B,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<T> {
		const result: string = (await this.put<B, P, H>(
				`${this.rootUrl}${script}`,
				body,
				params,
				headers,
				{
						...options,
						bodyDataEncoding: options.bodyDataEncoding || this.contentType,
						responseDataType: ECommonsHttpResponseDataType.STRING
				}
		)) as string;
		
		try {
			return JSON.parse(result);
		} catch (e) {
			throw new Error(`Unable to decode JSON response from rest: ${result.substr(0, 255)}`);
		}
	}
	
	public async patchRest<
			T extends TEncoded = TEncoded,
			B extends TEncodedObject = TEncodedObject,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			body: B,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<T|undefined> {
		const result: string = (await this.patch<B, P, H>(
				`${this.rootUrl}${script}`,
				body,
				params,
				headers,
				{
						...options,
						bodyDataEncoding: options.bodyDataEncoding || this.contentType,
						responseDataType: ECommonsHttpResponseDataType.STRING
				}
		)) as string;

		if (result.length === 0) return undefined;
		
		try {
			return JSON.parse(result);
		} catch (e) {
			throw new Error(`Unable to decode JSON response from rest: ${result.substr(0, 255)}`);
		}
	}
	
	public async deleteRest<
			T extends TEncoded = TEncoded,
			P extends TEncodedObject = TEncodedObject,
			H extends TEncodedObject = TEncodedObject
	>(
			script: string,
			params?: P|undefined,	// the params can be explicitly undefined (none) as well as optional
			headers?: H|undefined,	// the headers can be explicitly undefined (none) as well as optional
			options: TCommonsHttpRequestOptions = {}
	): Promise<T|undefined> {
		const result: string|Uint8Array = (await this.delete<P, H>(
				`${this.rootUrl}${script}`,
				params,
				headers,
				{
						...options,
						responseDataType: ECommonsHttpResponseDataType.STRING
				}
		)) as string;
		
		if (result.length === 0) return undefined;

		try {
			return JSON.parse(result);
		} catch (e) {
			throw new Error(`Unable to decode JSON response from rest: ${result.substr(0, 255)}`);
		}
	}
}
