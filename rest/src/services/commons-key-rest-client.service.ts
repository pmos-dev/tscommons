import { ICommonsHttpClientImplementation } from 'tscommons-http';
import { ECommonsHttpContentType } from 'tscommons-http';

import { ECommonsAuthorizationMethod } from '../enums/ecommons-authorization-method';

import { CommonsAuthorizedRestClientService } from './commons-authorized-rest-client.service';

export class CommonsKeyRestClientService extends CommonsAuthorizedRestClientService {
	constructor(
			implementation: ICommonsHttpClientImplementation,
			rootUrl: string,
			authKey: string,
			contentType: ECommonsHttpContentType = ECommonsHttpContentType.FORM_URL
	) {
		super(implementation, rootUrl, contentType);
		
		this.setAuthorization(
				ECommonsAuthorizationMethod.KEY,
				authKey
		);
	}
}
