import { CommonsType } from 'tscommons-core';

export enum ECommonsAuthorizationMethod {
		KEY = 'key',
		SESSION = 'session'
}

export function toECommonsAuthorizationMethod(type: string): ECommonsAuthorizationMethod|undefined {
	switch (type) {
		case ECommonsAuthorizationMethod.KEY.toString():
			return ECommonsAuthorizationMethod.KEY;
		case ECommonsAuthorizationMethod.SESSION.toString():
			return ECommonsAuthorizationMethod.SESSION;
	}
	return undefined;
}

export function fromECommonsAuthorizationMethod(type: ECommonsAuthorizationMethod): string {
	switch (type) {
		case ECommonsAuthorizationMethod.KEY:
			return ECommonsAuthorizationMethod.KEY.toString();
		case ECommonsAuthorizationMethod.SESSION:
			return ECommonsAuthorizationMethod.SESSION.toString();
	}
	
	throw new Error('Unknown ECommonsAuthorizationMethod');
}

export function isECommonsAuthorizationMethod(test: unknown): test is ECommonsAuthorizationMethod {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsAuthorizationMethod(test) !== undefined;
}

export function keyToECommonsAuthorizationMethod(key: string): ECommonsAuthorizationMethod {
	switch (key) {
		case 'KEY':
			return ECommonsAuthorizationMethod.KEY;
		case 'SESSION':
			return ECommonsAuthorizationMethod.SESSION;
	}
	
	throw new Error(`Unable to obtain ECommonsAuthorizationMethod for key: ${key}`);
}

export const ECOMMONS_AUTHORIZATION_METHODS: ECommonsAuthorizationMethod[] = Object.keys(ECommonsAuthorizationMethod)
		.map((e: string): ECommonsAuthorizationMethod => keyToECommonsAuthorizationMethod(e));
