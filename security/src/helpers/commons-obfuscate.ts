import { CommonsEncoding } from 'tscommons-core';

export abstract class CommonsObfuscate {
	private static generatePad(source: string, length: number): string {
		return source.repeat(Math.ceil(length / source.length)).substr(0, length);
	}

	private static hexXor(hex: string, pad: string): string {
		return hex.split('')
				// tslint:disable-next-line:no-bitwise
				.map((_, i: number) => ((parseInt(hex.charAt(i), 16)) ^ parseInt(pad.charAt(i), 16)).toString(16))
				.join('');
	}
	
	public obfuscateStringXor(value: string, key: string): string {
		const hex: string = CommonsEncoding.unicodeToHex(value);
		const pad: string = CommonsObfuscate.generatePad(key, hex.length);
		const hexor: string = CommonsObfuscate.hexXor(hex, pad);
		
		const test: string = CommonsEncoding.unicodeToHex(CommonsObfuscate.hexXor(hexor, pad));
		if (test !== value) throw new Error('Test failed on resulting obfuscated string');

		return hexor;
	}

	public deobfuscateStringXor(obfuscated: string, key: string): string {
		const pad: string = CommonsObfuscate.generatePad(key, obfuscated.length);

		const undo: string = CommonsObfuscate.hexXor(obfuscated, pad);
		const unicode: string = CommonsEncoding.hexToUnicode(undo);
		
		const test: string = CommonsObfuscate.hexXor(CommonsEncoding.unicodeToHex(unicode), pad);
		if (test !== obfuscated) throw new Error('Test failed on resulting deobfuscated string');

		return unicode;
	}
}
