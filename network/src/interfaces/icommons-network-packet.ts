import { CommonsBase62 } from 'tscommons-core';
import { CommonsType } from 'tscommons-core';
import { CommonsDate } from 'tscommons-core';

const REGEX_PATTERN_NS = '^[A-Za-z](-?[0-9A-Za-z]+)*$';
const REGEX_PATTERN_COMMAND = '^[A-Za-z](-?[0-9A-Za-z]+)*$';

export enum ETransmissionMethod {
	BROADCAST = 'broadcast',
	DIRECT = 'direct',
	MULTICAST = 'multicast'
}

export type TChannel = string;
export type TDeviceId = string;

export interface ICommonsNetworkPacket {
	id: string;
	method: ETransmissionMethod;
	channel: TChannel;
	destination?: TDeviceId;
	destinations?: TDeviceId[];
	source: TDeviceId;
	ignoreOwn?: boolean;
	timestamp: Date;
	expiry?: Date;
	ns: string;
	command: string;
	data?: any;
	replay?: boolean;
}

export function isICommonsNetworkPacket(test: any, dateAsString: boolean = false): test is ICommonsNetworkPacket {
	if (!CommonsType.isObject(test)) return false;

	const attempt: ICommonsNetworkPacket = test as ICommonsNetworkPacket;
	
	if (!CommonsType.hasPropertyString(attempt, 'id') || !CommonsBase62.isId(attempt.id)) return false;
	if (!CommonsType.hasPropertyString(attempt, 'method') || !Object.values(ETransmissionMethod).includes(attempt.method)) return false;
	if (!CommonsType.hasPropertyString(attempt, 'channel') || !CommonsBase62.isId(attempt.channel)) return false;
	if (!CommonsType.hasPropertyString(attempt, 'source') || !CommonsBase62.isId(attempt.source)) return false;
	if (!CommonsType.hasProperty(attempt, 'timestamp') || (!dateAsString && !CommonsType.isDate(attempt.timestamp))) return false;
	if (!CommonsType.hasPropertyString(attempt, 'ns')) return false;

	if (CommonsType.hasProperty(attempt, 'destination') && (typeof attempt.destination !== 'string' || !CommonsBase62.isId(attempt.destination))) return false;
	if (CommonsType.hasProperty(attempt, 'destinations')) {
		if (!Array.isArray(attempt.destinations)) return false;
		for (const destination of attempt.destinations) {
			if (!CommonsBase62.isId(destination)) return false;
		}
	}

	if (CommonsType.hasProperty(attempt, 'expiry') && !dateAsString && !CommonsType.isDate(attempt.expiry)) return false;

	if (attempt.method === ETransmissionMethod.BROADCAST && (CommonsType.hasProperty(attempt, 'destination') || CommonsType.hasProperty(attempt, 'destinations'))) return false;
	if (attempt.method === ETransmissionMethod.DIRECT && (!CommonsType.hasProperty(attempt, 'destination') || CommonsType.hasProperty(attempt, 'destinations'))) return false;
	if (attempt.method === ETransmissionMethod.MULTICAST && (!CommonsType.hasProperty(attempt, 'destinations') || CommonsType.hasProperty(attempt, 'destination'))) return false;
	
	const regexNs: RegExp = new RegExp(REGEX_PATTERN_NS);
	if (!regexNs.test(attempt.ns)) return false;
	
	const regexCommand: RegExp = new RegExp(REGEX_PATTERN_COMMAND);
	if (!regexCommand.test(attempt.command)) return false;

	if (!CommonsType.hasPropertyBooleanOrUndefined(attempt, 'ignoreOwn')) return false;
	if (!CommonsType.hasPropertyBooleanOrUndefined(attempt, 'replay')) return false;
	
	return true;
}

export interface ICommonsNetworkPacketCompressed {
	i: string;
	m: string;
	c: TChannel;
	d?: string;
	s: TDeviceId;
	o?: number;
	t: string;
	e?: string;
	n: string;
	x: string;
	z?: any;
	r?: number;
}

export function isICommonsNetworkPacketCompressed(test: any): test is ICommonsNetworkPacketCompressed {
	if (!CommonsType.isObject(test)) return false;

	const attempt: ICommonsNetworkPacketCompressed = test as ICommonsNetworkPacketCompressed;
	
	if (!CommonsType.hasPropertyString(attempt, 'i') || !CommonsBase62.isId(attempt.i)) return false;
	if (!CommonsType.hasPropertyString(attempt, 'm') || ![ 'b', 'd', 'm' ].includes(attempt.m)) return false;
	if (!CommonsType.hasPropertyString(attempt, 'c') || !CommonsBase62.isId(attempt.c)) return false;
	if (!CommonsType.hasPropertyString(attempt, 's') || !CommonsBase62.isId(attempt.s)) return false;
	if (!CommonsType.hasPropertyString(attempt, 't')) return false;
	if (!CommonsType.hasPropertyString(attempt, 'n')) return false;

	switch (attempt.m) {
		case 'b':
			break;
		case 'd':
			if (!CommonsType.hasPropertyString(attempt, 'd')) return false;
			if (!CommonsBase62.isId(attempt.d!)) return false;
			break;
		case 'm':
			if (!CommonsType.hasPropertyString(attempt, 'd')) return false;
			const destinations: string[] = attempt.d!.split(',');
			for (const destination of destinations) {
				if (!CommonsBase62.isId(destination)) return false;
			}
			break;
	}

	if (!CommonsType.hasPropertyStringOrUndefined(attempt, 'e')) return false;

	const regexNs: RegExp = new RegExp(REGEX_PATTERN_NS);
	if (!regexNs.test(attempt.n)) return false;
	
	const regexCommand: RegExp = new RegExp(REGEX_PATTERN_COMMAND);
	if (!regexCommand.test(attempt.x)) return false;

	if (!CommonsType.hasPropertyNumberOrUndefined(attempt, 'r')) return false;
	
	return true;
}

export function compressPacket(packet: ICommonsNetworkPacket): ICommonsNetworkPacketCompressed {
	const compressed: ICommonsNetworkPacketCompressed = {
			i: packet.id,
			m: '',
			c: packet.channel,
			s: packet.source,
			t: CommonsDate.dateToCompressed(packet.timestamp),
			n: packet.ns,
			x: packet.command
	};
	
	switch (packet.method) {
		case ETransmissionMethod.BROADCAST:
			compressed.m = 'b';
			break;
		case ETransmissionMethod.DIRECT:
			compressed.m = 'd';
			compressed.d = packet.destination;
			break;
		case ETransmissionMethod.MULTICAST:
			compressed.m = 'm';
			compressed.d = packet.destinations!.join(',');
			break;
	}
	
	if (packet.ignoreOwn) compressed.o = 1;
	if (packet.expiry) compressed.e = CommonsDate.dateToCompressed(packet.expiry);
	
	if (packet.data !== undefined) compressed.z = packet.data;
	
	if (packet.replay !== undefined) compressed.r = packet.replay ? 1 : 0;
	
	return compressed;
}

export function decompressPacket(compressed: ICommonsNetworkPacketCompressed): ICommonsNetworkPacket {
	const packet: ICommonsNetworkPacket = {
			id: compressed.i,
			method: ETransmissionMethod.BROADCAST,
			channel: compressed.c,
			source: compressed.s,
			timestamp: CommonsDate.compressedToDate(compressed.t),
			ns: compressed.n,
			command: compressed.x
	};
	
	switch (compressed.m) {
		case 'b':
			packet.method = ETransmissionMethod.BROADCAST;
			break;
		case 'd':
			packet.method = ETransmissionMethod.DIRECT;
			packet.destination = compressed.d;
			break;
		case 'm':
			packet.method = ETransmissionMethod.MULTICAST;
			packet.destinations = compressed.d!.split(',');
			break;
	}

	if (compressed.o === 1) packet.ignoreOwn = true;
	if (compressed.e) packet.expiry = CommonsDate.compressedToDate(compressed.e);

	if (compressed.z !== undefined) packet.data = compressed.z;
	
	if (compressed.r !== undefined) packet.replay = compressed.r === 1;
	
	return packet;
}
