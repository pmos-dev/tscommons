import { CommonsBase62 } from 'tscommons-core';
import { CommonsType } from 'tscommons-core';

import { TChannel, TDeviceId } from './icommons-network-packet';

export interface ICommonsNetworkHandshake {
		channel: TChannel;
		deviceId: TDeviceId;
		[name: string]: any;
}

export function isICommonsNetworkHandshake(test: any): test is ICommonsNetworkHandshake {
	if (!CommonsType.isObject(test)) return false;

	const attempt: ICommonsNetworkHandshake = test as ICommonsNetworkHandshake;
	
	if (!CommonsType.hasPropertyString(attempt, 'channel') || !CommonsBase62.isId(attempt.channel)) return false;
	if (!CommonsType.hasPropertyString(attempt, 'deviceId') || !CommonsBase62.isId(attempt.deviceId)) return false;

	return true;
}
