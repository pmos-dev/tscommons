import { CommonsType } from 'tscommons-core';

import { IRgb, IRgba } from '../interfaces/irgb';
import { IHsl, IHsla } from '../interfaces/ihsl';
import { IHsv, IHsva } from '../interfaces/ihsv';

import { ERgba } from '../enums/ergba';
import { EHsla } from '../enums/ehsla';
import { EHsva } from '../enums/ehsva';

export abstract class CommonsColor {
	public static rgbToHsl(rgb: IRgb|IRgba): IHsl|IHsla {
		const clone: IRgb = {
				[ERgba.RED]: rgb[ERgba.RED],
				[ERgba.GREEN]: rgb[ERgba.GREEN],
				[ERgba.BLUE]: rgb[ERgba.BLUE]
		};
		
		clone[ERgba.RED] /= 255;
		clone[ERgba.GREEN] /= 255;
		clone[ERgba.BLUE] /= 255;

		const max: number = Math.max(clone[ERgba.RED], clone[ERgba.GREEN], clone[ERgba.BLUE]);
		const min: number = Math.min(clone[ERgba.RED], clone[ERgba.GREEN], clone[ERgba.BLUE]);
		let h: number = 0;
		let s: number = 0;
		const l: number = (max + min) / 2;

		if (max === min) {
			h = s = 0; // achromatic
		} else {
			const d: number = max - min;
			s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
			switch (max){
				case clone[ERgba.RED]: h = (clone[ERgba.GREEN] - clone[ERgba.BLUE]) / d + (clone[ERgba.GREEN] < clone[ERgba.BLUE] ? 6 : 0); break;
				case clone[ERgba.GREEN]: h = (clone[ERgba.BLUE] - clone[ERgba.RED]) / d + 2; break;
				case clone[ERgba.BLUE]: h = (clone[ERgba.RED] - clone[ERgba.GREEN]) / d + 4; break;
			}
			h /= 6;
		}

		const hsl: IHsl = {
				[EHsla.HUE]: h,
				[EHsla.SATURATION]: s,
				[EHsla.LIGHTNESS]: l
		};
		
		// pass-through for alpha channel
		if (CommonsType.hasPropertyNumber(rgb, ERgba.ALPHA)) {
			const rgba: IRgba = rgb as IRgba;
			const hsla: IHsla = hsl as IHsla;
			
			hsla[EHsla.ALPHA] = rgba[ERgba.ALPHA];
			
			return hsla;
		}
		
		return hsl;
	}
	
	public static rgbToHex(rgb: IRgb, hash: boolean = true): string {
		return `${hash ? '#' : ''}${[
				Math.floor(rgb[ERgba.RED]).toString(16).padStart(2, '0'),
				Math.floor(rgb[ERgba.GREEN]).toString(16).padStart(2, '0'),
				Math.floor(rgb[ERgba.BLUE]).toString(16).padStart(2, '0')
		]
				.join('')}`;
	}
	
	public static hexToRgb(hex: string): IRgb {
		const regex: RegExp = /^#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i;
		
		const match: RegExpExecArray|null = regex.exec(hex);
		if (!match) throw new Error('Invalid hex color');
		
		return {
				[ ERgba.RED ]: parseInt(match[1], 16),
				[ ERgba.GREEN ]: parseInt(match[2], 16),
				[ ERgba.BLUE ]: parseInt(match[3], 16)
		};
	}

	private static hueToRgb(p: number, q: number, t: number): number {
		if (t < 0) t += 1;
		if (t > 1) t -= 1;
		if (t < 1 / 6) return p + (q - p) * 6 * t;
		if (t < 1 / 2) return q;
		if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
		
		return p;
	}
	
	public static hslToRgb(hsl: IHsl|IHsla): IRgb|IRgba {
		const rgb: IRgb = {
				[ERgba.RED]: 0,
				[ERgba.GREEN]: 0,
				[ERgba.BLUE]: 0
		};
	
		if (hsl[EHsla.SATURATION] === 0) {
			rgb[ERgba.RED] = rgb[ERgba.GREEN] = rgb[ERgba.BLUE] = hsl[EHsla.LIGHTNESS];
		} else {
			const q: number = hsl[EHsla.LIGHTNESS] < 0.5 ? hsl[EHsla.LIGHTNESS] * (1 + hsl[EHsla.SATURATION]) : hsl[EHsla.LIGHTNESS] + hsl[EHsla.SATURATION] - hsl[EHsla.LIGHTNESS] * hsl[EHsla.SATURATION];
			const p: number = 2 * hsl[EHsla.LIGHTNESS] - q;
			rgb[ERgba.RED] = CommonsColor.hueToRgb(p, q, hsl[EHsla.HUE] + 1 / 3);
			rgb[ERgba.GREEN] = CommonsColor.hueToRgb(p, q, hsl[EHsla.HUE]);
			rgb[ERgba.BLUE] = CommonsColor.hueToRgb(p, q, hsl[EHsla.HUE] - 1 / 3);
		}
	
		rgb[ERgba.RED] = Math.round(rgb[ERgba.RED] * 255);
		rgb[ERgba.GREEN] = Math.round(rgb[ERgba.GREEN] * 255);
		rgb[ERgba.BLUE] = Math.round(rgb[ERgba.BLUE] * 255);
		
		// pass-through for alpha channel
		if (CommonsType.hasPropertyNumber(hsl, EHsla.ALPHA)) {
			const hsla: IHsla = hsl as IHsla;
			const rgba: IRgba = rgb as IRgba;
			
			rgba[ERgba.ALPHA] = hsla[EHsla.ALPHA];
			return rgba;
		}

		return rgb;
	}
	
	public static hsvToHsl(hsv: IHsv|IHsva): IHsl|IHsla {
		const l: number = (2 - hsv[EHsva.SATURATION]) * (hsv[EHsva.VALUE] / 2);
		
		let s: number = hsv[EHsva.SATURATION];
		if (l !== 0) {
			if (l === 1) {
				s = 0;
			} else if (l < 0.5) {
				s = s * hsv[EHsva.VALUE] / (l * 2);
			} else {
				s = s * hsv[EHsva.VALUE] / (2 - l * 2);
			}
		}
		
		const hsl: IHsl = {
				[EHsla.HUE]: hsv[EHsva.HUE],
				[EHsla.SATURATION]: s,
				[EHsla.LIGHTNESS]: l
		};
		
		// pass-through for alpha channel
		if (CommonsType.hasPropertyNumber(hsl, EHsla.ALPHA)) {
			const hsva: IHsva = hsv as IHsva;
			const hsla: IHsla = hsl as IHsla;
			
			hsla[EHsla.ALPHA] = hsva[EHsva.ALPHA];
			return hsla;
		}

		return hsl;
	}
	
	public static hsvToRgb(hsv: IHsv|IHsva): IRgb|IRgba {
		const hsl: IHsl|IHsla = CommonsColor.hsvToHsl(hsv);
		return CommonsColor.hslToRgb(hsl);
	}
	
	public static blendRgbs(
			rgbA: IRgb|IRgba,
			rgbB: IRgb|IRgba,
			weight: number,
			invertHueWheelDirection: boolean = false
	): IRgb|IRgba {
		let hue: number = 0;
		
		const hslA: IHsl|IHsla = CommonsColor.rgbToHsl(rgbA);
		const hslB: IHsl|IHsla = CommonsColor.rgbToHsl(rgbB);
		
		if (invertHueWheelDirection) {
			// hue has to be calculated inverted round the colorwheel rather than directly
			
			if (hslA[EHsla.HUE] < rgbB[EHsla.HUE]) {
				const delta: number = hslA[EHsla.HUE] + (1 - hslB[EHsla.HUE]);
				hue = hslB[EHsla.HUE] + (delta * weight);
				if (hue > 1) hue -= 1;
			} else {
				const delta: number = hslB[EHsla.HUE] + (1 - hslA[EHsla.HUE]);
				hue = hslA[EHsla.HUE] + (delta * weight);
				if (hue > 1) hue -= 1;
			}
		} else {
			hue = (hslB[EHsla.HUE] * weight) + (hslA[EHsla.HUE] * (1 - weight));
		}
		
		const blend: IHsl = {
				[ EHsla.HUE ]: hue,
				[ EHsla.SATURATION ]: (hslB[EHsla.SATURATION] * weight) + (hslA[EHsla.SATURATION] * (1 - weight)),
				[ EHsla.LIGHTNESS ]: (hslB[EHsla.LIGHTNESS] * weight) + (hslA[EHsla.LIGHTNESS] * (1 - weight))
		};
		
		const rgb: IRgb = CommonsColor.hslToRgb(blend);
		
		// pass-through for alpha channel
		if (CommonsType.hasPropertyNumber(rgb, ERgba.ALPHA)) {
			const aa: IRgba = rgbA as IRgba;
			const ba: IRgba = rgbA as IRgba;
			const rgba: IRgba = rgb as IRgba;
			
			rgba[EHsla.ALPHA] = (ba[EHsla.ALPHA] * weight) + (aa[EHsla.ALPHA] * (1 - weight));
			return rgba;
		}

		return rgb;
	}
}
