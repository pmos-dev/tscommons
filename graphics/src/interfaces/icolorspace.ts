export interface IColorspace {
		[channel: string]: number;
}
