import { EHsla } from '../enums/ehsla';

import { IColorspace } from './icolorspace';

export interface IHsl extends IColorspace {
		[EHsla.HUE]: number;
		[EHsla.SATURATION]: number;
		[EHsla.LIGHTNESS]: number;
}

export interface IHsla extends IHsl {
		[EHsla.ALPHA]: number;
}
