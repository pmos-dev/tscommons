import { ERgba } from '../enums/ergba';

import { IColorspace } from './icolorspace';

export interface IRgb extends IColorspace {
		[ERgba.RED]: number;
		[ERgba.GREEN]: number;
		[ERgba.BLUE]: number;
}

export interface IRgba extends IRgb {
		[ERgba.ALPHA]: number;
}
