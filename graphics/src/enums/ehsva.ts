export enum EHsva {
		HUE = 'hue',
		SATURATION = 'saturation',
		VALUE = 'value',
		ALPHA = 'alpha'
}
