export enum EHsla {
		HUE = 'hue',
		SATURATION = 'saturation',
		LIGHTNESS = 'lightness',
		ALPHA = 'alpha'
}
