export enum ECommonsColor {
		RED = 'red',
		ORANGE = 'orange',
		YELLOW = 'yellow',
		GREEN = 'green',
		CYAN = 'cyan',
		BLUE = 'blue',
		INDIGO = 'indigo',
		MAGENTA = 'magenta',
		
		BLACK = 'black',
		WHITE = 'white',
		GREY = 'grey',
		SILVER = 'silver'
}

export function fromECommonsColor(color: ECommonsColor): string {
	switch (color) {
		case ECommonsColor.RED:
			return ECommonsColor.RED.toString();
		case ECommonsColor.ORANGE:
			return ECommonsColor.ORANGE.toString();
		case ECommonsColor.YELLOW:
			return ECommonsColor.YELLOW.toString();
		case ECommonsColor.GREEN:
			return ECommonsColor.GREEN.toString();
		case ECommonsColor.CYAN:
			return ECommonsColor.CYAN.toString();
		case ECommonsColor.BLUE:
			return ECommonsColor.BLUE.toString();
		case ECommonsColor.INDIGO:
			return ECommonsColor.INDIGO.toString();
		case ECommonsColor.MAGENTA:
			return ECommonsColor.MAGENTA.toString();
		case ECommonsColor.BLACK:
			return ECommonsColor.BLACK.toString();
		case ECommonsColor.WHITE:
			return ECommonsColor.WHITE.toString();
		case ECommonsColor.SILVER:
			return ECommonsColor.SILVER.toString();
		case ECommonsColor.GREY:
			return ECommonsColor.GREY.toString();
	}
	
	throw new Error('Unknown color');
}

export function toECommonsColor(color: string): ECommonsColor|undefined {
	switch (color) {
		case ECommonsColor.RED.toString():
			return ECommonsColor.RED;
		case ECommonsColor.ORANGE.toString():
			return ECommonsColor.ORANGE;
		case ECommonsColor.YELLOW.toString():
			return ECommonsColor.YELLOW;
		case ECommonsColor.GREEN.toString():
			return ECommonsColor.GREEN;
		case ECommonsColor.CYAN.toString():
			return ECommonsColor.CYAN;
		case ECommonsColor.BLUE.toString():
			return ECommonsColor.BLUE;
		case ECommonsColor.INDIGO.toString():
			return ECommonsColor.INDIGO;
		case ECommonsColor.MAGENTA.toString():
			return ECommonsColor.MAGENTA;
		case ECommonsColor.BLACK.toString():
			return ECommonsColor.BLACK;
		case ECommonsColor.WHITE.toString():
			return ECommonsColor.WHITE;
		case ECommonsColor.SILVER.toString():
			return ECommonsColor.SILVER;
		case ECommonsColor.GREY.toString():
			return ECommonsColor.GREY;
	}
	
	throw new Error('Unknown color');
}
