// This is a true interface similar to Java as opposed to a pseudo-struct interface as per TypeScript

import { ECommonsRunState } from '../enums/ecommons-run-state';

export interface ICommonsStateful {
	getState(): ECommonsRunState;
	isAborted(): boolean;
}
