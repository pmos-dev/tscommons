export class CommonsDebouncer<T> {
	private last: T|undefined = undefined;
	private unique: T|undefined = undefined;
	private history: (T|undefined)[] = [];
	
	constructor(
		private release: number
	) {}
	
	public debounce(value: T): T|undefined {
		this.last = value;
		
		if (this.last !== this.unique) {
			this.unique = this.last;

			if (!this.history.includes(this.unique)) {
				this.history.push(this.unique);
				setTimeout((): void => {
					const index: number = this.history.indexOf(this.unique);
					this.history.splice(index, 1);
				}, this.release);
				
				return this.unique;
			}
		}
		
		return undefined;
	}
}
