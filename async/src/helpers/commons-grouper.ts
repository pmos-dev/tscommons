import { Observable, Subject } from 'rxjs';

export class CommonsGrouper<T> {
	private onGroup: Subject<T[]> = new Subject<T[]>();

	private interval: any;	// has to any, as node.js uses Timer and Angular uses number
	private group: T[] = [];
	
	constructor(
			period: number,
			private ignoreEmpty: boolean = true
	) {
		this.interval = setInterval(
				(): void => {
					if (this.group.length === 0 && this.ignoreEmpty) return;
					
					this.onGroup.next(this.group);
					this.group = [];
				},
				period
		);
	}
	
	public groupObservable(): Observable<T[]> {
		return this.onGroup;
	}

	public add(value: T): void {
		this.group.push(value);
	}
	
	public cancel(): void {
		clearInterval(this.interval);
	}
}
