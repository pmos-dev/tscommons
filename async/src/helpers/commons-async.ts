import { CommonsBase62 } from 'tscommons-core';

import { ECommonsInvocation } from '../enums/ecommons-invocation';

type TTimeout = {
	thread: any;	// has to any, as node.js uses Timer and Angular uses number
	reject: () => void;
};

export abstract class CommonsAsync {
	private static timeouts: Map<string, TTimeout> = new Map<string, TTimeout>();
	
	// has to any, as node.js uses Timer and Angular uses number
	private static intervals: Map<string, any> = new Map<string, any>();
	
	private static isInCycle: Map<string, boolean> = new Map<string, boolean>();
	private static idUid: Map<string, string> = new Map<string, string>();
	private static isAborted: Map<string, boolean> = new Map<string, boolean>();
	
	private static clearTimeout(id: string): void {
		clearTimeout(CommonsAsync.timeouts.get(id)!.thread);
		CommonsAsync.timeouts.delete(id);
	}
	
	public static timeout(ms: number, id?: string): Promise<void> {
		if (id && CommonsAsync.timeouts.has(id)) CommonsAsync.clearTimeout(id);
		
		return new Promise((resolve: () => void, reject: (e: Error) => void): void => {
			const thread = setTimeout((): void => {
				if (id && CommonsAsync.timeouts.has(id)) CommonsAsync.clearTimeout(id);
				resolve();
			}, ms);
			
			if (id) {
				CommonsAsync.timeouts.set(id, {
						thread: thread,
						reject: (): void => { reject(new Error('abortTimeout called')); }
				});
			}
		});
	}
	
	public static abortTimeout(id: string): void {
		if (CommonsAsync.timeouts.has(id)) {
			const reject: () => void = CommonsAsync.timeouts.get(id)!.reject;
			CommonsAsync.clearTimeout(id);
			reject();
		}
	}
	
	private static clearInterval(id: string): void {
		clearInterval(CommonsAsync.intervals.get(id)!);
		CommonsAsync.intervals.delete(id);
		
		const internalId: string|undefined = this.idUid.get(id);
		if (internalId !== undefined) this.isInCycle.delete(internalId);
		
		this.idUid.delete(id);
	}
	
	public static interval(
			ms: number,
			approach: ECommonsInvocation,
			callback: () => Promise<void>,
			id?: string
	): void {
		if (id && CommonsAsync.intervals.has(id)) CommonsAsync.clearInterval(id);
		
		switch (approach) {
			case ECommonsInvocation.FIXED: {
				const thread = setInterval(async (): Promise<void> => {
					await callback();
				}, ms);
			
				if (id) CommonsAsync.intervals.set(id, thread);
				
				break;
			}
			case ECommonsInvocation.WHEN: {
				const cycle: () => Promise<void> = async (): Promise<void> => {
					if (id !== undefined) this.isAborted.set(id, false);
					
					const isAborted: () => boolean = () => {
						if (id === undefined) return false;
						return this.isAborted.get(id) || false;
					};
					
					let first: boolean = true;
					while (!isAborted()) {
						try {
							if (first) {
								first = false;
								continue;
							}
							await callback();
						} catch (ex) {
							// can't throw as it would abort the while loop
							console.log(ex);
						} finally {
							await CommonsAsync.timeout(ms);
						}
					}
					
					if (id !== undefined) this.isAborted.delete(id);
				};
				
				cycle();
				
				break;
			}
			case ECommonsInvocation.IF: {
				const internalId = CommonsBase62.generateRandomId();
				if (id) this.idUid.set(id, internalId);
				
				this.isInCycle.set(internalId, false);
				
				CommonsAsync.interval(
						ms,
						ECommonsInvocation.FIXED,
						async (): Promise<void> => {
							if (this.isInCycle.get(internalId)) return;

							try {
								this.isInCycle.set(internalId, true);
								await callback();
							} finally {
								this.isInCycle.set(internalId, false);
							}
						},
						id
				);
				
				break;
			}
		}
	}
	
	public static abortInterval(id: string): void {
		if (this.isAborted.has(id)) this.isAborted.set(id, true);
		
		if (CommonsAsync.intervals.has(id)) {
			CommonsAsync.clearInterval(id);
		}
	}
}
