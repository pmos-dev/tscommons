import { Observable, Subject } from 'rxjs';

import { TCommonsFinalValue } from '../types/tcommons-final-value';

export class CommonsFinalValue<T> {
	private onRelease: Subject<TCommonsFinalValue<T>> = new Subject<TCommonsFinalValue<T>>();

	private timeout: any|undefined;	// has to any, as node.js uses Timer and Angular uses number

	constructor(
			private delay: number
	) {}

	public push(value: T): void {
		if (this.timeout) clearTimeout(this.timeout);
		
		const snapshot: Date = new Date();
		
		this.timeout = setTimeout((): void => {
			this.onRelease.next({ value: value, timestamp: snapshot });
		}, this.delay);
	}

	public releaseObservable(): Observable<TCommonsFinalValue<T>> {
		return this.onRelease;
	}
}
