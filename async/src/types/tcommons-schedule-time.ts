import { CommonsType } from 'tscommons-core';

export type TCommonsScheduleTime = {
		year?: number|number[];
		month?: number|number[];
		day?: number|number[];
		dow?: number|number[];
		hour?: number|number[];
		minute?: number|number[];
		second?: number|number[];
};

export function isTCommonsScheduleTime(test: unknown): test is TCommonsScheduleTime {
	if (!CommonsType.isObject(test)) return false;

	for (const key of 'year,month,day,dow,hour,minute,second'.split(',')) {
		if (!CommonsType.hasPropertyNumberOrUndefined(test, key) && !CommonsType.hasPropertyArrayOrUndefined(test, key)) return false;
		if (CommonsType.hasPropertyArray(test, key) && !CommonsType.isNumberArray(test[key])) return false;
	}
	
	return true;
}
