import { CommonsType } from 'tscommons-core';

import { TCommonsScheduleTime, isTCommonsScheduleTime } from './tcommons-schedule-time';

export type TCommonsSchedule = {
		at: TCommonsScheduleTime;
		action: string;
};

export function isTCommonsSchedule(test: unknown): test is TCommonsSchedule {
	if (!CommonsType.hasPropertyObject(test, 'at')) return false;
	if (!isTCommonsScheduleTime(test.at)) return false;
	
	if (!CommonsType.hasPropertyString(test, 'action')) return false;
	
	return true;
}
