import { CommonsType } from 'tscommons-core';

import { CommonsSemaphore } from '../helpers/commons-semaphore';

import { ECommonsSynchronisedTimeoutAction } from '../enums/ecommons-synchronised-timeout-action';

export class CommonsSynchronised {
	private semaphores: Map<string, CommonsSemaphore> = new Map<string, CommonsSemaphore>();

	public async synchronised<T>(
			context: string,
			timeout: number,
			timeoutAction: ECommonsSynchronisedTimeoutAction,
			callback: () => Promise<T>
	): Promise<T> {
		if (!this.semaphores.has(context)) this.semaphores.set(context, new CommonsSemaphore());
		const semaphore: CommonsSemaphore = CommonsType.assertObject(this.semaphores.get(context)) as CommonsSemaphore;

		let claimed: boolean = false;
		try {
			await semaphore.claim(timeout);
			claimed = true;
		} catch (e) {
			claimed = false;
		}

		if (!claimed && timeoutAction === ECommonsSynchronisedTimeoutAction.ABORT) throw new Error('Timeout on synchronised block');
		
		try {
			return await callback();
		} finally {
			if (claimed) semaphore.release();
		}
	}
}
