import { CommonsBase62 } from 'tscommons-core';

import { CommonsAsync } from '../helpers/commons-async';

import { ICommonsStateful } from '../interfaces/icommons-stateful';

import { ECommonsRunState } from '../enums/ecommons-run-state';

export abstract class CommonsStateThread implements ICommonsStateful {
	public static async maximumTimeoutThread(
			timeout: number,
			callback: (stateful: ICommonsStateful) => Promise<void>
	): Promise<boolean> {
		const thread: CommonsStateThread = new (class extends CommonsStateThread {
			public async run(): Promise<void> {
				await callback(this);
			}
		})();
		
		return new Promise((resolve: (completed: boolean) => void, reject: (reason: Error) => void): void => {
			thread.start(
					timeout,
					(): void => {
						switch (thread.getState()) {
							case ECommonsRunState.PRE:	// never happens, but doesn't hurt
							case ECommonsRunState.RUNNING:
								return;
							case ECommonsRunState.ERROR:
								reject(thread.getError() || new Error('Unknown thread error'));
								return;
							case ECommonsRunState.COMPLETED:
								resolve(true);
								return;
							case ECommonsRunState.TIMEDOUT:
							case ECommonsRunState.ABORTED:
								resolve(false);
								return;
						}
						
						throw new Error('Unknown thread state');
					}
			);
		});
	}
	
	private state: ECommonsRunState;

	private timedOut: boolean = false;
	private aborted: boolean = false;
	private error: Error|undefined;
	
	constructor() {
		this.state = ECommonsRunState.PRE;
	}
	
	public getState(): ECommonsRunState {
		return this.state;
	}
	
	protected abstract run(): Promise<void>;
	
	public isAborted(): boolean {
		return this.aborted;
	}
	
	public isTimedOut(): boolean {
		return this.timedOut;
	}
	
	public getError(): Error|undefined {
		return this.error;
	}
	
	public start(timeout: number, changed?: () => void): void {
		this.state = ECommonsRunState.RUNNING;
		if (changed) changed();

		const internalId = CommonsBase62.generateRandomId();
		
		(async (): Promise<void> => {
			try {
				await CommonsAsync.timeout(timeout, internalId);
			
				this.timedOut = true;
				this.abort();
			} catch (_) {
				// abort timeout, so ignore
			}
		})();
		
		(async (): Promise<void> => {
			try {
				await this.run();
				
				if (this.timedOut) this.state = ECommonsRunState.TIMEDOUT;
				else if (this.aborted) this.state = ECommonsRunState.ABORTED;
				else this.state = ECommonsRunState.COMPLETED;
			} catch (e) {
				this.error = e;
				this.state = ECommonsRunState.ERROR;
			} finally {
				CommonsAsync.abortTimeout(internalId);
				if (changed) changed();
			}
		})();
	}
	
	public abort(): void {
		this.aborted = true;
	}
}
