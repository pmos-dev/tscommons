import { CommonsType } from 'tscommons-core';
import { CommonsBase62 } from 'tscommons-core';

import { CommonsAsync } from '../helpers/commons-async';

import { TCommonsScheduleTime } from '../types/tcommons-schedule-time';
import { isTCommonsSchedule } from '../types/tcommons-schedule';

import { ECommonsInvocation } from '../enums/ecommons-invocation';

type TScheduled = {
		at: TCommonsScheduleTime;
		callback: () => Promise<void>;
};

export class CommonsSchedule {
	private static doesMatchSchedule(test: Date, at: TCommonsScheduleTime) {
		const years: number[] = [];
		if (at.year !== undefined) {
			if (CommonsType.isNumberArray(at.year)) years.push(...at.year);
			else years.push(at.year);
		}
		if (years.length > 0 && !years.includes(test.getFullYear())) return false;

		const months: number[] = [];
		if (at.month !== undefined) {
			if (CommonsType.isNumberArray(at.month)) months.push(...at.month);
			else months.push(at.month);
		}
		if (months.length > 0 && !months.includes(test.getMonth())) return false;

		const days: number[] = [];
		if (at.day !== undefined) {
			if (CommonsType.isNumberArray(at.day)) days.push(...at.day);
			else days.push(at.day);
		}
		if (days.length > 0 && !days.includes(test.getDate())) return false;

		const dows: number[] = [];
		if (at.dow !== undefined) {
			if (CommonsType.isNumberArray(at.dow)) dows.push(...at.dow);
			else dows.push(at.dow);
		}
		if (dows.length > 0 && !dows.includes(test.getDay())) return false;

		const hours: number[] = [];
		if (at.hour !== undefined) {
			if (CommonsType.isNumberArray(at.hour)) hours.push(...at.hour);
			else hours.push(at.hour);
		}
		if (hours.length > 0 && !hours.includes(test.getHours())) return false;

		const minutes: number[] = [];
		if (at.minute !== undefined) {
			if (CommonsType.isNumberArray(at.minute)) minutes.push(...at.minute);
			else minutes.push(at.minute);
		}
		if (minutes.length > 0 && !minutes.includes(test.getMinutes())) return false;

		const seconds: number[] = [];
		if (at.second !== undefined) {
			if (CommonsType.isNumberArray(at.second)) seconds.push(...at.second);
			else seconds.push(at.second);
		}
		if (seconds.length > 0 && !seconds.includes(test.getSeconds())) return false;

		return true;
	}

	private schedules: Map<string, TScheduled> = new Map<string, TScheduled>();

	private isRunning: boolean = false;

	constructor(
		private id: string
	) {}
	
	public schedule(
			at: TCommonsScheduleTime,
			callback: () => Promise<void>,
			id?: string
	): void {
		if (id && this.schedules.has(id)) this.clearSchedule(id);
	
		if (!id) id = CommonsBase62.generateRandomId();

		const schedule: TScheduled = {
			at: at,
			callback: callback
		};
	
		this.schedules.set(id, schedule);
	}

	public clearSchedule(id: string): void {
		this.schedules.delete(id);
	}

	private async dispatch(): Promise<void> {
		const now: Date = new Date();

		for (const schedule of Array.from(this.schedules.values())) {
			if (!CommonsSchedule.doesMatchSchedule(now, schedule.at)) continue;

			(async (): Promise<void> => { schedule.callback(); })();
		}
	}

	public start(): boolean {
		if (this.isRunning) return false;
	
		CommonsAsync.interval(
				1000,
				ECommonsInvocation.FIXED,
				async (): Promise<void> => {
					await this.dispatch();
				},
				`schedule_${this.id}`
		);
	
		return true;
	}

	public stop(): boolean {
		if (!this.isRunning) return false;
	
		CommonsAsync.abortInterval(`schedule_${this.id}`);
	
		return true;
	}
	
	public parse<T>(json: unknown[], toAction: (_: string) => T|undefined, callback: (_: T) => Promise<void>): void {
		for (const schedule of json) {
			if (!isTCommonsSchedule(schedule)) throw new Error('Schedule JSON contains an invalid entry');

			const action: T|undefined = toAction(schedule.action);
			if (action === undefined) throw new Error('Schedule JSON contains an invalid action');

			this.schedule(
					schedule.at,
					async (): Promise<void> => {
						await callback(action);
					}
			);
		}
	}
}
