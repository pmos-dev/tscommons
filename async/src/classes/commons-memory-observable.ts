import { Observable } from 'rxjs';

export class CommonsMemoryObservable<T> {
	constructor(
			private observable: Observable<T>,
			private value: T
	) {
		this.observable
				.subscribe(
					(v: T): void => {
						this.value = v;
					}
				);
	}
	
	public get(): T {
		return this.value;
	}
}
