export enum ECommonsInvocation {
	// run as normal; cycle per interval
	FIXED = 'fixed',
	
	// restart interval when cycle complete
	WHEN = 'when',
	
	// only run cycle if last one complete
	IF = 'if'
}
